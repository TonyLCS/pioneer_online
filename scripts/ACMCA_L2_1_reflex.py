#!/usr/bin/env python

import sys
import rospy
import time
import math
import os
import copy
import csv
import pandas as pd

#   from xcs_api import XCS  # import xcs agent API
#   from visualization import Visualization  # import Visualization to show results
import random
import csv
#   import pandas as pd
#   import numpy as np

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64, Int64, Bool

from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist

from sensor_msgs.msg import LaserScan

from nav_msgs.msg import Odometry

from robot_nav_plan.srv import *


from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io, feature
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped

import subprocess

from xcs_api import XCS  # import xcs agent API
"""
def callback_bumper(msg):
    global ros_perception

    if len(msg.data) > 0:
        ros_perception = msg.data
        print("ros_perception:", ros_perception)

"""
def callback_effect(msg):
    global bumper_fire
    bumper_fire = msg.data




def callback_touching_stimulus(msg):
    "to update reflex reward"
    global touching_stimulus # this one will be update all the time and passing for reward
    touching_stimulus = msg.data
    #print("touching stimulus:", touching_stimulus)

def callback_XCS_iteration(msg):
    global start_xcs_iteration
    global touching_perception
    global reflex_iteration_in_progress
    if reflex_iteration_in_progress:
        "reflex_iteration_in_progress, no receive and respond to any more touching perception "
        pass
    else:
        start_xcs_iteration = True
        touching_perception = msg.data
        print("touching_perception:", touching_perception)

    #print("##############reflex action#########################")
    #action_xcs = xcs_model.XCS_get_prediction_API(ros_perception)
    #print("==>> reflex action:", action_xcs)
    #print("##############reflex action#########################")

    "publish reflex action"
    #pub_reflex_action.publish(action_xcs)

def callback_complete(msg):
    global complete_a_behaviour
    complete_a_behaviour = msg.data
    print("complete_a_behaviour :",complete_a_behaviour)

"#################################################"
"###########   XCS agents     ########"
"#################################################"

def XCS_agent_initiate(task_path=None, classifiers_population=None):

    # Initialize game replay object
    xcs_model = XCS()
    xcs_model.set_parameters(task_path)
    xcs_model.XCS_initiate_fiters_API()
    #xcs_model.XCS_initiate_test_fiters_API()
    #xcs_model.XCS_initiate_mcts_fiters_API()

    xcs_model.set_learning_mode()

    if classifiers_population is not None:
        xcs_model.XCS_loading_classifiers_population_from_csv(classifiers_population) # loading classifiers [P]

    return xcs_model

"""
def XCS_agent_initiate(classifiers_population = None):

    # Initialize game replay object
    xcs_model = XCS()
    xcs_model.set_parameters()
    xcs_model.XCS_initiate_fiters_API()
    #xcs_model.XCS_initiate_test_fiters_API()
    #xcs_model.XCS_initiate_mcts_fiters_API()

    xcs_model.set_learning_mode()
    if classifiers_population is not None:
        xcs_model.XCS_loading_classifiers_population_from_csv(classifiers_population) # loading classifiers [P]

    return xcs_model
"""


if __name__ == "__main__":
    """
    XCS select a reflex_action as output by the touching_preception
    """
    "##############################"
    "initiate ros interactions"
    "##############################"
    rospy.init_node("acmca_l2_reflex", anonymous=False)


    "#############################"
    "initiate xcs agent"
    "#############################"
    classifiers_population = None
    "/reflex_hire_l_OnlyBumper/lastest"
    xcs_model = XCS_agent_initiate("/Results/doThisTask.csv", "/reflex_node_l/lastest")
    #xcs_model = XCS_agent_initiate("/Results/doThisTask.csv", classifiers_population)
    rospy.sleep(3)

    "#############################"
    "active xcs"
    "#############################"
    "active  xcs iteration by this callback"
    touching_perception = tuple()
    rospy.Subscriber('/p3dx/ACMCA/touching/perception', Float64MultiArray, callback_XCS_iteration)
    # rospy.Subscriber('/p3dx/bumper_state_date', Float64MultiArray, callback_bumper)

    "#############################"
    "for effect"
    "#############################"

    pub_reflex_action = rospy.Publisher('/p3dx/ACMCA/reflex/action', Int64, queue_size=5) #reflex action

    action_pattern_pub = rospy.Publisher('/p3dx/ACMCA/action_modifier/action', Int64, queue_size=5) #in pioneer_take_action L5-1

    "#############################"
    "update reward"
    "#############################"
    " touching stimulus is for update reward of reflex action"
    touching_stimulus = 0
    rospy.Subscriber('/p3dx/ACMCA/touching/stimulus', Float64, callback_touching_stimulus)

    bumper_fire =0
    rospy.Subscriber('/p3dx/ACMCA/L5_reflex/effect', Int64, callback_effect)

    complete_a_behaviour = False
    rospy.Subscriber('/p3dx/ACMCA/L5_reflex/complete', Bool, callback_complete)


    print("acmca_l2_reflex spin...")
    start_xcs_iteration = False
    reflex_iteration_in_progress = False
    xcs_iter_index = 0

    while not rospy.is_shutdown():
        complete_a_behaviour = False # is true by l5 feedback

        if start_xcs_iteration:
            reflex_iteration_in_progress = True # this will block other touching_perception.

            "#####################################"
            "######  select reflex action  #######"
            "#####################################"

            print("##############reflex action#########################")
            action_xcs = xcs_model.XCS_get_prediction_API(touching_perception)
            print("==>> reflex action:", action_xcs)
            print("##############reflex action#########################")

            "#####################################"
            "#########  has effect  ##############"
            "#####################################"
            action_pattern = 2
            action_pattern_pub.publish(action_pattern)
            #before_touching_stimulus = touching_stimulus
            pub_reflex_action.publish(action_xcs)
            "begin to count collision during effect"

            #print("publish reflex action and sleep for 2.5 second:", action_xcs)
            #rospy.sleep(5.5)

            "#####################################"
            "#########  update xcs  ##############"
            "#####################################"
            waiting_index = 0
            while not complete_a_behaviour:
                waiting_index = waiting_index + 1
                print("not complete the reflex behaviour yet. take action ", action_xcs," and waiting", round(0.2*waiting_index, 2), "...")
                rospy.sleep(0.2)

            reward_episode = 1000
            if bumper_fire > 4:
                reward_episode = -1000

            #"go back to path planning "
            #action_pattern = 1 # recover to path Planning
            #action_pattern_pub.publish(action_pattern)

            reward_method = "even"

            xcs_model.XCS_update_reward_API(reward_episode, reward_method, True, 0.2)

            print("       bumper_fire -> ", bumper_fire, "<=")
            if reward_episode > 500:
                print("         ++++++++++++++++++++++++++++++++++++++++++++++++++++")
                print("         ++++++++++++++++++++++++++++++++++++++++++++++++++++")
                print("         ++++++++++++++++++++++++++++++++++++++++++++++++++++")
            else:
                print("                 ....................................................")
                print("                 ....................................................")
                print("                 ....................................................")
                print("                 ....................................................")
                print("                 ....................................................")

            """
            #after_touching_stimulus = touching_stimulus
            reward_episode = 1000
            if after_touching_stimulus >= before_touching_stimulus and after_touching_stimulus >= 1: #prevent shaking
                reward_episode = -1000


            reward_method = "even"
            print("     before_touching_stimulus -> after_touching_stimulus:    ", before_touching_stimulus, " -> ", after_touching_stimulus,   "<=")
            xcs_model.XCS_update_reward_API(reward_episode, reward_method, True, 0.2)

            if reward_episode > 500:
                print("####################################################")
                print("++++++++++++++++++++++++++++++++++++++++++++++++++++")
                print("####################################################")
            else:
                print("         ....................................................")
                print("         ....................................................")
                print("         ....................................................")
            """

            "end of update xcs"

            "save classifiers population"
            xcs_model.XCS_save_classifiers_API(xcs_iter_index, "./src/robot_nav_plan/result/reflex_node_l/")
            xcs_model.XCS_deletion_API(900)  # pop_size =9000
            xcs_iter_index = xcs_iter_index + 1
            start_xcs_iteration = False
            reflex_iteration_in_progress = False
            print("  end of an iteration")
            print("\n")
            print("\n")



    "###############################"
    "go to a step iteration"
    "###############################"
    """
        if not first_collision_flag:
        print("not collision,but apply reflex process...")
        action_xcs = xcs_model.XCS_get_prediction_API(current_bumper_data)
    else:
        action_xcs = xcs_model.XCS_get_prediction_API(bumper_data)

    print("##############reflex action#########################")
    print("==>> reflex action:", action_xcs)
    print("##############reflex action#########################")

    """
