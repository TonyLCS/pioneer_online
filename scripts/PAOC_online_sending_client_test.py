#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *



def publich_client(header_token_seq):
    rospy.wait_for_service('PAOC_online_reading_record_service')
    try:
        ask_to_publich = rospy.ServiceProxy('PAOC_online_reading_record_service', PAOCPerceptionSRV)
        resp1 = ask_to_publich(header_token_seq)
        return resp1
    except rospy.ServiceException:
        print("Service call failed: %s", rospy.ServiceException)
        #print "Service call failed: %s"%e


if __name__ == "__main__":

    #print "PubModClient active to call PubModService"
    header_token_seq = 1
    #seq = 0
    #modifier_request_value = 0.3
    print("PAOC_online_sending_record_client header_token_seq:", header_token_seq)
    resp = publich_client(header_token_seq)
    print("resp",resp)
    #print("header_token_seq=",str(header_token_seq), "/ x=", str(resp.location_X), " / y=", str(resp.location_Y), " / IR=", str(resp.radius_inflation), "/ groundTrue=", str(resp.groundTrue),". " )

