#!/usr/bin/env python

#print "importing io, data from Python package skimage"
from skimage import io, data
#print "importing io, data from Python package skimage"

import matplotlib.pyplot as plt

if __name__ == '__main__':
	#print "show image of astronaut."
	img=data.astronaut()

	io.imshow(img)
	io.show()
