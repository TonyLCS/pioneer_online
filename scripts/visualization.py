import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import axes3d
import numpy as np
import pandas as pd

from matplotlib.collections import PolyCollection
from matplotlib import colors as mcolors
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from scipy import stats
import math

from mpl_toolkits.mplot3d.art3d import Poly3DCollection

class Visualization:

    result_path = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/"
    dataSet_file = "1_PAOC_C.csv"
    classifiers_file = "lastest"#""instance100.csv"

    def __init__(self, classifiers_file):
        print("Visualization active.")
        #csv_data = self.loadcsv(dataset_path, header=0) # contain header
        #print("csv_data:", csv_data)
        self.classifiers_file = classifiers_file+".csv"
        print("classifiers_file", self.classifiers_file)

        "draw dataset distribution"
        dataset_path = self.result_path + self.dataSet_file
        self.draw_PAOC_data_set_distribution(dataset_path)

        "draw classifeirs"
        classifiers_data_path = self.result_path + self.classifiers_file
        self.draw_classifiers(classifiers_data_path)


    def loadcsv(self, dataset_path, header):
        # to del
        print("loading csv file", dataset_path, "...")
        csv_data = pd.read_csv(dataset_path, header=header)  # this line for task 1
        return csv_data

    def draw_PAOC_data_set_distribution(self, dataset_path):
        "draw 3D distribution of learned PAOC dataset"
        "read PAOC dataSet from csv"
        try:
            print("loading csv file", dataset_path, "...")
            instances_csv = pd.read_csv(dataset_path, header=0)
        except:
            pass
            print("fail to read/load local csv file.")
            return


        #print("instances head 5:", instances_csv.head(5))
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        which_class = 1
        csv_data_1 = instances_csv[instances_csv['class'] == which_class]

        x = csv_data_1['location_X']
        y = csv_data_1['location_Y']
        z = csv_data_1['radius_inflation']
        ax.scatter(x, y, z, color='blue', marker='o')

        which_class = 2
        csv_data_2 = instances_csv[instances_csv['class'] == which_class]

        x = csv_data_2['location_X']
        y = csv_data_2['location_Y']
        z = csv_data_2['radius_inflation']
        ax.scatter(x, y, z, color='red', marker='o')

        ax.set_xlabel('x axis (metre)')
        ax.set_ylabel('y axis (metre)')
        ax.set_zlabel('modifier axis (metre)')

        ax.view_init(15, -90)
        plt.draw()
        acc_png_complete_name = self.result_path + "3_dataSet_distribution_yAxis.png"
        fig.savefig(acc_png_complete_name)
        plt.pause(0.01)

        ax.view_init(15, 180)
        plt.draw()
        acc_png_complete_name = self.result_path + "2_dataSet_distribution_xAxis.png"
        fig.savefig(acc_png_complete_name)
        plt.pause(0.01)

        ax.zaxis.set_rotate_label(False)
        ax.set_zlabel('modifier axis (metre)', rotation=-30)

        ax.view_init(89, -90)
        plt.draw()
        acc_png_complete_name = self.result_path + "1_dataSet_distribution_top.png"
        fig.savefig(acc_png_complete_name)
        plt.pause(0.01)
        plt.close()
        print("finish drawing figures of data instances distribution. ")
        # plt.show()
        """
        for angle in range(0, 360):
            ax.view_init(30, angle)
            plt.draw()
            plt.pause(.001)        
        """


    def get_classifiers(self, classifiers_data_path, act):
        classifiers_data = pd.read_csv(classifiers_data_path, header=0)  # this line for task 1
        " PATTERN ONE"
        classifiers_acc = classifiers_data[classifiers_data["ACT"] == act]
        classifiers_acc = classifiers_acc[classifiers_acc["ACC"] >= 0.95]
        classifiers_acc = classifiers_acc[classifiers_acc["PRD"] >= 950]
        classifiers_acc = classifiers_acc[classifiers_acc["EXP"] >= 200]
        classifiers_acc = classifiers_acc[classifiers_acc["NUM"] >= 15]
        print("classifier", classifiers_acc)

        return classifiers_acc

    def create_classifier_distribution_img(self, data, act="one"):
        print("create_classifier_distribution_img")

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        xpos = data['C1L'].values
        ypos = data['C2L'].values
        zpos = data['C3L'].values
        # zpos = data['ACC1'].values
        # zpos = (np.ones_like(xpos)) * 0.2

        dx = (data['C1H'] - data['C1L']).values
        dy = (data['C2H'] - data['C2L']).values
        dz = (data['C3H'] - data['C3L']).values
        # dz = (np.ones_like(dx))*0.0111

        if act == "one":
            ax.bar3d(xpos, ypos, zpos, dx, dy, dz, zsort='average', color='b')
        else:
            ax.bar3d(xpos, ypos, zpos, dx, dy, dz, zsort='average', color='g')

        ax.set_xlabel('x axis (metre)')
        ax.set_ylabel('y axis (metre)')
        ax.set_zlabel('modifier axis (metre) ')
        #ax.set_xlim(-15, 10)
        #ax.set_ylim(-10, 15)
        """
        c1l = boundaries.loc[0, 0] - 0.1
        c1h = boundaries.loc[0, 1] + 0.1
        c2l = boundaries.loc[0, 2] - 0.1
        c2h = boundaries.loc[0, 3] + 0.1

        ax.set_xlim(c1l - 0.1, c1h + 0.1)
        ax.set_ylim(c2l - 0.1, c2h + 0.1)        
        """
        ax.set_xlim(- 0.1, 3 + 0.1)
        ax.set_ylim(-1.5 - 0.1, 1.5 + 0.1)
        ax.set_zlim(0, 1)

        # print(xpos)
        # print(ypos)
        # print(zpos)
        # ax.view_init(15,  180)
        # ax.view_init(15, -75)

        """
        ax.view_init(15, 90)
        # ax.view_init(15, 0)
        plt.draw()
        plt.pause(0.8)
        acc_png_complete_name = self.result_path + act + "_" + self.classifiers_file+"_along_y_5.png"
        # acc_png_complete_name = self.report_path + "task_14_control_along_y.png"
        fig.savefig(acc_png_complete_name)
        plt.pause(0.01)


        ax.view_init(15, -180)
        plt.draw()
        plt.pause(0.8)
        acc_png_complete_name = self.result_path + act + "_" + self.classifiers_file+"_along_x_2.png"
        # acc_png_complete_name = self.report_path + "task_14_control_along_x.png"
        fig.savefig(acc_png_complete_name)
        plt.pause(0.01)


        ax.view_init(15, 0)
        plt.draw()
        plt.pause(0.8)
        acc_png_complete_name = self.result_path + act + "_" + self.classifiers_file+ "_-180_along_x_3.png"
        # acc_png_complete_name = self.report_path + "task_14_control_along_x.png"
        fig.savefig(acc_png_complete_name)
        plt.pause(0.01)

        ax.view_init(15, -90)
        # ax.view_init(15, -180)
        plt.draw()
        plt.pause(0.8)
        # acc_png_complete_name = self.report_path + "task_14_mitosis_-180_along_y.png"
        acc_png_complete_name = self.result_path + act + "_" + self.classifiers_file+"_-180_along_y_4.png"
        fig.savefig(acc_png_complete_name)
        plt.pause(0.01)
        """

        ax.zaxis.set_rotate_label(False)
        ax.set_zlabel('modifier axis (metre)', rotation=30)
        ax.view_init(90, -90)
        plt.draw()
        plt.pause(0.8)
        acc_png_complete_name = self.result_path + act + "_" + self.classifiers_file+ "_top_1.png"
        fig.savefig(acc_png_complete_name)

        #plt.show()
        plt.close()



        # for angle in range(0, 360, 5):
        # ax.view_init(15, angle-75)
        # plt.draw()
        # plt.pause(0.8)
        # acc_png_complete_name = self.png_path + "GW_room_Mitosis_" + str(angle) + ".png"
        # fig.savefig(acc_png_complete_name)
        # plt.pause(0.01)

        # plt.show()


    def draw_classifiers(self, classifiers_data_path):

        for act in ["one", "two"]:

            classifiers = self.get_classifiers(classifiers_data_path, act)
            print(" acc classifiers total line:", classifiers.shape[0])
            if classifiers.shape[0] >0:
                self.create_classifier_distribution_img(classifiers, act)


