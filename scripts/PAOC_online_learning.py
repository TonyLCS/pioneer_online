#!/usr/bin/env python

import sys
import rospy


from xcs_api import XCS  # import xcs agent API
from visualization import Visualization  # import Visualization to show results
import random
import csv
import pandas as pd
import numpy as np

from robot_nav_plan.srv import *



def XCS_agent_initiate(classifiers_population = None):

    # Initialize game replay object
    xcs_model = XCS()
    xcs_model.set_parameters()
    xcs_model.XCS_initiate_fiters_API()
    #xcs_model.XCS_initiate_test_fiters_API()
    #xcs_model.XCS_initiate_mcts_fiters_API()

    xcs_model.set_learning_mode()
    if classifiers_population is not None:
        xcs_model.XCS_loading_classifiers_population_from_csv(classifiers_population) # loading classifiers [P]

    return xcs_model


def XCS_agent_test_mode_initiate(classifiers_population="lastest"):

    # Initialize game replay object
    xcs_model = XCS()
    xcs_model.set_parameters()
    xcs_model.XCS_initiate_test_fiters_API()
    xcs_model.set_testing_mode()

    xcs_model.XCS_loading_classifiers_population_from_csv(classifiers_population)

    return xcs_model


def xcs_single_step_iteration(xcs_model, iter_index, goto_online_dataset):

    "get data"
    if goto_online_dataset:
        "goto remote service"
        header_token_seq = 0  # 0:return random instance in dataset, 1: return index_NO.x in dataset"
        index = 0
        perception, groundTrue, dataset_length = get_perception_client(header_token_seq, index) # online learning
    else:
        "go to local file"
        #path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/1_PAOC_C.csv"
        perception, groundTrue = get_perception_localFile(offline_path_dataset) #offline learning

    "go to a step iteration"
    action_xcs = xcs_model.XCS_get_prediction_API(perception)
    print("print action_xcs:", action_xcs)

    "reward single step"
    if action_xcs == groundTrue:
        reward_episode = 1000
    else:
        reward_episode = 0

    reward_method = "even"
    xcs_model.XCS_update_reward_API(reward_episode, reward_method, True,
                                    0.2)  # discount_big_end, even. dynamic learning rate

    "save classifiers population"
    xcs_model.XCS_save_classifiers_API(iter_index)

    return xcs_model


def xcs_testing_iteration(classifiers_population):
    "initiate xcs agent"
    xcs_testing_model = XCS_agent_test_mode_initiate(classifiers_population)

    #path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/1_PAOC_C.csv"

    "read length of dataset"
    if goto_online_dataset:  # in xcs_testing_iteration
        "goto remote service"
        header_token_seq = 0  # 0:return random instance in dataset, 1: return index_NO.x in dataset"
        index = 0
        perception, groundTrue, dataset_length = get_perception_client(header_token_seq, index)  # online test
    else:
        dataset_length = get_length_of_dataset(offline_path_dataset)

    correct_count = 0
    wrong_count = 0
    wrong_instance_list = []
    correct_rate = 0
    "goto iteration loop"
    for instance_index in range(1, dataset_length): #!! start from row NO.1 , dataset_length contains csv header!, but range is [1, 901)
        "get data"
        # perception, groundTrue = get_perception_client() #online
        #perception, groundTrue = get_perception_localFile(offline_path_dataset, False, instance_index)  # offline

        #goto_online_dataset = False

        "get data"
        if goto_online_dataset:
            "goto remote service"
            header_token_seq = 1  # 0:return random instance in dataset, 1: return index_NO.x in dataset"
            index = instance_index
            perception, groundTrue, dataset_length = get_perception_client(header_token_seq, index)  # online test
        else:
            "go to local file"
            perception, groundTrue = get_perception_localFile(offline_path_dataset, False, instance_index)  # offline test

        "go to a step iteration"
        action_xcs = xcs_testing_model.XCS_get_prediction_API(perception)

        "check prediction"
        if groundTrue == float(action_xcs):
            correct_count = correct_count + 1
        else:
            wrong_count = wrong_count + 1
            wrong_instance_list.append(instance_index)

        print(" ")

    correct_rate = correct_count/dataset_length
    print("correct_count:", correct_count, "; correct_rate:", correct_rate)
    print("wrong instance:", wrong_instance_list)
    return correct_rate, wrong_instance_list


def publich_client(header_token_seq, index):
    rospy.wait_for_service('PAOC_online_reading_record_service')
    try:
        ask_to_publich = rospy.ServiceProxy('PAOC_online_reading_record_service', PAOCPerceptionSRV)
        resp1 = ask_to_publich(header_token_seq, index)
        return resp1
    except rospy.ServiceException:
        print("Service call failed: %s", rospy.ServiceException)
        #print "Service call failed: %s"%e


def get_perception_client(header_token_seq, index):
    #seq = 0
    #modifier_request_value = 0.3
    print("get_perception_client header_token_seq, index:", header_token_seq, index)
    resp = publich_client(header_token_seq, index)
    preception = [resp.location_X, resp.location_Y, resp.radius_inflation]
    groundTrue = resp.groundTrue
    dataset_length = resp.dataset_length
    print("get perception and groundTrue: ", resp)
    return preception, groundTrue, dataset_length

def get_length_of_dataset(path_dataset):
    data_record = pd.read_csv(path_dataset)
    print("data_record total line:", data_record.shape[0])
    return data_record.shape[0]

def get_a_row_from_dataset(random_mode, row_index, path_dataset):
    #path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/1_PAOC_C.csv"
    data_record = pd.read_csv(path_dataset)
    #print("data_record total line:", data_record.shape[0])

    if random_mode:
        "toselect random one from data_record.shape[0] "
        row_index = np.random.randint(data_record.shape[0])

     #   a = np.random.choice(np.arange(0, maze_iter.getActionSize()), p=probobility_of_actions_normal)
        # print("3. likely action(", a, ") with pai2 (", pai2, ")")
     #   a = np.random.randint(maze_iter.getActionSize())  # todo:remove this line

    print("data_record a row :",  row_index, " from length:",data_record.shape[0])
    #print("data_record location_Y:", data_record.loc[row_index, "location_Y"])
    #print("data_record location_X:", data_record.iloc[row_index, 0])

    #perception = pd.DataFrame(columns=['location_X', 'location_Y', 'location_Y', 'class'])
    print("perception :", data_record.loc[row_index])
    return data_record.loc[row_index]


def get_perception_localFile(path_dataset, random_mode=True, row_index=0):
    data_row = get_a_row_from_dataset(random_mode, row_index, path_dataset)
    perception = [data_row.loc["location_X"], data_row.loc["location_Y"], data_row.loc["radius_inflation"]]
    groundTrue = data_row.loc["class"]
    print("==>", perception)
    print("type of groundTrue", type(groundTrue))
    return perception, groundTrue


if __name__ == "__main__":
    #classifiers_population_test = "lastest"
    #Visualization(classifiers_population_test)

    # local offline PAOC dataset
    goto_online_dataset = True
    offline_path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/1_PAOC_C.csv"
    accuracy_record = pd.DataFrame(columns=("iter", "correct_rate"))
    "new list"
    with open("wrong_instance_list.csv", 'w', newline='') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow("wrong_instance_list")


    # """
    "++++++++++++++++++++++++++++++++++++"
    " offline learning"
    "++++++++++++++++++++++++++++++++++++"
    "initiate xcs agent"
    classifiers_population = "instance100" # local file
    classifiers_population = None
    xcs_model = XCS_agent_initiate(classifiers_population)
    
    "goto iteration loop"
    testing_frequence = 10000 # this value should match to the parameter setting in TaskParameters file in Dataset path.
    #for iter_index in range(1, 500002):
    iter_index = 0
    while True:
        if iter_index > 50000000000:
            iter_index = 0
        iter_index = iter_index + 1

        xcs_model = xcs_single_step_iteration(xcs_model, iter_index, goto_online_dataset)
        xcs_model.XCS_deletion_API(9000) # pop_size =9000

        if iter_index%testing_frequence == 1 and iter_index > 10:
            "goto testing mode"
            print("************************************************")
            print("************************************************")
            print("***************go test***********************")
            print("************************************************")
            print("************************************************")

            
            #classifiers_population_test = "lastest"
            classifiers_population_test = "instance"+str(iter_index//testing_frequence)
            correct_rate, wrong_instance_list = xcs_testing_iteration(classifiers_population_test)
            accuracy_record.loc[iter_index//testing_frequence -1, "iter"] = iter_index//testing_frequence
            accuracy_record.loc[iter_index//testing_frequence -1, "correct_rate"] = correct_rate
            #accuracy_record.iloc[iter_index//10000-1, "wrong_instance_list"] = pd.Series(wrong_instance_list)

            with open("wrong_instance_list.csv", 'a', newline='') as f:
                writer = csv.writer(f, delimiter=',')
                writer.writerow(wrong_instance_list)

            print("************************************************")
            print("************************************************")
            print("accuracy_record")
            print(accuracy_record)
            "save testing result"
            accuracy_record.to_csv("accuracy_record.csv", encoding='utf-8', index=False)

            # """
            print("************************************************")
            print("***************go Visualization***********************")
            print("************************************************")
            classifiers_population_test = "instance"+str(iter_index//testing_frequence)
            Visualization(classifiers_population_test)


    
    """

    "++++++++++++++++++++++++++++++++++++"
    "online learning"
    "++++++++++++++++++++++++++++++++++++"

    "initiate xcs agent"
    xcs_model = XCS_agent_initiate()

    learning_flag = True
    iter_index = 0
    while learning_flag:

        xcs_model = xcs_single_step_iteration(xcs_model, iter_index)

        iter_index = iter_index + 1

        if iter_index > 10001:
            "goto testing mode"
            xcs_testing_iteration() #TODO: ONLINE METHOD
            Visualization()
            iter_index = 0
    """



