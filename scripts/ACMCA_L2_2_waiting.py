#!/usr/bin/env python

import sys
import rospy
import time
import math
import os
import copy
import csv
import pandas as pd

#   from xcs_api import XCS  # import xcs agent API
#   from visualization import Visualization  # import Visualization to show results
import random
import csv
#   import pandas as pd
#   import numpy as np

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64, Int64, Bool

from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist

from sensor_msgs.msg import LaserScan

from nav_msgs.msg import Odometry

from robot_nav_plan.srv import *


from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io, feature
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped

import subprocess

from xcs_api import XCS  # import xcs agent API

"#################################################"
"###########   XCS agents     ########"
"#################################################"

def XCS_agent_initiate(task_path=None, classifiers_population=None):

    # Initialize game replay object
    xcs_model = XCS()
    xcs_model.set_parameters(task_path)
    xcs_model.XCS_initiate_fiters_API()
    #xcs_model.XCS_initiate_test_fiters_API()
    #xcs_model.XCS_initiate_mcts_fiters_API()

    xcs_model.set_learning_mode()

    if classifiers_population is not None:
        xcs_model.XCS_loading_classifiers_population_from_csv(classifiers_population) # loading classifiers [P]

    return xcs_model

"#################################################"
"###########   callbacks                  ########"
"#################################################"

def calculate_length_of_path(path):
    global path_length
    path_length = len(path.poses)
    print("show path_planner's path :", path_length)

def callback_receiv_reward(msg):
    global flag_receive_reward
    global reward_episode
    flag_receive_reward = True
    reward_episode = msg.data
    print("receiving reward_episode:", reward_episode)

def callback_start_xcs_iteration(msg):
    global start_xcs_iteration
    global time_delay_from_expectation

    start_xcs_iteration = msg.data
    print("start xcs iteration, start_xcs_iteration: ",start_xcs_iteration)

def callback_waiting_perception(msg):
    global waiting_perception
    global has_waiting_perception

    waiting_perception = msg.data
    has_waiting_perception = True


if __name__ == "__main__":
    """
    continue to the existing plan or switch to a new plan and new target
    input :     perception 
                A1: time taken; A2: waiting time; A3: Blocking recognision or A4 (path available) 
    output :    
                action 1: waiting time length
                action 2: continue; B2 new plan 
                action 3: quit
    """

    "##############################"
    "initiate ros interactions"
    "##############################"
    rospy.init_node("acmca_l2_waiting", anonymous=False)

    "#############################"
    "initiate xcs agent"
    "#############################"
    classifiers_population = None
    "/reflex_hire_l_OnlyBumper/lastest"
    task_path = os.path.join(os.getcwd(), '/Results/doThisTask2Block.csv')
    #xcs_model = XCS_agent_initiate("/Results/doThisTask.csv", "/reflex_node_l/lastest")
    #xcs_model = XCS_agent_initiate(task_path, None)
    "src/robot_nav_plan/result/waiting_node_l/"
    #xcs_model = XCS_agent_initiate(task_path, "/waiting_node_l/lastest")
    xcs_model = XCS_agent_initiate(task_path, "/waiting_node_l/instance134")
    #xcs_model = XCS_agent_initiate(task_path, None)

    rospy.sleep(3)

    "#############################"
    "activate iteration"
    "#############################"
    rospy.Subscriber('/p3dx/ACMCA/task_iteration/status', Bool, callback_start_xcs_iteration)


    "#############################"
    " perception"
    "#############################"
    "active  xcs iteration by callback_XCS_iteration"


    time_delay_from_expectation = 0  # A1
    #rospy.Subscriber('/p3dx/ACMCA/expectation/difference', Float64, callback_XCS_iteration)  # "activate"

    time_to_wait = 0# np.random.randint(1, 5)       # A2

    path_length = 0     # A3
    #rospy.Subscriber("/p3dx/path_planner_homeo/navfn/plan", Path, calculate_length_of_path)

    block_recognision = 0   # A4
    "cnn"

    #waiting_perception = (time_delay_from_expectation, time_to_wait, path_length, block_recognision)

    waiting_perception = tuple()
    has_waiting_perception = False
    rospy.Subscriber('/p3dx/ACMCA/L1_waiting_perception/waiting_perception', Float64MultiArray, callback_waiting_perception)
    "#############################"
    "for effect"
    "#############################"
    "Float64MultiArray = (time_to_wait, action)"
    action_pattern_pub = rospy.Publisher('/p3dx/ACMCA/L2_waiting/action', Float64MultiArray, queue_size=1) #in pioneer_take_action


    "#############################"
    "update reward"
    "#############################"
    " touching stimulus is for update reward of reflex action"
    rospy.Subscriber('/p3dx/ACMCA/task_iteration/time_consumption', Float64, callback_receiv_reward)



    #complete_a_behaviour = False
    #rospy.Subscriber('/p3dx/ACMCA/L5_waiting/complete', Bool, callback_complete)


    print("acmca_l2_waiting spin...")
    start_xcs_iteration = False
    xcs_iteration_in_progress = False
    xcs_iter_index = 0

    while not rospy.is_shutdown():
        #complete_a_behaviour = False # is true by l5 feedback

        if start_xcs_iteration:
            start_xcs_iteration = False
            xcs_iteration_in_progress = True # this will block other touching_perception.

            "perception"
            #time_to_wait = np.random.randint(1, 5)  # A2
            #waiting_perception = (time_delay_from_expectation, time_to_wait, path_length, block_recognision)
            #has_waiting_perception =False
            print("waiting for waiting_perception")
            while not has_waiting_perception:
                rospy.sleep(0.1)

            print("waiting perception: ", waiting_perception)
            "#####################################"
            "######  select reflex action  #######"
            "#####################################"

            print("##############XCS action#########################")
            action_xcs = xcs_model.XCS_get_prediction_API(waiting_perception)
            print("==>> waiting action:", action_xcs)
            print("##############XCS action#########################")

            "#####################################"
            "#########  has effect  ##############"
            "#####################################"
            msg =Float64MultiArray()
            time_to_wait = waiting_perception[1]
            msg.data = (time_to_wait, action_xcs)
            action_pattern_pub.publish(msg)
            print("\n")
            print("has obstacle occupied:", waiting_perception[0], " and  ==> publish: time_to_wait=", time_to_wait, ";  action_xcs=", action_xcs)
            #rospy.sleep(time_to_wait)

            #print("publish reflex action and sleep for 2.5 second:", action_xcs)
            #rospy.sleep(5.5)

            "#####################################"
            "#########  update xcs  ##############"
            "#####################################"
            flag_receive_reward = False
            reward_episode = 0
            print("waiting for reward_episode from ACMCA_L1_4_task_time_calculator")
            while not flag_receive_reward:
                rospy.sleep(0.1)

            reward_method = "even"
            xcs_model.XCS_update_reward_API(reward_episode, reward_method, True,
                                            0.2)  # discount_big_end, even. dynamic learning rate

            "end of update xcs"

            "save classifiers population"
            print("os.getcwd:", os.getcwd())
            save_model_dir = os.path.join(os.getcwd(), 'src/robot_nav_plan/result/waiting_node_l/')
            if not os.path.isdir(save_model_dir):
                os.makedirs(save_model_dir)
            xcs_model.XCS_save_classifiers_API(xcs_iter_index, save_model_dir)
            #xcs_model.XCS_save_classifiers_API(xcs_iter_index, "./src/robot_nav_plan/result/waiting_node_l/")
            xcs_model.XCS_deletion_API(900)  # pop_size =9000
            xcs_iter_index = xcs_iter_index + 1
            start_xcs_iteration = False
            xcs_iteration_in_progress = False #unlock
            has_waiting_perception =False

            print("  end of an iteration")
            print("\n")
            print("\n waiting...\n")
            #rospy.sleep(1)

