#!/usr/bin/env python

import sys
import rospy
import time
import math
import os
import copy
import csv
import pandas as pd

#   from xcs_api import XCS  # import xcs agent API
#   from visualization import Visualization  # import Visualization to show results
import random
import csv
#   import pandas as pd
#   import numpy as np

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64, Int64, Bool

from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist

from sensor_msgs.msg import LaserScan

from nav_msgs.msg import Odometry

from robot_nav_plan.srv import *


from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io, feature
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped

import subprocess

from xcs_api import XCS  # import xcs agent API

print("rospy path:", rospy.__file__)
#print("skimage feature path:", feature.__file__)

"#################################################"
"########### Pioneer action ######################"
"#################################################"


def callback_bumper(msg):
    global touching_perception_stock
    current_time = rospy.get_rostime()  # math.floor(rospy.get_rostime().secs)
    is_collision = 0
    for a_bamper_data in msg.data:
        if a_bamper_data > 0.5:
            is_collision = 1
    to_append = pd.DataFrame([[current_time, is_collision]], columns=("time", "collision"))
    touching_perception_stock = touching_perception_stock.append(to_append)
    touching_perception_stock = touching_perception_stock.loc[touching_perception_stock["time"] > (current_time - rospy.Duration(10))]

def pioneer_take_action(pioneer_take_action):
    " publich 6 actions"
    #pub = rospy.Publisher('/p3dx/cmd_vel', Twist, queue_size=5)
    global pub_modifier_cmd
    global flag_to_active_path_planning_cmd #to activate output of path planning
    global reflex_action #get input the secondary node reflex and XCS

    # test=> pioneer_take_action = np.random.randint(7, 13)
    cmd = set_vel(0, 0)  # default

    "for reflex process"
    if pioneer_take_action == 1:
        cmd = set_vel(0.1, 0)
    if pioneer_take_action == 2:
        cmd = set_vel(0.05, -0.32) # right
    if pioneer_take_action == 3:
        cmd = set_vel(0.05, 0.32) # left

    if pioneer_take_action == 4:
        cmd = set_vel(-0.1, 0)
    if pioneer_take_action == 5:
        cmd = set_vel(-0.05, 0.32)
    if pioneer_take_action == 6:
        cmd = set_vel(-0.05, -0.32)

    "for random process"
    if pioneer_take_action == 7:
        cmd = set_vel(0.15, 0)
    if pioneer_take_action == 8:
        cmd = set_vel(0.1, -0.32) # right
    if pioneer_take_action == 9:
        cmd = set_vel(0.1, 0.32) # left

    if pioneer_take_action == 10:
        cmd = set_vel(-0.15, 0)
    if pioneer_take_action == 11:
        cmd = set_vel(-0.1, 0.32)
    if pioneer_take_action == 12:
        cmd = set_vel(-0.1, -0.32)


    "stop"
    if pioneer_take_action == 100:
        cmd = set_vel(0, 0)
        print("pioneer set cmd = set_vel(0, 0)")

    "go ahead anyway"
    if pioneer_take_action == 101:
        cmd = set_vel(0.5, 0)

    "go spin right"
    if pioneer_take_action == 102:
        cmd = set_vel(0, -0.32)

    "go spin left"
    if pioneer_take_action == 103:
        cmd = set_vel(0, 0.32)

    "pathplanning"
    if pioneer_take_action == 501:
        "pathplanning: will be take effect in republish cmd in callback function through flag_to_active_path_planning_cmd"
        print("Pionner will take pathplanning execution:")
        flag_to_active_path_planning_cmd = True
        return
    else:
        flag_to_active_path_planning_cmd = False

    "reflex"
    if pioneer_take_action == 502:
        print("Pionner will take reflex execution:", reflex_action)
        "for reflex process"
        if reflex_action == 1:
            cmd = set_vel(0.1, 0)
        if reflex_action == 2:
            cmd = set_vel(0.05, -0.32)  # right
        if reflex_action == 3:
            cmd = set_vel(0.05, 0.32)  # left

        if reflex_action == 4:
            cmd = set_vel(-0.1, 0)
        if reflex_action == 5:
            cmd = set_vel(-0.05, 0.32)
        if reflex_action == 6:
            cmd = set_vel(-0.05, -0.32)
        if reflex_action == 0:
            cmd = set_vel(0, 0)


    "##################"
    "take effect"
    "##################"
    pub_modifier_cmd.publish(cmd)
    rate = rospy.Rate(15)
    pub_iter = 0
    while pub_iter < 3:

        print(" publiching pioneer_take_action:", pioneer_take_action)
        pub_modifier_cmd.publish(cmd)
        rate.sleep()
        pub_iter += 1

    "###############################"
    "reset first_collision_flag"
    "###############################"
    #first_collision_flag = False
    #rospy.sleep(0.2)

    "##################"
    "stop"
    "##################"
    #cmd = set_vel(0, 0)
    #cmd_pub.publish(cmd)

def set_vel(vel_x, vel_z):
    cmd = Twist()
    cmd.linear.x = vel_x
    cmd.linear.y = 0
    cmd.linear.z = 0
    cmd.angular.x = 0
    cmd.angular.y = 0
    cmd.angular.z = vel_z
    return cmd



"#################################################"
"############  modifiers  #######################"
"#################################################"

def do_freeze_process():
    print("#################################################")
    print("do_freeze_process")
    pioneer_take_action(100)
    pass

def do_go_ahead_anyway_process():
    print("#################################################")
    print("do_go_ahead_anyway_process for 5 seconds")
    pioneer_take_action(101)
    rospy.sleep(2)
    print("do_freeze_process")
    pioneer_take_action(100)
    pass

def do_spin_right_process():
    print("#################################################")
    print("do_spin_right_process for 5 seconds")
    pioneer_take_action(102)
    rospy.sleep(2)
    print("do_freeze_process")
    pioneer_take_action(100)
    pass

def do_spin_left_process():
    print("#################################################")
    print("do_spin_left_process for 5 seconds")
    pioneer_take_action(103)
    rospy.sleep(2)
    print("do_freeze_process")
    pioneer_take_action(100)
    pass

def do_random_action_process():
    #print("do_random_action_process")

    "############################################"
    "pioneer take random action"
    "############################################"
    #print("not collisions")
    random_action = np.random.randint(0, 100)
    if random_action < 30:
        print("#################################################")
        print(" do_random_action_process, randomly take action 1\n")
        pioneer_take_action(7)  # go ahead for 10 seconds
        #rospy.sleep(1)
    if 30 <= random_action < 60:
        print("#################################################")
        print(" do_random_action_process randomly take action 4\n")
        pioneer_take_action(10)  # go back for 10 seconds
        #rospy.sleep(1)
    if 60 <= random_action < 70:
        print("#################################################")
        print(" do_random_action_process randomly take action 2\n")
        pioneer_take_action(8)
        #rospy.sleep(1)
    if 70 <= random_action < 80:
        print("#################################################")
        print(" do_random_action_process, randomly take action 3\n")
        pioneer_take_action(9)
        #rospy.sleep(1)
    if 80 <= random_action < 90:
        print("#################################################")
        print(" do_random_action_process, randomly take action 5\n")
        pioneer_take_action(11)
        #rospy.sleep(1)
    if 90 <= random_action <= 100:
        print("#################################################")
        print(" do_random_action_process, randomly take action 6\n")
        pioneer_take_action(12)
        #rospy.sleep(1)

    rospy.sleep(2)
    #print("stop random")
    #pioneer_take_action(100)
    pass

def do_pathPlanning_execution():
    print("#################################################")
    print("follow path planning schedule")
    pioneer_take_action(501)

def do_reflex_execution():
    global pub_reflex_complete
    global touching_perception_stock

    print("#################################################")
    print("do reflex execution")
    pioneer_take_action(502) #reflex
    touching_perception_stock = touching_perception_stock.iloc[0:0]
    print("touching_perception_stock", touching_perception_stock["collision"])
    rospy.sleep(5)

    "end of reflex effect"
    "publish completement for reflex node to update its effect"
    pub_reflex_complete.publish(True)
    bumper_fire = touching_perception_stock.loc[touching_perception_stock["collision"] == 1].shape[0]
    reflex_effect_pub.publish(bumper_fire)
    print("###########################################################")
    print(">>>>>>>>>>>>>>>>effect of reflex: ", bumper_fire)
    print("###########################################################")
    print("end of  an reflex behaviour on Pioneer! ")
    #pioneer_take_action(100)
    #print("stop reflex")


"#################################################"
"############# modifier list ##########################"
"############## action pattern #######################"
modifier_action_process = {
    0: do_freeze_process,
    1: do_pathPlanning_execution,
    2: do_reflex_execution,
    3: do_go_ahead_anyway_process,
    4: do_spin_right_process,
    5: do_spin_left_process,
    6: do_random_action_process,

}
"""
3 callback from subscription
"""

def callback_pathPlanning(msg):
    "remap path planning as the source of cmd"
    global flag_to_active_path_planning_cmd
    global pub_modifier_cmd
    #print("subscribe path planner cmd:", msg)
    if flag_to_active_path_planning_cmd:
        pub_modifier_cmd.publish(msg)

def callback_pioneer_action(msg):
    which_cmd_action = msg.data
    print("which_cmd_action= ", which_cmd_action)
    modifier_action_process.get(which_cmd_action, 0)()

def callback_reflex(msg):
    global reflex_action
    reflex_action = msg.data
    print("receive reflex_action= ", reflex_action)

if __name__ == "__main__":
    "input: '/p3dx/ACMCA/action_modifier/action'"
    " output: execute action required by the input."
    rospy.init_node("acmca_l5_1_action_modifier", anonymous=False)
    flag_to_active_path_planning_cmd = False # if true, remap cmd_Vel from the source of path planning
    reflex_action = 0  # default

    rospy.Subscriber('/p3dx/ACMCA/action_modifier/action', Int64, callback_pioneer_action)
    rospy.Subscriber('/p3dx/Multiplexer_PathPlanner/cmd_vel', Twist, callback_pathPlanning) # subscrib path planning
    rospy.Subscriber('/p3dx/ACMCA/L4_reflex/action', Int64, callback_reflex) # subscrib path planning
    pub_modifier_cmd = rospy.Publisher('/p3dx/cmd_vel', Twist, queue_size=5) #in pioneer_take_action
    pub_reflex_complete = rospy.Publisher('/p3dx/ACMCA/L5_reflex/complete', Bool, queue_size=1) #in pioneer_take_action

    touching_perception_stock = pd.DataFrame(columns=("time", "collision"))
    rospy.Subscriber('/p3dx/bumper_state_date', Float64MultiArray, callback_bumper)
    reflex_effect_pub = rospy.Publisher('/p3dx/ACMCA/L5_reflex/effect', Int64, queue_size=1) #reflex action





    print("acmca_l5_action_modifier :")
    for key in modifier_action_process.keys():
        print(modifier_action_process[key], ":", key, "\n")
    print("spin...")
    rospy.spin()





