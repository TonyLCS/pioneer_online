#!/usr/bin/env python
"""
This program is to test the response of the Reflex.
It load the XCS agent trained for the reflex process.
The perception also comes from the value attributes in the classifiers popluation.
When each perception is given to the XCS agent, the XCS agent will make a prediction (action).
Both the perception and the the action will show in a plot.
"""
import sys
import rospy
import time
import math

from xcs_api import XCS  # import xcs agent API
from visualization import Visualization  # import Visualization to show results
import random
import csv
import pandas as pd
import numpy as np

from geometry_msgs.msg import Twist
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry


from robot_nav_plan.srv import *

from geometry_msgs.msg import Twist
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray


import matplotlib.pyplot as plt



def get_perception_localFile(path_dataset, random_mode=False, row_index=0):
    data_row = get_a_row_from_dataset(random_mode, row_index, path_dataset)
    perception = [data_row.loc["C1V"], data_row.loc["C2V"], data_row.loc["C3V"], data_row.loc["C4V"], data_row.loc["C5V"], data_row.loc["C6V"], data_row.loc["C7V"], data_row.loc["C8V"], data_row.loc["C9V"], data_row.loc["C10V"]]
    #perception = [data_row.loc["Bumper_1"], data_row.loc["Bumper_2"], data_row.loc["Bumper_3"], data_row.loc["Bumper_4"], data_row.loc["Bumper_5"], data_row.loc["Bumper_6"], data_row.loc["Bumper_7"], data_row.loc["Bumper_8"], data_row.loc["Bumper_9"], data_row.loc["Bumper_10"]]

    groundTrue = data_row.loc["ACT"]
    print("==>", perception)
    print("type of groundTrue", type(groundTrue))
    return perception, groundTrue


def get_a_row_from_dataset(random_mode, row_index, path_dataset):
    #path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/1_PAOC_C.csv"
    data_record = pd.read_csv(path_dataset)
    #print("data_record total line:", data_record.shape[0])

    if random_mode:
        "toselect random one from data_record.shape[0] "
        row_index = np.random.randint(data_record.shape[0])

     #   a = np.random.choice(np.arange(0, maze_iter.getActionSize()), p=probobility_of_actions_normal)
        # print("3. likely action(", a, ") with pai2 (", pai2, ")")
     #   a = np.random.randint(maze_iter.getActionSize())  # todo:remove this line

    print("data_record a row :",  row_index, " from length:",data_record.shape[0])
    #print("data_record location_Y:", data_record.loc[row_index, "location_Y"])
    #print("data_record location_X:", data_record.iloc[row_index, 0])

    #perception = pd.DataFrame(columns=['location_X', 'location_Y', 'location_Y', 'class'])
    print("perception :", data_record.loc[row_index])
    return data_record.loc[row_index]



def XCS_agent_initiate(task_path=None, classifiers_population=None):

    # Initialize game replay object
    xcs_model = XCS()
    xcs_model.set_parameters(task_path)
    xcs_model.XCS_initiate_test_fiters_API()
    #xcs_model.XCS_initiate_fiters_API()


    #xcs_model.set_learning_mode()
    xcs_model.set_testing_mode()

    if classifiers_population is not None:
        xcs_model.XCS_loading_classifiers_population_from_csv(classifiers_population) # loading classifiers [P]

    return xcs_model


if __name__ == "__main__":

    "#################################"
    " load classifiers population"
    "#################################"
    relative_path = "/reflex_hire_l_temp/"
    classifiers_population = "lastest"
    task_path = "/Results/doThisTask.csv"

    xcs_model = XCS_agent_initiate(task_path, relative_path + classifiers_population)

    print("finish loading XCS agent")

    "#################################"
    " load local testing data"
    "#################################"
    offline_path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/reflex_hire_l_OnlyBumper/lastest.csv"
    length_data = pd.read_csv(offline_path_dataset).shape[0]
    print("length_data:", length_data)


    "#################################"
    " plot"
    "#################################"

    fig = plt.figure()
    ax = fig.add_subplot(111)
    "adding background"
    img = plt.imread("/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/bumpers.png")
    ax.imshow(img, extent=[0,5,-3,3])

    """
    "adding bumper signals"
    #ax.scatter(1, 1, color="blue", marker='o')

    x =pd.DataFrame(np.array([0.43, 1.34, 2.45, 3.52, 4.36,  0.8, 1.34, 2.45, 3.52, 4.36]))
    y =pd.DataFrame(np.array([1.9,  2.53, 2.6,  2.53, 1.9,  -1.8,  -2.4, -2.6,  -2.40, -1.68]))

    for a_bumper_index in range(0, 10):
        ax.scatter(x.loc[a_bumper_index], y.loc[a_bumper_index],  color="blue", marker='o')

    "add arrows"
    for action_xcs in range(1,7):

        if action_xcs == 1:
            arrow_direction_x = 0
            arrow_direction_y = 2
        if action_xcs == 2:
            arrow_direction_x = 1.5
            arrow_direction_y = 1.5
        if action_xcs == 3:
            arrow_direction_x = -1.5
            arrow_direction_y = 1.5

        if action_xcs == 4:
            arrow_direction_x = 0
            arrow_direction_y = -2
        if action_xcs == 5:
            arrow_direction_x = 1.5
            arrow_direction_y = -1.5
        if action_xcs == 6:
            arrow_direction_x = -1.5
            arrow_direction_y = -1.5

        ax.arrow(2.45, 0, arrow_direction_x, arrow_direction_y, width=0.05, head_width=0.2, head_length=0.2, fc='k', ec='g')

    plt.draw()
    plt.show()
    plt.close
    """

    for row_index in range(0, length_data):

        #row_index = 10
        perception, groundTrue = get_perception_localFile(offline_path_dataset, False, row_index)
        print("perception: ", perception)

        action_xcs = xcs_model.XCS_get_prediction_API(perception)

        print("action_xcs: ", action_xcs)


        "#################################"
        " plot"
        "#################################"
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_xticks([])
        ax.set_yticks([])

        img = plt.imread("/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/bumpers.png")
        ax.imshow(img, extent=[0, 5, -3, 3])

        x =pd.DataFrame(np.array([0.43, 1.34, 2.45, 3.52, 4.36,  0.8, 1.34, 2.45, 3.52, 4.36]))
        y =pd.DataFrame(np.array([1.9,  2.53, 2.6,  2.53, 1.9,  -1.8,  -2.4, -2.6,  -2.40, -1.68]))

        a_bumper_index = 0

        for a_bumper in perception:

            if a_bumper > 0.5:
                bumper_color = "red"
            else:
                bumper_color = "blue"

            ax.scatter(x.loc[a_bumper_index], y.loc[a_bumper_index],  color=bumper_color, marker='o')
            a_bumper_index = a_bumper_index + 1
        arrow_direction_x = 0
        arrow_direction_y = 0
        if action_xcs == 1:
            arrow_direction_x = 0
            arrow_direction_y = 2
        if action_xcs == 2:
            arrow_direction_x = 1.5
            arrow_direction_y = 1.5
        if action_xcs == 3:
            arrow_direction_x = -1.5
            arrow_direction_y = 1.5

        if action_xcs == 4:
            arrow_direction_x = 0
            arrow_direction_y = -2
        if action_xcs == 5:
            arrow_direction_x = 1.5
            arrow_direction_y = -1.5
        if action_xcs == 6:
            arrow_direction_x = -1.5
            arrow_direction_y = -1.5

        ax.arrow(2.45, 0, arrow_direction_x, arrow_direction_y, width=0.05, head_width=0.2, head_length=0.2, fc='k', ec='g')

        plt.draw()
        plt.pause(0.5)
        plt.close()

