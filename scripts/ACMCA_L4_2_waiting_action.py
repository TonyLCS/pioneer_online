#!/usr/bin/env python

import sys
import rospy
import time
import math
import os
import copy
import csv
import pandas as pd

#   from xcs_api import XCS  # import xcs agent API
#   from visualization import Visualization  # import Visualization to show results
import random
import csv
#   import pandas as pd
#   import numpy as np

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64, Int64, Bool

from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist

from sensor_msgs.msg import LaserScan

from nav_msgs.msg import Odometry

from robot_nav_plan.srv import *


from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io, feature
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped

import subprocess

from xcs_api import XCS  # import xcs agent API


"#################################################"
"########### targeted_position client ############"
"#################################################"

def targeted_position_client(header_token_seq, seq, targeted_position):
    "publich targeted_position and return current_position"
    rospy.wait_for_service('targeted_position_service')
    try:
        print("client call targeted_position_service")
        request_the_server = rospy.ServiceProxy('targeted_position_service', TargetedPositionMSG)
        resp_position = request_the_server(header_token_seq, seq, targeted_position)  # calls the server with a request
        return resp_position.current_position   # get current_slam position a response from the server
    except rospy.ServiceException:
        print("Service call failed: %s")

def combine_position(x, y, angle):
    a_targeted_postition = PoseStamped()
    a_targeted_postition.header.frame_id = 'map'
    a_targeted_postition.pose.position.x = x
    a_targeted_postition.pose.position.y = y
    a_targeted_postition.pose.position.z = 0

    #print "angle -deg = %s"%angle
    angle = radians(angle)
    #print "angle -rad = %s"%angle
    (a_targeted_postition.pose.orientation.x, a_targeted_postition.pose.orientation.y,
     a_targeted_postition.pose.orientation.z, a_targeted_postition.pose.orientation.w) = rotate(0, angle, 0)
    #print "a_targeted_postition.pose.orientation.z= %s, a_targeted_postition.pose.orientation.w= %s"%(a_targeted_postition.pose.orientation.z, a_targeted_postition.pose.orientation.w)
    return a_targeted_postition

def rotate(heading, attitude, bank):
    # Assuming the angles are in radians.

    c1 = cos(heading / 2)
    s1 = sin(heading / 2)
    c2 = cos(attitude / 2)
    s2 = sin(attitude / 2)
    c3 = cos(bank / 2)
    s3 = sin(bank / 2)
    c1c2 = c1 * c2
    s1s2 = s1 * s2

    w = c1c2 * c3 - s1s2 * s3
    x = c1c2 * s3 + s1s2 * c3
    y = s1 * c2 * c3 + c1 * s2 * s3
    z = c1 * s2 * c3 - s1 * c2 * s3
    return x, y, z, w

"#################################################"
"########### waiting action modifers  ############"
"#################################################"
def do_freeze_process():
    global action_pattern_pub
    action_pattern_pub.publish(0)


def do_quit_process():
    global action_pattern_pub
    print("cxxxx{|::::::::::::::::::::::> do_quit_process (xcs_action 3)")
    rospy.sleep(1)
    action_pattern_pub.publish(0)


def do_continuous_process():
    global action_pattern_pub
    print("cxxxx{|::::::::::::::::::::::> do_continuous_process (xcs_action 1)")
    targeted_position = combine_position(4, -0.3, 0)
    #targeted_position_client(1, 1, targeted_position)

    pub_new_target.publish(targeted_position)
    rospy.sleep(1)
    action_pattern_pub.publish(1)

def new_target_process():
    global action_pattern_pub
    global pub_new_target
    "publish new target"
    print("cxxxx{|::::::::::::::::::::::>  new_target_process (xcs_action 2)")
    targeted_position = combine_position(6, 0.5, 0)
    #targeted_position_client(1, 1, targeted_position)
    pub_new_target.publish(targeted_position)
    rospy.sleep(1)
    action_pattern_pub.publish(1)

def go_to_initial_position_process():
    global action_pattern_pub
    global pub_new_target
    "publish new target"
    print("cxxxx{|::::::::::::::::::::::>  goto initial_position ")
    targeted_position = combine_position(2, 0, 0)
    #targeted_position_client(1, 1, targeted_position)

    pub_new_target.publish(targeted_position)
    rospy.sleep(1)
    action_pattern_pub.publish(1)


modifier_action_process = {
    0: do_freeze_process,
    1: do_continuous_process,
    2: new_target_process,
    3: do_quit_process,
    4: go_to_initial_position_process,


}


def callback_waiting(msg):
    global action_pattern
    global reflex_action
    print("in callback_waiting")
    time_to_wait = msg.data[0]
    action_xcs = msg.data[1]
    # print("\n\n\n")
    print("###############################")
    print("in layer 4 ")
    print("publish: time_to_wait=", time_to_wait, ";  action_xcs=", action_xcs)
    print("###############################")

    if time_to_wait > 0.05:
        action_pattern_pub.publish(0)
        print("starting waiting for ", time_to_wait," s.")
        rospy.sleep(time_to_wait)
        print("wake up from the waiting")

    print("which_cmd_action= ", action_xcs)
    modifier_action_process.get(action_xcs, 0)()

    print("return callback waiting.")
    print("\n\n\n")



if __name__ == "__main__":
    """
        input: (time_to_wait, action_xcs)
        time_to_wait: 
            waiting time: hold freeze
        action_xcs: 
            specify modifier_action_process: 
                1: do_continuous_process,
                2: new_target_process,
                3: do_quit_process,
    """
    "output: '/p3dx/ACMCA/action_modifier/action'"
    rospy.init_node("acmca_l4_2_waiting_action", anonymous=False)
    waiting_action = -1
    rospy.Subscriber('/p3dx/ACMCA/L2_waiting/action', Float64MultiArray, callback_waiting) # subscrib path planning

    action_pattern_pub = rospy.Publisher('/p3dx/ACMCA/action_modifier/action', Int64, queue_size=1) #in pioneer_take_action
    pub_new_target = rospy.Publisher('/p3dx/path_planner/goal', PoseStamped, queue_size=1)  # set the goal for the path planner

    print("acmca_l4_2_waiting_action spin...")
    rospy.spin()


