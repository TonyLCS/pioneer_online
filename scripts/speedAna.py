import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import axes3d
import numpy as np
import pandas as pd

from matplotlib.collections import PolyCollection
from matplotlib import colors as mcolors
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from scipy import stats
import math

from mpl_toolkits.mplot3d.art3d import Poly3DCollection

class SpeedAna:

    result_path = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/"
    #result_path = "C:/Users/zzm/Documents/software/pioneer/result/"
    dataSet_file = "Speed3.csv"
    #classifiers_file = "lastest"#""instance100.csv"

    def __init__(self):
        print("Visualization active.")
        dataset_path = self.result_path + self.dataSet_file
        csv_data = self.loadcsv(dataset_path, header=0) # contain header
        #print("csv_data:", csv_data)

        statistic_analysis = pd.DataFrame(columns=("speed_request", "max", "min", "mean", "std", "num"))

        speed_matrix = []
        speed_list = []
        speed_draw_position = []
        for row_index in range(1, 80):
            speed_request = row_index * 0.05

            #speed_request = 1.2

            csv_data_sp_1 = csv_data[round(csv_data["speed_request"], 3) == round(speed_request, 3)]
            #print("csv_data_sp_1:", csv_data_sp_1)
            #print("min:", min(csv_data_sp_1["speed_feedback"]))
            #print("max:", max(csv_data_sp_1["speed_feedback"]))
            #print("mean:", csv_data_sp_1["speed_feedback"].mean())
            #print("std:", csv_data_sp_1["speed_feedback"].std())
            #print("num:", csv_data_sp_1["speed_feedback"].shape[0])

            if csv_data_sp_1["speed_feedback"].shape[0] >= 1:
                statistic_analysis.loc[row_index, "speed_request"] = speed_request
                statistic_analysis.loc[row_index, "min"] = csv_data_sp_1["speed_feedback"].min()
                statistic_analysis.loc[row_index, "max"] = csv_data_sp_1["speed_feedback"].max()
                statistic_analysis.loc[row_index, "mean"] = csv_data_sp_1["speed_feedback"].mean()
                statistic_analysis.loc[row_index, "std"] = csv_data_sp_1["speed_feedback"].std()
                statistic_analysis.loc[row_index, "num"] = csv_data_sp_1["speed_feedback"].shape[0]

                speed_list = csv_data_sp_1["speed_feedback"].values.tolist()
                speed_matrix.append(speed_list)
                speed_draw_position.append(round(speed_request, 3))
            else:
                pass
                # print("a failed speed_request:", speed_request, ", shape is: ", csv_data_sp_1["speed_feedback"].shape[0])
            #print("speed feedback:", csv_data_sp_1["speed_feedback"].values.tolist())


        #print(speed_matrix)
        #print("statistic_analysis")
        #print(statistic_analysis)

        #fig, (ax0, ax1) = plt.subplots()
        """
        #################draw version one########################
        yerror= [statistic_analysis["mean"]-statistic_analysis["min"], statistic_analysis["max"]-statistic_analysis["mean"]]

        fig = plt.figure(figsize=(8,8))
        plt.xlabel("reference speed (metre/second)")
        plt.ylabel("feedback speed (metre/second)")
        plt.plot([0, 0.4, 0.8, 1.4], [0, 0.4, 0.8, 1.4], linestyle=":")
        plt.plot([0, 0.4, 0.8, 1.4], [0.7, 0.7, 0.7, 0.7], linestyle=":")
        plt.errorbar(statistic_analysis["speed_request"], statistic_analysis["mean"], yerr=yerror, xerr=statistic_analysis["std"] , linestyle="None", fmt='.')

        fig_name = self.result_path + "speed.png"
        fig.savefig(fig_name)
        plt.show()
        
        """
        "#################draw violin version ##############################"
        fig, axes = plt.subplots(figsize=(6, 6))

        plt.xlabel("reference speed (metre/second)")
        plt.ylabel("feedback speed (metre/second)")
        plt.plot([0, 0.4, 0.8, 1.4], [0, 0.4, 0.8, 1.4], linestyle=":")
        plt.plot([0, 0.4, 0.8, 1.4], [0.7, 0.7, 0.7, 0.7], linestyle=":")

        axes.violinplot(dataset=speed_matrix,positions=speed_draw_position, widths=0.05, showmeans=True, showextrema=True, showmedians=False)
        fig_name = self.result_path + "speed3.png"
        fig.savefig(fig_name)
        plt.show()

    def loadcsv(self, dataset_path, header):
        # to del
        print("loading csv file", dataset_path, "...")
        csv_data = pd.read_csv(dataset_path, header=header)  # this line for task 1
        return csv_data


if __name__ == "__main__":
    SpeedAna()
