#!/usr/bin/env python

#print "importing io, data from Python package skimage"
from skimage import io, data
#print "importing io, data from Python package skimage"

import sys
import rospy
from robot_nav_plan.srv import *
from nav_msgs.msg import OccupancyGrid
import numpy as np

def show_spacewoman():
	#print "show image of astronaut."
	img=data.astronaut()
	io.imshow(img)
	io.show()



def costmap_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('costmap_cell_service')
    try:
        print("costmap_cell_service")
        ask_to_publich_modifier = rospy.ServiceProxy('costmap_cell_service', CostMapMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value, resp1.height, resp1.width
    except rospy.ServiceException:
        print("Service call failed: %s")


if __name__ == '__main__':

	token = 3
	seq = 2
	modifier_request_value = 3
	(costmap_data, height, width) = costmap_client(token, seq, modifier_request_value)
# print "data %s" %str(costmap_data)


