#!/usr/bin/env python

import sys
import rospy
import time
import math
import os
import copy
import csv
import pandas as pd

#   from xcs_api import XCS  # import xcs agent API
#   from visualization import Visualization  # import Visualization to show results
import random
import csv
#   import pandas as pd
#   import numpy as np

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64, Int64

from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist

from sensor_msgs.msg import LaserScan

from nav_msgs.msg import Odometry

from robot_nav_plan.srv import *


from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io, feature
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped

import subprocess

from xcs_api import XCS  # import xcs agent API

print("rospy path:", rospy.__file__)
#print("skimage feature path:", feature.__file__)



if __name__ == "__main__":
    """
    input the index for action: 
    0: do_freeze_process,
    1: do_pathPlanning_execution,
    2: do_reflex_execution,
    3: do_go_ahead_anyway_process,
    4: do_spin_right_process,
    5: do_spin_left_process,
    6: do_random_action_process,
    """
    rospy.init_node("acmca_l5_action_modifier_input", anonymous=False)
    action_pattern_pub = rospy.Publisher('/p3dx/ACMCA/action_modifier/action', Int64, queue_size=5) #in pioneer_take_action
    reflex_action_pub = rospy.Publisher('/p3dx/ACMCA/reflex/action', Int64, queue_size=5) #reflex action


    print("acmca_l5_action_modifier_input init. action_pattern list:")
    print(" 0: do_freeze_process,\n 1: do_pathPlanning_process,\n 2: do_reflex_execution,\n 3: do_go_ahead_anyway_process,\n 4: do_spin_right_process,\n 5: do_spin_left_process,\n 6: do_random_action_process,")
    while not rospy.is_shutdown():
        """
        try:
            reflex_action = int(input("entry reflex_action:"))
            print("set reflex_action = ", str(reflex_action))
        except ValueError:
            print("entry error")
        reflex_action_pub.publish(reflex_action)

        """

        try:
            action_pattern = int(input("entry action_pattern:"))
            print("set action_pattern = ", str(action_pattern))
        except ValueError:
            print("entry error")

        "##################"
        "take effect"
        "##################"
        action_pattern_pub.publish(action_pattern)





