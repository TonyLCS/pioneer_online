#!/usr/bin/env python

"""
===============
Rain simulation
===============

Simulates rain drops on a surface by animating the scale and opacity
of 50 scatter points.

Author: Nicolas P. Rougier
"""

import sys
import rospy
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation


from geometry_msgs.msg import Twist
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry


from robot_nav_plan.srv import *

from geometry_msgs.msg import Twist
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray

global bumper_fire_value
global speed_odom_value
global which_process


def update(frame_number):
    global bumper_fire_value
    global speed_odom_value
    global which_process

    # Get an index which we can use to re-spawn the oldest raindrop.
    current_index = frame_number % n_drops

    # Make all colors more transparent as time progresses.
    rain_drops['color'][:, 3] -= 1.0/len(rain_drops)
    rain_drops['color'][:, 3] = np.clip(rain_drops['color'][:, 3], 0, 1)

    # Make all circles bigger.
    rain_drops['size'] += rain_drops['growth']

    # Pick a new position for oldest rain drop, resetting its size,
    # color and growth factor.
    rain_drops['position'][current_index] = (bumper_fire_value, speed_odom_value )
    #rain_drops['position'][current_index] = (np.clip(bumper_fire_value, 0, 4000), np.clip(speed_odom_value, 0, 4000) )
    #rain_drops['position'][current_index] = np.random.uniform(0, 1, 2)
    rain_drops['size'][current_index] = 10
    if 0.5 < which_process < 1.5:
        rain_drops['color'][current_index] = (229/255, 134/255, 18/255, 1)  # orange, for reflex process
    elif 1.5 < which_process < 2.5:
        rain_drops['color'][current_index] = (25/255, 14/255, 214/255, 1)  # blue, for move base process
    elif -0.5 < which_process < 0.5:
        rain_drops['color'][current_index] = (25/255, 14/255, 214/255, 1)  # blue, for move base process
    elif 99.5 < which_process < 100.5:
        rain_drops['color'][current_index] = (43/255, 155/255, 18/255, 1)  # green, stop
    else:
        rain_drops['color'][current_index] = (229/255, 204/255, 18/255, 1)  # yellow, default

    rain_drops['growth'][current_index] = 15 #np.random.uniform(50, 200)

    # Update the scatter collection, with the new colors, sizes and positions.
    scat.set_edgecolors(rain_drops['color'])
    scat.set_sizes(rain_drops['size'])
    scat.set_offsets(rain_drops['position'])

def callback_bumper_fire_value(msg):
    global bumper_fire_value
    bumper_fire_value = msg.data
    #print("bumper_fire_value: ", bumper_fire_value)


def callback_speed_odom_value(msg):
    global speed_odom_value
    speed_odom_value = msg.data
    #print("speed_odom_value: ", speed_odom_value)

def callback_which_process(msg):
    global which_process
    which_process = msg.data



if __name__ == "__main__":
    global bumper_fire_value
    global speed_odom_value
    global which_process
    which_process = 0

    "##############################"
    "initiate ros interactions"
    "##############################"
    rospy.init_node("core_affect_space_visualization", anonymous=False)
    rospy.Subscriber('/p3dx/bumper_fire_value', Float64, callback_bumper_fire_value)
    rospy.Subscriber('/p3dx/speed_odom_value', Float64, callback_speed_odom_value)
    rospy.Subscriber('/p3dx/which_process', Float64, callback_which_process)

    # Fixing random state for reproducibility
    np.random.seed(19680801)


    # Create new Figure and an Axes which fills it.
    fig = plt.figure(figsize=(4, 4))
    ax = fig.add_axes([0, 0, 1, 1], frameon=False)
    ax.set_xlim(-1100, 1100), ax.set_xticks([])
    ax.set_ylim(-1, 11), ax.set_yticks([])

    # Create rain data
    n_drops = 10
    rain_drops = np.zeros(n_drops, dtype=[('position', float, 2),
                                          ('size',     float, 1),
                                          ('growth',   float, 1),
                                          ('color',    float, 4)])

    # Initialize the raindrops in random positions and with
    # random growth rates.
    rain_drops['position'] = np.random.uniform(0, 1, (n_drops, 2))
    #rain_drops['growth'] = np.random.uniform(50, 200, n_drops)
    rain_drops['growth'] = np.zeros(n_drops)

    # Construct the scatter which we will update during animation
    # as the raindrops develop.
    scat = ax.scatter(rain_drops['position'][:, 0], rain_drops['position'][:, 1],
                      s=rain_drops['size'], lw=0.5, edgecolors=rain_drops['color'],
                      facecolors='none')

    # Construct the animation, using the update function as the animation director.
    animation = FuncAnimation(fig, update, interval=100)  # 1 interval = 0.001 s
    plt.show()
