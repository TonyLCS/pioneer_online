#!/usr/bin/env python

import sys
import rospy


import random
import csv
import pandas as pd
import numpy as np
from robot_nav_plan.srv import *

def get_a_row_from_dataset():
    path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/1_PAOC_C.csv"
    data_record = pd.read_csv(path_dataset)
    #print("data_record total line:", data_record.shape[0])

    "toselect random one from data_record.shape[0] "
    row_index = np.random.randint(data_record.shape[0])

    print("data_record a row: NO.",  row_index)
    #print("perception :", data_record.loc[row_index])
    return data_record.loc[row_index]


def PAOC_online_reading_record_service_callback(req):
    print("pring req:", req.header_token_seq)
    #print "end of one PAOC process"
    a_perception = get_a_row_from_dataset()
    x = a_perception.loc["location_X"]
    y = a_perception.loc["location_Y"]
    ir = a_perception.loc["radius_inflation"]
    groundTrue = a_perception.loc["class"]
    print("perception (PAOCPerceptionSRVResponse) :", x,y,ir,groundTrue)
    return PAOCPerceptionSRVResponse(x,y,ir,groundTrue)  # response the request


if __name__ == "__main__":

    rospy.init_node('PAOC_online_reading_record_service')
    #print "PAOC_online_reading_record_service ready for client."
    print("PAOC_online_reading_record_service ready~")
    rospy.Service('PAOC_online_reading_record_service', PAOCPerceptionSRV, PAOC_online_reading_record_service_callback)
    #rospy.Service('PAOC_service', PAOCMSG, PAOC_online_reading_record_service_callback)

    rospy.spin()




