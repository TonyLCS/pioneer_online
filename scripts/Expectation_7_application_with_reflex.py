#!/usr/bin/env python

import sys
import rospy
import time
import math
import os
import copy
import csv
import pandas as pd

#   from xcs_api import XCS  # import xcs agent API
#   from visualization import Visualization  # import Visualization to show results
import random
import csv
#   import pandas as pd
#   import numpy as np

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64

from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist

from sensor_msgs.msg import LaserScan

from nav_msgs.msg import Odometry

from robot_nav_plan.srv import *


from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io, feature
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped

import subprocess

from xcs_api import XCS  # import xcs agent API

print("rospy path:", rospy.__file__)
print("skimage feature path:", feature.__file__)

"XCS AGENT"
global xcs_model

"#### costmap figures##########"

global slam_current_position
global get_current_slam_current_position

"#### NO.4 layer ##########"
global flag_to_active_path_planning_cmd

"#### NO.5 layer ##########"
global pub_cmd

"############################################################################"
"########################### NO.0 Dynamic layer : Path available  ###########"
"############################################################################"
def check_if_path_avaiable():
    print("0. costmap_NN_prediction client ask for prediction")
    seq =1
    #prediction = costmap_nn_reinforcerNode_client(seq)
    prediction = lidar_nn_reinforcerNode_client(seq)

    print("costmap_NN_prediction client receive prediction:", prediction)
    if prediction[1] >= 0.6:
        "60% certainty that there is no obstacle in the front"
        print("go~ go~ go~")
        return True
    else:
        return False


"############################################################################"
"########################### NO.1 layer : PAOC    ##########################"
"############################################################################"

def check_targeted_position_avaiable(targeted_position):

    "#############################"
    "1. setup modifier dynamically "
    "#############################"
    print("1. PubModClient active to call PubModService")
    token, seq = 1, 1
    modifier_request_value = 0.1
    modifier_response_value = publich_modifier_client(token, seq, modifier_request_value)
    print("1.set modifier dynamically : / mod-req %s/ mod-rsp %s",token, seq, str(modifier_request_value), str(modifier_response_value))

    "#############################"
    "2. publish targeted position "
    "#############################"
    print("2. PubModClient active to call targeted_position_service")
    token = 2
    current_position = targeted_position_client(token, seq, targeted_position)
    print("2. target_position ( x = %s, y = %s)" % (
        str(targeted_position.pose.position.x), str(targeted_position.pose.position.y)))
    print("2. current_position( x = %s, y = %s)" % (
        str(current_position.pose.position.x), str(current_position.pose.position.y)))


    "#############################"
    "3. publish path length "
    "#############################"
    print("3. PubModClient active to call planner_path_length_service")
    token = 3
    # modifier_request_value = 1000
    length_return = planner_path_length_client(token, seq, modifier_request_value)
    print("3. token %s/ seq %s/ the length of the path %s" % (token, seq, str(length_return)))
    print("#########################check_targeted_position_avaiable BY PAOC ##################################################")

    print("path length: %s" % str(length_return))
    print("path length: %s" % str(length_return))
    print("path length: %s" % str(length_return))
    print("path length: %s" % str(length_return))

    if 5 <= length_return <= 120:
        return True
    else:
        return False


def combine_position(x, y, angle):
    a_targeted_postition = PoseStamped()
    a_targeted_postition.header.frame_id = 'map'
    a_targeted_postition.pose.position.x = x
    a_targeted_postition.pose.position.y = y
    a_targeted_postition.pose.position.z = 0

    #print "angle -deg = %s"%angle
    angle = radians(angle)
    #print "angle -rad = %s"%angle
    (a_targeted_postition.pose.orientation.x, a_targeted_postition.pose.orientation.y,
     a_targeted_postition.pose.orientation.z, a_targeted_postition.pose.orientation.w) = rotate(0, angle, 0)
    #print "a_targeted_postition.pose.orientation.z= %s, a_targeted_postition.pose.orientation.w= %s"%(a_targeted_postition.pose.orientation.z, a_targeted_postition.pose.orientation.w)
    return a_targeted_postition

def rotate(heading, attitude, bank):
    # Assuming the angles are in radians.

    c1 = cos(heading / 2)
    s1 = sin(heading / 2)
    c2 = cos(attitude / 2)
    s2 = sin(attitude / 2)
    c3 = cos(bank / 2)
    s3 = sin(bank / 2)
    c1c2 = c1 * c2
    s1s2 = s1 * s2

    w = c1c2 * c3 - s1s2 * s3
    x = c1c2 * s3 + s1s2 * c3
    y = s1 * c2 * c3 + c1 * s2 * s3
    z = c1 * s2 * c3 - s1 * c2 * s3
    return x, y, z, w


"#################################################"
"###########   XCS agents     ########"
"#################################################"

def XCS_agent_initiate(task_path=None, classifiers_population=None):
    # Initialize game replay object
    xcs_model = XCS()
    xcs_model.set_parameters(task_path)
    xcs_model.XCS_initiate_fiters_API()
    # xcs_model.XCS_initiate_test_fiters_API()
    # xcs_model.XCS_initiate_mcts_fiters_API()

    xcs_model.set_learning_mode()

    if classifiers_population is not None:
        xcs_model.XCS_loading_classifiers_population_from_csv(classifiers_population)  # loading classifiers [P]

    return xcs_model

"#################################################"
"###########   NN Prediction client       ########"
"#################################################"

def costmap_nn_reinforcerNode_client(seq):
    rospy.wait_for_service('costmap_NN_prediction_service')
    try:
        getCostmapNNPrediction = rospy.ServiceProxy('costmap_NN_prediction_service', CostMapNNMSG)
        resp1 = getCostmapNNPrediction(seq)
        return resp1.prediction
    except rospy.ServiceException:
        print("Service call failed")


def lidar_nn_reinforcerNode_client(seq):
    rospy.wait_for_service('LIDAR_NN_prediction_service')
    try:
        getCostmapNNPrediction = rospy.ServiceProxy('LIDAR_NN_prediction_service', CostMapNNMSG)
        resp1 = getCostmapNNPrediction(seq)
        return resp1.prediction
    except rospy.ServiceException:
        print("Service call failed")


def callback_slammed_position(msg):
    global slam_current_position
    global get_current_slam_current_position

    slam_current_position = msg
    get_current_slam_current_position = True
    pass


def callback_bumper(msg):
    global bumper_data  # store first time bumper data
    global current_bumper_data
    global first_collision_flag
    global check_bumper_time_integral_flag
    global has_collsion_this_period

    global bumper_data_stock
    global bumper_fire_maximum
    global bumper_fire_value
    global pub_bumper_fire_value

    "store bumper data when it is the first time"
    "when the bumper data has been process by do_xcs_action_process, the first_collision_flag will be reset."
    # print("bumper data", msg.data, ",  first_collision_flag", first_collision_flag)
    has_collsion_this_moment = False
    current_bumper_data = msg.data
    if not first_collision_flag:
        for a_bumper in msg.data:
            if a_bumper >= 0.5:
                has_collsion_this_moment = True

    if has_collsion_this_moment:
        bumper_data = msg.data  # store data
        first_collision_flag = True
        print("set first_collision_flag = True, first collision happen")

    "check if the bumper happen at this period"
    if check_bumper_time_integral_flag:
        for a_bumper in msg.data:
            if a_bumper >= 0.5:
                has_collsion_this_period = True

    "##################################"
    "TO publish bumper fire"
    "##################################"

    "#####################################"
    "set the bumper data stock by updating the current data and keep fresh data within 3 seconds time duration"
    "#####################################"
    current_time = rospy.get_rostime()  # math.floor(rospy.get_rostime().secs)
    is_collision = 0
    for a_bamper_data in msg.data:
        if a_bamper_data > 0.5:
            is_collision = 1
    to_append = pd.DataFrame([[current_time, is_collision]], columns=("time", "collision"))
    bumper_data_stock = bumper_data_stock.append(to_append)
    "keep the resent 3 seconds"
    bumper_data_stock = bumper_data_stock.loc[bumper_data_stock["time"] > (current_time - rospy.Duration(2.5))]
    # => print("size of bumper_data_stock", bumper_data_stock.shape[0])
    """
    "#####################################"
    "establish fire curve"
    "#####################################"

    bumper_fire_value = 0
    bumper_fire = bumper_data_stock.loc[bumper_data_stock["collision"] == 1].shape[0]
    # print("ros current_time: ", current_time, "bumper_fire", bumper_fire, "bumper_fire_maximum", bumper_fire_maximum)
    if bumper_fire >= bumper_fire_maximum:
        "ignite curve"
        bumper_fire_maximum = bumper_fire
        bumper_fire_value = math.pow(bumper_fire, 3)  # poly
        # bumper_fire_value = 1/(1 + math.exp(-1*(bumper_fire + 10))) - 1/(1 + math.exp(-10)) # sigmoid
    else:
        "decay curve"
        delta = bumper_fire_maximum - bumper_fire
        bumper_fire_value = math.pow(bumper_fire_maximum, 3) - math.pow(delta, 3)
        # bumper_fire_value= 1/(1 + math.exp(-1*(bumper_fire_maximum + 10))) - 1/(1 + math.exp(-1*(delta + 10))) # sigmoid

        if bumper_fire_maximum - delta <= 1:
            bumper_fire_maximum = 0
    #print("bumper_fire_value:", bumper_fire_value)
    # if bumper_fire_value < 0:
    #    bumper_fire_value = 0
    bumper_fire_value = np.clip(-0.25 * bumper_fire_value, -1000, 1)  # reschedule to -1000 and 0
    """

    "#############################"
    "normalise bumper value into 1000 or -1000 "
    "#############################"
    bumper_fire_value = 50  # 1000
    bumper_fire = bumper_data_stock.loc[bumper_data_stock["collision"] == 1].shape[0]
    if bumper_fire >= 2:
        bumper_fire_value = -1000

    pub_bumper_fire_value.publish(float(bumper_fire_value))


def callback_odom(msg):
    global speed_odom_value
    global pub_speed_odom_value

    x = msg.twist.twist.linear.x
    y = msg.twist.twist.linear.y
    w = msg.twist.twist.angular.z
    # speed_odom_value = round(445 * math.pow(x, 2) + 445 * math.pow(y, 2) + 34 * math.pow(w, 2), 1)  # reschedule to -1000 and 0
    speed_odom_value = round(445 * math.pow(x, 2) + 34 * math.pow(w, 2), 1)  # reschedule to -1000 and 0
    # speed_odom_value = round((math.sqrt(math.pow(x, 2) + math.pow(y, 2)) + 0.25 * math.pow(w, 2)) *50, 1)  # reschedule to -1000 and 0
    speed_odom_value = np.clip(speed_odom_value, 0,
                               8)  # speed_odom_value are 500 for process reflex, 800 and 1000 for move-base process
    speed_odom_value = math.ceil(speed_odom_value)  # floor bring speed value to 10 states...

    pub_speed_odom_value.publish(float(speed_odom_value))

def convert_map_data_3(data, height_ori, width_ori):
    # height_ori = 320
    # width_ori = 480
    mapdata = np.zeros([height_ori, width_ori])
    print ("convert into map data")
    for line_index in range(0, height_ori):
        width_index = line_index * width_ori
        mapdata[height_ori - line_index - 1] = data[width_index: (width_index + width_ori)]
    return mapdata


def add_scope_to_map(mapdata_ori, height_ori, width_ori, origin_x, origin_y, position_x, position_y, x_range, y_range):
    # print(mapdata)
    mapdata = copy.deepcopy(mapdata_ori)
    """
    mapdata[height_ori-228-1, 100] = 150 # this is original point of coordinary
    #mapdata[height_ori-228-1-10, 100+10] = 150 # (1,1)
    "draw scope"
    y_range = 20
    x_range = 10

    mapdata[(height_ori-228-1-y_range),(100-x_range):(100+x_range)] = 250
    mapdata[(height_ori-228-1+y_range),(100-x_range):(100+x_range+1)] = 250
    mapdata[(height_ori-228-1-y_range):(height_ori-228-1+y_range),(100-x_range)] = 250
    mapdata[(height_ori-228-1-y_range):(height_ori-228-1+y_range),(100+x_range)] = 250
    """
    mapdata[height_ori - origin_y - 1, origin_x] = 250  # this is original point of coordinary
    # mapdata[height_ori-228-1-10, 100+10] = 150 # (1,1)
    "draw scope"

    """
    mapdata[(height_ori-origin_y-1-y_range),(origin_x-x_range):(origin_x+x_range)] = 250
    mapdata[(height_ori-origin_y-1+y_range),(origin_x-x_range):(origin_x+x_range+1)] = 250
    mapdata[(height_ori-origin_y-1-y_range):(height_ori-origin_y-1+y_range),(origin_x-x_range)] = 250
    mapdata[(height_ori-origin_y-1-y_range):(height_ori-origin_y-1+y_range),(origin_x+x_range)] = 250

    """

    linewidth = 3
    mapdata[int(height_ori - position_y - 1 - y_range):int(height_ori - position_y - 1 + y_range),
    int(position_x - x_range - linewidth):int(position_x - x_range + linewidth)] = 250
    mapdata[int(height_ori - position_y - 1 - y_range):int(height_ori - position_y - 1 + y_range),
    int(position_x + x_range - linewidth):int(position_x + x_range + linewidth)] = 250
    mapdata[
    int(height_ori - position_y - 1 - y_range - linewidth):int(height_ori - position_y - 1 - y_range + linewidth),
    int(position_x - x_range):int(position_x + x_range)] = 250
    mapdata[
    int(height_ori - position_y - 1 + y_range - linewidth):int(height_ori - position_y - 1 + y_range + linewidth),
    int(position_x - x_range):int(position_x + x_range)] = 250

    return mapdata
    # io.imshow(mapdata)
    # io.show()


def slice_map(mapdata, height_ori, position_x, position_y, x_range, y_range):
    submap = mapdata[int(height_ori - position_y - 1 - y_range):int(height_ori - position_y - 1 + y_range),
             int(position_x - x_range):int(position_x + x_range)]
    return submap


def costmap_client_3(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('costmap_cell_service')
    try:
        print("costmap_cell_service")
        ask_to_publich_modifier = rospy.ServiceProxy('costmap_cell_service', CostMapMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value, resp1.height, resp1.width, resp1.origin_x, resp1.origin_y
    except rospy.ServiceException:
        print("Service call failed: %s")


"#################################################"
"########### dynamic modifier client #############"
"#################################################"

def publich_modifier_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('publish_modifier')
    try:
        ask_to_publich_modifier = rospy.ServiceProxy('publish_modifier', PublishModifierMSG)
        resp_mod = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp_mod.modifier_return_value
    except rospy.ServiceException:
        print("Service call failed")


"#################################################"
"########### targeted_position client ############"
"#################################################"

def targeted_position_client(header_token_seq, seq, targeted_position):
    "publich targeted_position and return current_position"
    rospy.wait_for_service('targeted_position_service')
    try:
        print("client call targeted_position_service")
        request_the_server = rospy.ServiceProxy('targeted_position_service', TargetedPositionMSG)
        resp_position = request_the_server(header_token_seq, seq, targeted_position)  # calls the server with a request
        return resp_position.current_position   # get current_slam position a response from the server
    except rospy.ServiceException:
        print("Service call failed: %s")




"#################################################"
"########### path length from planner client #####"
"#################################################"

def planner_path_length_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('planner_path_length_service')
    try:
        print("client call planner_path_length_service")
        ask_to_publich_modifier = rospy.ServiceProxy('planner_path_length_service', PublishModifierMSG)
        resp_length = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp_length.modifier_return_value
    except rospy.ServiceException:
        print ("Service call failed")



"############################################################################"
"########################### NO.4 layer: to active two process  #############"
"############################################################################"


def do_nothing_process():
    print("#################################################")
    print("do_nothing_process")
    pass


def do_go_process():
    print("#################################################")
    print(" do_go_process \n") #    print("do_go_process")
    pioneer_take_action(111)


def do_freeze_process():
    print("#################################################")
    print("do_freeze_process")
    pioneer_take_action(100)
    pass

def do_go_ahead_anyway_process():
    print("#################################################")
    print("do_go_ahead_anyway_process for 5 seconds")
    pioneer_take_action(555)
    rospy.sleep(3.3)
    print("do_freeze_process")
    pioneer_take_action(100)
    pass

def do_random_action_process():
    #print("do_random_action_process")

    "############################################"
    "pioneer take random action"
    "############################################"
    #print("not collisions")
    random_action = np.random.randint(0, 100)
    if random_action < 30:
        print("#################################################")
        print(" do_random_action_process, randomly take action 1\n")
        pioneer_take_action(7)  # go ahead for 10 seconds
        #rospy.sleep(1)
    if 30 <= random_action < 60:
        print("#################################################")
        print(" do_random_action_process randomly take action 4\n")
        pioneer_take_action(10)  # go back for 10 seconds
        #rospy.sleep(1)
    if 60 <= random_action < 70:
        print("#################################################")
        print(" do_random_action_process randomly take action 2\n")
        pioneer_take_action(8)
        #rospy.sleep(1)
    if 70 <= random_action < 80:
        print("#################################################")
        print(" do_random_action_process, randomly take action 3\n")
        pioneer_take_action(9)
        #rospy.sleep(1)
    if 80 <= random_action < 90:
        print("#################################################")
        print(" do_random_action_process, randomly take action 5\n")
        pioneer_take_action(11)
        #rospy.sleep(1)
    if 90 <= random_action <= 100:
        print("#################################################")
        print(" do_random_action_process, randomly take action 6\n")
        pioneer_take_action(12)
        #rospy.sleep(1)

    rospy.sleep(3)
    print("end of random pioneer moving action: do_freeze_process")
    pioneer_take_action(100)

def do_xcs_action_process():
    global xcs_action_process_flag
    global bumper_data
    global first_collision_flag

    print("do_xcs_action_process")

    "set flag to lock the process"
    xcs_action_process_flag = True

    "get xcs action effect"
    # goto_xcs_iterations()

    "reset flags"
    first_collision_flag = False
    xcs_action_process_flag = False


process = {
    0: do_nothing_process,
    1: do_go_process,
    2: do_freeze_process,
    3: do_go_ahead_anyway_process,
    4: do_random_action_process,
    5: do_xcs_action_process,


}

"############################################################################"
"########################### NO.5 layer: Pioneer cmd output    ##############"
"############################################################################"

"#################################################"
"########### publish Pioneer targeted_position ###"
"#################################################"

def moving_to_targeted_position_client(token, seq, targeted_position):
    rospy.wait_for_service('multiple_cmdVel_service')
    try:
        print("     client call multiple_cmdVel_service")
        request_the_server = rospy.ServiceProxy('multiple_cmdVel_service', TargetedPositionMSG)
        resp1 = request_the_server(token, seq, targeted_position)  # calls the server with a request
        print("     Service success response.")
        return resp1.current_position   # get a response from the server
    except rospy.ServiceException:
        print("     Service call failed")

"#################################################"
"########### Pioneer action ######################"
"#################################################"


def pioneer_take_action(pioneer_take_action):
    global flag_to_active_path_planning_cmd

    " publich 6 actions"
    pub = rospy.Publisher('/p3dx/cmd_vel', Twist, queue_size=5)
    # test=> pioneer_take_action = np.random.randint(7, 13)
    cmd = set_vel(0, 0)  # default

    "for reflex process"
    if pioneer_take_action == 1:
        cmd = set_vel(0.1, 0)
    if pioneer_take_action == 2:
        cmd = set_vel(0.05, -0.32) # right
    if pioneer_take_action == 3:
        cmd = set_vel(0.05, 0.32) # left

    if pioneer_take_action == 4:
        cmd = set_vel(-0.1, 0)
    if pioneer_take_action == 5:
        cmd = set_vel(-0.05, 0.32)
    if pioneer_take_action == 6:
        cmd = set_vel(-0.05, -0.32)

    "for random process"
    if pioneer_take_action == 7:
        cmd = set_vel(0.15, 0)
    if pioneer_take_action == 8:
        cmd = set_vel(0.1, -0.32) # right
    if pioneer_take_action == 9:
        cmd = set_vel(0.1, 0.32) # left

    if pioneer_take_action == 10:
        cmd = set_vel(-0.15, 0)
    if pioneer_take_action == 11:
        cmd = set_vel(-0.1, 0.32)
    if pioneer_take_action == 12:
        cmd = set_vel(-0.1, -0.32)


    "stop"
    if pioneer_take_action == 100:
        cmd = set_vel(0, 0)
        print("pioneer set cmd = set_vel(0, 0)")

    "go ahead anyway"
    if pioneer_take_action == 555:
        cmd = set_vel(0.5, 0)

    "pathplanning"
    if pioneer_take_action == 111:
        print("will be take effect in republish cmd in callback function through flag_to_active_path_planning_cmd")
        flag_to_active_path_planning_cmd = True
        return
    else:
        flag_to_active_path_planning_cmd = False

    "##################"
    "take effect"
    "##################"
    pub.publish(cmd)
    rate = rospy.Rate(15)
    pub_iter = 0
    while pub_iter < 3:

        print(" publiching pioneer_take_action:", pioneer_take_action)
        pub.publish(cmd)
        rate.sleep()
        pub_iter += 1

    "###############################"
    "reset first_collision_flag"
    "###############################"
    #first_collision_flag = False
    #rospy.sleep(0.2)

    "##################"
    "stop"
    "##################"
    #cmd = set_vel(0, 0)
    #pub.publish(cmd)


def set_vel(vel_x, vel_z):
    cmd = Twist()
    cmd.linear.x = vel_x
    cmd.linear.y = 0
    cmd.linear.z = 0
    cmd.angular.x = 0
    cmd.angular.y = 0
    cmd.angular.z = vel_z
    return cmd


"#################################################"
"###########    modifiers   ######################"
"#################################################"

def callback_pathPlanning(msg):
    "Path planning cmd output, modifier"
    global flag_to_active_path_planning_cmd
    global pub_cmd
    #print("subscribe path planner cmd:", msg)
    if flag_to_active_path_planning_cmd:
        pub_cmd.publish(msg)


def get_costmap(save_dir, fig_index):
    global get_current_slam_current_position
    global slam_current_position

    token = 3
    seq = 2
    modifier_request_value = 3
    get_current_slam_current_position = False
    rate = rospy.Rate(1)
    while not get_current_slam_current_position:
        rate.sleep()

    "###############################"
    "####### get Costmap   #########"
    "###############################"

    (costmap_data, height, width, origin_x, origin_y) = costmap_client_3(token, seq, modifier_request_value)
    # print "data %s" %str(costmap_data)
    print("get costmap with height %s/ width %s/ " % (str(height), str(width)))

    resolution = 10
    original_x = int(-1 * resolution * origin_x)
    original_y = int(-1 * resolution * origin_y)
    world_x = int(slam_current_position.pose.position.x * resolution)
    world_y = int(slam_current_position.pose.position.y * resolution)
    print("current position : x=", slam_current_position.pose.position.x, "; y=", slam_current_position.pose.position.y)

    x_matrix = original_x + world_x
    y_matrix = original_y + world_y

    # convert_map_data(costmap_data)
    mapdata_converted = convert_map_data_3(costmap_data, height, width)
    x_range = 30
    y_range = 30
    mapdata = add_scope_to_map(mapdata_converted, height, width, int(-10 * origin_x), int(-10 * origin_y), x_matrix,
                               y_matrix, x_range, y_range)
    submapdata = slice_map(mapdata_converted, height, x_matrix, y_matrix, x_range, y_range)

    print("plt plot")
    # plt.imshow(mapdata)
    # plt.draw()
    # plt.pause(1)

    fig = plt.figure(figsize=(3.4, 1.7))
    ax1 = fig.add_subplot(1, 2, 1)
    ax2 = fig.add_subplot(1, 2, 2)
    ax1.imshow(mapdata)
    ax2.imshow(submapdata)
    plt.draw()

    fig_name = str(fig_index)
    # Save model and weights
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)

    png_complete_name = os.path.join(save_dir, "full" + str(fig_index) + ".png")
    fig.savefig(png_complete_name)

    extent = ax2.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    png_part_name = os.path.join(save_dir, str(fig_index) + ".png")
    fig.savefig(png_part_name, bbox_inches=extent)

    plt.pause(2)

    # plt.show()
    plt.close()
    #fig_index = fig_index + 1
    #return fig_index

def manual_input():
    global do_manual_input
    try:
        position_input_x = float(input("entry position.x (metre):"))
        print("entry targeted position.x = ", position_input_x)
    except ValueError:
        print("entry error")
    try:
        position_input_y = float(input("entry position.y (metre):"))
        print("entry targeted position.y = ", position_input_y)
    except ValueError:
        print("entry error")
    try:
        position_input_ang = float(input("entry position.angular (degree):"))
        print("entry targeted position.angular = ", position_input_ang, "degree")
    except ValueError:
        print("entry error")
    """
    try:
        last_manual = float(input("do manual :True = 1 False =0::"))
        if last_manual == 0:
            do_manual_input = False
        print("do_manual_input = ", do_manual_input)
    except ValueError:
        print("entry error")
    """
    return position_input_x, position_input_y, position_input_ang


def predict_time_consumption():
    seq =1
    print("expectation_nn_prediction client ask for prediction, seq:", seq)
    prediction = costmap_nn_reinforcerNode_client(seq)
    print("expectation_nn_prediction client receive prediction:", prediction)
    #print(max(prediction))
    print(" predicted label: ", prediction.index(max(prediction)), " with probability :", max(prediction))

    which_label = prediction.index(max(prediction))

    save_dir = os.path.join(os.getcwd(), 'src/robot_nav_plan/result/checkPointExpectations')
    labelsFile = os.path.join(save_dir, "localMapLabel.csv")

    "read PAOC dataSet from csv"
    labels_csv = pd.read_csv(labelsFile, header=0)
    labels_csv_2 = labels_csv[labels_csv['checkpoint'] == which_label]
    #print(labels_csv_2)
    last_row = labels_csv_2.iloc[-1]
    expectation_last = last_row.loc["expectation"]
    # print("last_row", last_row)
    print("expactation:", expectation_last)
    message = "expactation:" + str(round(expectation_last, 2)) +"; predicted label: "+ str(prediction.index(max(prediction))) + " with probability :"+str(max(prediction))
    sendmessage(message)
    return expectation_last


def costmap_nn_reinforcerNode_client(seq):
    rospy.wait_for_service('expectation_NN_prediction_service')
    try:
        print("try to call service expectation_NN_prediction_service with seq = ", seq)
        getCostmapNNPrediction = rospy.ServiceProxy('expectation_NN_prediction_service', CostMapNNMSG)
        resp1 = getCostmapNNPrediction(seq)
        return resp1.prediction
    except rospy.ServiceException:
        print("Service call failed")

def sendmessage(message):
    subprocess.Popen(['notify-send', message])
    return

def get_check_point_position(do_manual_input, check_point, goals_read):

    if do_manual_input:
        position_input_x, position_input_y, position_input_ang = manual_input()
    else:
        goals_read_row = goals_read.loc[check_point]

        position_input_x = goals_read_row.loc["x"]
        position_input_y = goals_read_row.loc["y"]
        position_input_ang = goals_read_row.loc["angle"]

        print("reading goals from goalsFileName => targeted position[", check_point, "] : ", position_input_x,
              position_input_y, position_input_ang)

    return position_input_x, position_input_y, position_input_ang

if __name__ == "__main__":

    global xcs_model


    "### NO.1 layer##"
    global speed_odom_value
    global pub_speed_odom_value

    "#### costmap figure##########"
    global get_current_slam_current_position
    global slam_current_position

    "#### NO.4 layer ##########"
    global flag_to_active_path_planning_cmd
    "#### NO.5 layer ##########"
    global pub_cmd
    #global save_dir

    save_dir = os.path.join(os.getcwd(), 'src/robot_nav_plan/result/checkPointExpectations')
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)



    "#############################"
    "initiate xcs agent"
    "#############################"
    classifiers_population = "lastest"
    xcs_model = XCS_agent_initiate("/Results/doThisTask.csv", "/reflex_hire_l_OnlyBumper/lastest")
    goto_online_dataset = True # this parameter is not useless in this case. it is overrided by learn_from_real_time_affect.
    learn_from_real_time_affect = True # learning form real time affect, instead of from dataset.

    "#############################"
    "initiate xcs hierarchy agent"
    "#############################"

    "##############################"
    "initiate ros interactions"
    "##############################"
    rospy.init_node("paoc_go", anonymous=False)

    "#### NO.1 layer ##########"
    "###### bumper #######"
    bumper_data = tuple()
    current_bumper_data = tuple()
    first_collision_flag = False # if true, goto process bumper data
    check_bumper_time_integral_flag = False
    has_collsion_this_period = False

    bumper_fire_value = 0
    bumper_data_stock = pd.DataFrame(columns=("time", "collision"))
    bumper_fire_maximum = 0

    rospy.Subscriber('/p3dx/bumper_state_date', Float64MultiArray, callback_bumper)
    pub_bumper_fire_value = rospy.Publisher('/p3dx/bumper_fire_value', Float64, queue_size=5)

    "###### speed #######"

    speed_odom_value = 0
    rospy.Subscriber('/p3dx/odom', Odometry, callback_odom)
    pub_speed_odom_value = rospy.Publisher('/p3dx/speed_odom_value', Float64, queue_size=5)

    pub_which_process = rospy.Publisher('/p3dx/which_process', Float64, queue_size=5)
    core_affect_perception = tuple()

    "#### NO.4 layer ##########"
    flag_to_active_path_planning_cmd = False

    "#### NO.5 layer ##########"
    pub_cmd = rospy.Publisher('/p3dx/cmd_vel', Twist, queue_size=1) # pioneer_take_action
    rospy.Subscriber('/p3dx/Multiplexer_PathPlanner/cmd_vel', Twist, callback_pathPlanning)

    "#### costmap figure##########"
    get_current_slam_current_position = False
    sub = rospy.Subscriber('/p3dx/pose_listener/slammed_pose', PoseStamped, callback_slammed_position)
    rate = rospy.Rate(1)
    while not get_current_slam_current_position:
        rate.sleep()

    "#### csv headline writing #########"
    csvfilename = os.path.join(save_dir, "localMapLabel.csv")
    print("csvfilename:", csvfilename)
    #csvfilename = "./src/robot_nav_plan/result/radius_inflation_data_1.csv"
    fig_index = 0
    check_point = 0
    """
    fieldnames = ['id', 'checkpoint', 'timeConsumption', 'expectation']

    with open(csvfilename, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        #writer.writerow({'id': 100, 'checkpoint': 2, 'timeConsumption': 20.55})
        #writer.writerow({'id': 100, 'checkpoint': 2, 'timeConsumption': 20.55})
    """

    goalsFileName = os.path.join(save_dir, "goals.csv")
    print("reading goals from goalsFileName:", goalsFileName)
    goals_read = pd.read_csv(goalsFileName)
    print("goalsFile shape:", goals_read.shape)



    print("paoc go prepare~")

    "##############################"
    " goto iteration loop"
    "##############################"

    which_process = 0
    while not rospy.is_shutdown():

        "##########################################"
        " set up goal position in this trail"
        "##########################################"

        check_point = fig_index % 6
        do_manual_input = False
        check_point = 0
        position_input_x, position_input_y, position_input_ang = get_check_point_position(do_manual_input, check_point, goals_read)

        "##########################################"
        "layer 0: dynamic environment recognition"
        " if no obstacle in front, process, otherwise, stop"
        "##########################################"
        wait_index = 100
        while wait_index < 100 :
            path_avaiable = check_if_path_avaiable()

            if path_avaiable:
                print("path_avaiable is ", path_avaiable)
                #which_process = 3
                "##########################################"
                " execute process and take effect"
                "##########################################"
                which_process = 3
                process.get(which_process, 0)()
                pub_which_process.publish(float(which_process))
                print("execute process 3, then sleeping 5 second...")
                rospy.sleep(5)
                wait_index = 1000
                continue
            else:
                "this disable PAOC PROCESS!!! The following code will not be executed"
                wait_index = wait_index + 1
                rospy.sleep(0.5)
                continue


        "##########################################"
        "layer 1: paoc"
        " input:targeted location(Plan), output: avaiable(Outcome)"
        "##########################################"
        targeted_position = combine_position(position_input_x, position_input_y, position_input_ang)

        targeted_position_available = check_targeted_position_avaiable(targeted_position)


        "##########################################"
        "core affect "
        "##########################################"




        "##########################################"
        "layer 5: set targeted_position for the navigation process"
        "##########################################"

        if targeted_position_available:
            "##########################################"
            "save costmap figure"
            "##########################################"
            #get_costmap(save_dir, fig_index)
            expectation_last = predict_time_consumption()


            "##########################################"
            " execute process and take effect"
            "##########################################"
            #which_process = 1  # navigation process
            print("\n which_process:", which_process)
            which_process = 1
            process.get(which_process, 0)() # which process =1
            pub_which_process.publish(float(which_process))

            time_start = rospy.get_time()
            print("=> start timing:")

            "##########################################"
            "go to the targeted position"
            "##########################################"

            publish_position = targeted_position
            print("aims to achieve position x=%s, y=%s ." % ( str(publish_position.pose.position.x), str(publish_position.pose.position.y)))
            token, seq = 5, 1
            current_position = moving_to_targeted_position_client(token, seq, publish_position)
            print("current position: ", current_position.pose.position.x, current_position.pose.position.y)
            which_process = 2
            process.get(which_process, 0)()            #do_freeze_process
            pub_which_process.publish(float(which_process))

            "###############################"
            "get perception form topics"
            "###############################"
            print(" learn from the interaction ")
            # rospy.Subscriber('/p3dx/bumper_state_date', Float64MultiArray, callback_bumper)
            # perception = get_perception_from_topic()

            "###############################"
            "go to a step iteration"
            "###############################"
            if not first_collision_flag:
                print("not collision,but apply reflex process...")
                action_xcs = xcs_model.XCS_get_prediction_API(current_bumper_data)
            else:
                action_xcs = xcs_model.XCS_get_prediction_API(bumper_data)

            print("##############reflex action#########################")
            print("==>> reflex action:", action_xcs)
            print("##############reflex action#########################")
            "###############################"
            "action take effect"
            "###############################"
            pioneer_take_action(action_xcs)
            rospy.sleep(0.5)
            check_bumper_time_integral_flag = True
            print("#####################################################")
            print(
                "sleep 1.5s to wait the effect of xcs actions, check if there is a bumping happen during this waiting period?")
            rospy.sleep(1.5)

            "###############################"
            "action take effect"
            "###############################"
            """ DEBUG OK
            pioneer_take_action(action_xcs)
            rospy.sleep(0.5)
            check_bumper_time_integral_flag = True
            print("#####################################################")
            print(
                "sleep 1.5s to wait the effect of xcs actions, check if there is a bumping happen during this waiting period?")
            rospy.sleep(1.5)
            print("##############reflex action#########################") 
            
            """

            "GO random"

            which_process = 4
            print("\n which_process:", which_process)
            process.get(which_process, 0)()
            pub_which_process.publish(float(which_process))

            "##########################################"
            "calculate time and save"
            "##########################################"
            duration = rospy.get_time() - time_start
            #print("=> start time:", seconds)
            print("=> take time: ", duration, " s")

            message = "expactation:" + str(round(expectation_last, 2)) + "; real time consumption: " + str(round(duration, 2)) \
                      + " with difference :" + str(round(expectation_last-duration, 2))
            sendmessage(message)
            print("message:", message)
            "##########################################"
            "writing csv save"
            "##########################################"
            #a_row = [fig_index, check_point, duration]
            "update expectation"
            """ 
            if fig_index< 6:
                expectation = duration
            else:
                exp_read = pd.read_csv(csvfilename)
                exp_row = exp_read.loc[exp_read.shape[0] - 6]
                print("read the checkpoint last time: ", exp_row)
                expectation_last = exp_row.loc['expectation']
                expectation = 0.8* expectation_last + 0.2 * duration
                print("update expectation from", expectation_last, " to ", expectation)
            #fig_index = fig_index + 1
          
            with open(csvfilename, 'a', newline='') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writerow({'id': fig_index, 'checkpoint': check_point, 'timeConsumption': duration, 'expectation': expectation})
            """

            fig_index = fig_index + 1
        else:
            "GO random"
            pass
            """DEBUG OK
            
            
            which_process = 4
            print("\n which_process:", which_process)
            process.get(which_process, 0)()
            pub_which_process.publish(float(which_process))
            """
            "###############################"
            "go to a step iteration"
            "###############################"
            if not first_collision_flag:
                print("not collision,but apply reflex process...")
                action_xcs = xcs_model.XCS_get_prediction_API(current_bumper_data)
            else:
                action_xcs = xcs_model.XCS_get_prediction_API(bumper_data)

            print("##############reflex action#########################")
            print("==>> reflex action:", action_xcs)
            print("##############reflex action#########################")
            "###############################"
            "action take effect"
            "###############################"
            pioneer_take_action(action_xcs)
            rospy.sleep(0.5)
            check_bumper_time_integral_flag = True
            print("#####################################################")
            print(
                "sleep 1.5s to wait the effect of xcs actions, check if there is a bumping happen during this waiting period?")
            rospy.sleep(1.5)


        "##########################################"
        "reward update"
        "##########################################"

        "##########################################"
        "save hierarchy classifier"
        "##########################################"

        print("main loop sleeping 5 second...")
        rospy.sleep(5)





