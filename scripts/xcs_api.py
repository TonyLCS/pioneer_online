from jpype import *
import os


class XCS():
    """
        this class servers as api. get date from python script, trains xcs rules, and return predict result to the script.
    """
    def __init__(self):
        """initial the class"""
        # establish JVM environment.
        if isJVMStarted():
            # main api class
            self.jdprint = java.lang.System.out.println
            self.jdprint("jvm environment initialed from .jar file. again!")

            self.JDClass = JClass("main.java.GridSim.GridOp.GridOp")
            self.gridOp = self.JDClass()
        else:
            startJVM(getDefaultJVMPath(), "-ea",
                     "-Djava.class.path=/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/XCS_online_26.jar") # for robot_nav_plan on hyperion
                     #"-Djava.class.path=/home/tony/PycharmProjects/maze/maze_deep_learning/Java_Grid.jar")# for maze project on hyperion
                     #"-Djava.class.path=C:/Users/zzm/Documents/software/GITLAB/Java_Grid/out/artifacts/Java_Grid_jar/Java_Grid.jar")# windows
            self.jdprint = java.lang.System.out.println
            self.jdprint("jvm environment initialed from .jar file.")

            # main api class
            self.JDClass = JClass("main.java.GridSim.GridOp.GridOp")

            self.gridOp = self.JDClass()

    def set_parameters(self, path="/Results/doThisTask.csv"):
        self.gridOp.set_DataSet_PARAMS(path)
        # self.gridOp.XCS_set_learning_mode_API()# set training mode

    def set_learning_mode(self):
        self.gridOp.XCS_set_learning_mode_API()  # set training mode

    def set_testing_mode(self):
        self.gridOp.XCS_set_testing_mode_API()  # set test mode

    def set_mcts_mode(self):
        self.gridOp.XCS_set_mcts_mode_API()  # set test mode

    def learning_iteration(self):
        self.gridOp.MultiStepLearning_Instance()

    def XCS_get_prediction_API(self, perception):
        return self.gridOp.XCS_get_prediction_API(perception)

    def XCS_get_prediction_mcts_API(self, perception):
        return self.gridOp.XCS_get_prediction_mcts_API(perception)

    def XCS_get_prediction_test_mode_API(self, perception):
        return self.gridOp.XCS_get_prediction_test_mode_API(perception)

    def XCS_get_actions_reward_hashmap(self, training_mode):
        "dictionary (x,y): (Reward_act1, Reward_act2, Reward_act3, Reward_act4)"
        return self.gridOp.XCS_get_actions_reward_hashmap(training_mode)

    def XCS_initiate_fiters_API(self):
        self.gridOp.XCS_initiate_fiters_API()

    def XCS_initiate_test_fiters_API(self):
        "only match filter and selection filter"
        self.gridOp.XCS_initiate_test_fiters_API()

    def XCS_initiate_mcts_fiters_API(self):
        "only match filter and selection filter"
        self.gridOp.XCS_initiate_mcts_fiters_API()

    def XCS_update_reward_API(self, reward, reward_method='even', dynamic_learning=False, learning_rate=0.2):
        "discount_big_start, discount_big_end, even"
        self.gridOp.XCS_update_reward_API(reward, reward_method, dynamic_learning, learning_rate)

    def XCS_update_mcts_reward_API(self, reward, reward_method='even', dynamic_learning=False, learning_rate=0.1):
        "discount_big_start, discount_big_end, even"
        self.gridOp.XCS_update_mcts_reward_API(reward, reward_method, dynamic_learning, learning_rate)

    def XCS_mcts_establish_MacthSet_ActionSet(self, current_state, actions):
        self.gridOp.XCS_mcts_establish_MacthSet_ActionSet(current_state, actions)

    def XCS_save_classifiers_API(self, iteration_index, path="./src/robot_nav_plan/result/"):
        self.gridOp.XCS_save_classifiers_API(iteration_index, path)

    def XCS_save_classifiers_manually_API(self, path="./src/robot_nav_plan/result/"):
        self.gridOp.XCS_save_classifiers_manually_API(path)

    def XCS_loading_classifiers_population_from_csv(self, file_name, path="./src/robot_nav_plan/result/"):
        "loading population from dir /Results/file_name.csv -> /src/robot_nav_plan/result/file.csv"
        self.gridOp.XCS_loading_classifiers_population_from_csv_API(file_name, path)

    def XCS_deletion_API(self, pop_size):
        self.gridOp.XCS_deletion_API(pop_size)

    def XCS_subsumption_API(self):
        self.gridOp.XCS_subsumption_API()

    def terminate(self):
        del self.JDClass
        del self.gridOp
        self.jdprint("terminate JVM.")
        # shutdownJVM()





