#!/usr/bin/env python

import sys
import rospy
import time
import math

from xcs_api import XCS  # import xcs agent API
from visualization import Visualization  # import Visualization to show results
import random
import csv
import pandas as pd
import numpy as np

from geometry_msgs.msg import Twist
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry


from robot_nav_plan.srv import *

from geometry_msgs.msg import Twist
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray

global bumper_data
global current_bumper_data

global iter_index
global xcs_model

global iter_index_hier
global xcs_model_hier

global first_collision_flag
global xcs_action_process_flag
global random_action_process_flag
global time_inhibition

global check_bumper_time_integral_flag
global has_collsion_this_period
global path_and_folder

global bumper_fire_value
global speed_odom_value
global core_affect_perception

global odom_speed
global pub_speed_odom_value
global speed_odom_stock

global project_name_for_director

global path_planning_cmd


def inhibition_timer():
    """stop vel published by pathPlanner, just do reflex"""
    global time_inhibition
    time_inhibition = rospy.Time.from_sec(time.time())


def XCS_agent_initiate(task_path=None, classifiers_population=None):

    # Initialize game replay object
    xcs_model = XCS()
    xcs_model.set_parameters(task_path)
    xcs_model.XCS_initiate_fiters_API()
    #xcs_model.XCS_initiate_test_fiters_API()
    #xcs_model.XCS_initiate_mcts_fiters_API()

    xcs_model.set_learning_mode()

    if classifiers_population is not None:
        xcs_model.XCS_loading_classifiers_population_from_csv(classifiers_population) # loading classifiers [P]

    return xcs_model


def XCS_agent_test_mode_initiate(classifiers_population="lastest"):

    # Initialize game replay object
    xcs_model = XCS()
    xcs_model.set_parameters()
    xcs_model.XCS_initiate_test_fiters_API()
    xcs_model.set_testing_mode()

    xcs_model.XCS_loading_classifiers_population_from_csv(classifiers_population)

    return xcs_model

def get_core_affect_space_perception():
    global bumper_fire_value
    global speed_odom_value
    global core_affect_perception
    print("\n")
    print("\n")
    print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
    print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
    print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
    #print("bumper_fire_value:", bumper_fire_value, " ", type(bumper_fire_value), " speed_odom_value: ", speed_odom_value, " ",type(speed_odom_value))
    core_affect_perception = (bumper_fire_value, speed_odom_value)
    print("         core affect perception :", core_affect_perception)

    """
    
    if math.isnan(bumper_fire_value) and math.isnan(speed_odom_value):
        print("bumper_fire_value:", bumper_fire_value, " ", type(bumper_fire_value), " speed_odom_value: ", speed_odom_value, " ", type(speed_odom_value))
        core_affect_perception[0] = bumper_fire_value
        core_affect_perception[1] = speed_odom_value
        print("         core affect perception :", core_affect_perception)
        return True
    else:
        print("         core affect perception not ready")
        return False
    """
    return True

def xcs_single_step_iteration(xcs_model, iter_index, goto_online_dataset, learn_from_real_time_affect=False):
    global bumper_data
    global first_collision_flag
    global current_bumper_data

    global check_bumper_time_integral_flag
    global has_collsion_this_period

    global xcs_model_hier


    if learn_from_real_time_affect:
        "learning and interacting with the environment in real time"

        if not current_bumper_data:
            return xcs_model

        if first_collision_flag and not bumper_data:
            return xcs_model

        if not first_collision_flag:
            print("not collision, the reflex process pass takes random actions")
            #pioneer_take_action(np.random.randint(1, 7))
            pioneer_take_action(100) #stop
            return xcs_model

        "###############################"
        "get perception form topics"
        "###############################"
        print(" learn from the interaction ")
        #rospy.Subscriber('/p3dx/bumper_state_date', Float64MultiArray, callback_bumper)
        #perception = get_perception_from_topic()

        "###############################"
        "go to a step iteration"
        "###############################"
        if not first_collision_flag:
            print("not collision,but apply reflex process...")
            action_xcs = xcs_model.XCS_get_prediction_API(current_bumper_data)
        else:
            action_xcs = xcs_model.XCS_get_prediction_API(bumper_data)

        print("##############reflex action#########################")
        print("reflex action:", action_xcs)
        print("##############reflex action#########################")


        "###############################"
        "action take effect"
        "###############################"
        pioneer_take_action(action_xcs)
        rospy.sleep(0.5)
        check_bumper_time_integral_flag = True
        print("#####################################################")
        print("sleep 1.5s to wait the effect of xcs actions, check if there is a bumping happen during this waiting period?")
        rospy.sleep(1.5)
        #print("now, has_collsion_this_period: ", has_collsion_this_period)
        "###############################"
        "get single step reward"
        "###############################"
        reward_episode = 1000
        if has_collsion_this_period:
            reward_episode = -1000  # has collision, reward = 0
            print("#####################----------------1000----------------#######################################")
            print("#####################----------------1000----------------#######################################")
            print("#####################----------------1000----------------#######################################")
            print("#####################----------------1000----------------#######################################")
            print("#####################----------------1000----------------#######################################")
            print("#####################----------------1000----------------#######################################")
        else:
            print("#####################++++++++++++++++1000++++++++++++++++#######################################")
            print("#####################++++++++++++++++1000++++++++++++++++#######################################")
            print("#####################++++++++++++++++1000++++++++++++++++#######################################")
            print("#####################++++++++++++++++1000++++++++++++++++#######################################")
            print("#####################++++++++++++++++1000++++++++++++++++#######################################")
            print("#####################++++++++++++++++1000++++++++++++++++#######################################")

        reward_method = "even"
        if reward_episode == 1000:
            xcs_model.XCS_update_reward_API(reward_episode, reward_method, True, 0.1)
            #xcs_model_hier.XCS_update_reward_API(reward_episode, reward_method, True, 0.2)
        else:
            xcs_model.XCS_update_reward_API(reward_episode, reward_method, True, 0.025)
            #xcs_model_hier.XCS_update_reward_API(reward_episode, reward_method, True, 0.05)


        "###############################"
        "reset global flags"
        "###############################"
        check_bumper_time_integral_flag = False
        has_collsion_this_period = False
    else:
        "learning form the dataset"

        "get data"
        if goto_online_dataset:
            "goto remote service"
            header_token_seq = 0  # 0:return random instance in dataset, 1: return index_NO.x in dataset"
            index = 0
            perception, groundTrue, dataset_length = get_perception_client(header_token_seq, index) # online learning
        else:
            "go to local file"
            #path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/1_PAOC_C.csv"
            perception, groundTrue = get_perception_localFile(offline_path_dataset) #offline learning

        "go to a step iteration"
        action_xcs = xcs_model.XCS_get_prediction_API(perception)
        print("print action_xcs:", action_xcs)

        "reward single step"
        if action_xcs == groundTrue:
            reward_episode = 1000
        else:
            reward_episode = 0

        reward_method = "even"
        xcs_model.XCS_update_reward_API(reward_episode, reward_method, True,
                                        0.2)  # discount_big_end, even. dynamic learning rate

    "save classifiers population"
    #path_and_folder = "./src/robot_nav_plan/result/reflex_temp/"
    xcs_model.XCS_save_classifiers_API(iter_index, path_and_folder)

    return xcs_model


def xcs_testing_iteration(classifiers_population):
    "initiate xcs agent"
    xcs_testing_model = XCS_agent_test_mode_initiate(classifiers_population)

    #path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/1_PAOC_C.csv"

    "read length of dataset"
    if goto_online_dataset:  # in xcs_testing_iteration
        "goto remote service"
        header_token_seq = 0  # 0:return random instance in dataset, 1: return index_NO.x in dataset"
        index = 0
        perception, groundTrue, dataset_length = get_perception_client(header_token_seq, index)  # online test
    else:
        dataset_length = get_length_of_dataset(offline_path_dataset)

    correct_count = 0
    wrong_count = 0
    wrong_instance_list = []
    correct_rate = 0
    "goto iteration loop"
    for instance_index in range(1, dataset_length): #!! start from row NO.1 , dataset_length contains csv header!, but range is [1, 901)
        "get data"
        # perception, groundTrue = get_perception_client() #online
        #perception, groundTrue = get_perception_localFile(offline_path_dataset, False, instance_index)  # offline

        #goto_online_dataset = False

        "get data"
        if goto_online_dataset:
            "goto remote service"
            header_token_seq = 1  # 0:return random instance in dataset, 1: return index_NO.x in dataset"
            index = instance_index
            perception, groundTrue, dataset_length = get_perception_client(header_token_seq, index)  # online test
        else:
            "go to local file"
            perception, groundTrue = get_perception_localFile(offline_path_dataset, False, instance_index)  # offline test

        "go to a step iteration"
        action_xcs = xcs_testing_model.XCS_get_prediction_API(perception)

        "check prediction"
        if groundTrue == float(action_xcs):
            correct_count = correct_count + 1
        else:
            wrong_count = wrong_count + 1
            wrong_instance_list.append(instance_index)

        print(" ")

    correct_rate = correct_count/dataset_length
    print("correct_count:", correct_count, "; correct_rate:", correct_rate)
    print("wrong instance:", wrong_instance_list)
    return correct_rate, wrong_instance_list


def publich_client(header_token_seq, index):
    rospy.wait_for_service('PAOC_online_reading_record_service')
    try:
        ask_to_publich = rospy.ServiceProxy('PAOC_online_reading_record_service', PAOCPerceptionSRV)
        resp1 = ask_to_publich(header_token_seq, index)
        return resp1
    except rospy.ServiceException:
        print("Service call failed: %s", rospy.ServiceException)
        #print "Service call failed: %s"%e


def goto_xcs_iterations():
    global iter_index
    global xcs_model
    "########################################"
    "goto single step XCS learning iteration, take action, have effect and received reward"
    "########################################"
    iter_index += 1
    goto_online_dataset = True  # this parameter is not useless in this case. it is overrided by learn_from_real_time_affect.
    learn_from_real_time_affect = True  # learning form real time affect, instead of from dataset.
    xcs_model = xcs_single_step_iteration(xcs_model, iter_index, goto_online_dataset, learn_from_real_time_affect)
    xcs_model.XCS_deletion_API(9000)  # pop_size =9000

def get_which_process_from_xcsHier():
    global iter_index_hier
    global xcs_model_hier
    global core_affect_perception

    if get_core_affect_space_perception():  # this will make sure core_affect_perception has data that comes from other threads.
        "###############################"
        "go to a step iteration"
        "###############################"
        iter_index_hier += 1
        action_xcs_hier = xcs_model_hier.XCS_get_prediction_API(core_affect_perception)
        print("#######################################!!!")
        print("action_xcs_hier: which process:", action_xcs_hier)
        print("#######################################!!!")
        return action_xcs_hier
    else:
        return 0

def callback_bumper(msg):
    global bumper_data  # store first time bumper data
    global current_bumper_data
    global first_collision_flag
    global check_bumper_time_integral_flag
    global has_collsion_this_period

    global bumper_data_stock
    global bumper_fire_maximum
    global bumper_fire_value
    global pub_bumper_fire_value

    "store bumper data when it is the first time"
    "when the bumper data has been process by do_xcs_action_process, the first_collision_flag will be reset."
    #print("bumper data", msg.data, ",  first_collision_flag", first_collision_flag)
    has_collsion_this_moment = False
    current_bumper_data = msg.data
    if not first_collision_flag:
        for a_bumper in msg.data:
            if a_bumper >= 0.5:
                has_collsion_this_moment = True

    if has_collsion_this_moment:
        bumper_data = msg.data # store data
        first_collision_flag = True
        print("set first_collision_flag = True, first collision happen")

    "check if the bumper happen at this period"
    if check_bumper_time_integral_flag:
        for a_bumper in msg.data:
            if a_bumper >= 0.5:
                has_collsion_this_period = True

    "##################################"
    "TO publish bumper fire"
    "##################################"

    "#####################################"
    "set the bumper data stock by updating the current data and keep fresh data within 3 seconds time duration"
    "#####################################"
    current_time = rospy.get_rostime()  # math.floor(rospy.get_rostime().secs)
    is_collision = 0
    for a_bamper_data in msg.data:
        if a_bamper_data > 0.5:
            is_collision = 1
    to_append = pd.DataFrame([[current_time, is_collision]], columns=("time", "collision"))
    bumper_data_stock = bumper_data_stock.append(to_append)
    "keep the resent 3 seconds"
    bumper_data_stock = bumper_data_stock.loc[bumper_data_stock["time"] > (current_time - rospy.Duration(2.5))]
    # => print("size of bumper_data_stock", bumper_data_stock.shape[0])
    """
    "#####################################"
    "establish fire curve"
    "#####################################"
    
    bumper_fire_value = 0
    bumper_fire = bumper_data_stock.loc[bumper_data_stock["collision"] == 1].shape[0]
    # print("ros current_time: ", current_time, "bumper_fire", bumper_fire, "bumper_fire_maximum", bumper_fire_maximum)
    if bumper_fire >= bumper_fire_maximum:
        "ignite curve"
        bumper_fire_maximum = bumper_fire
        bumper_fire_value = math.pow(bumper_fire, 3)  # poly
        # bumper_fire_value = 1/(1 + math.exp(-1*(bumper_fire + 10))) - 1/(1 + math.exp(-10)) # sigmoid
    else:
        "decay curve"
        delta = bumper_fire_maximum - bumper_fire
        bumper_fire_value = math.pow(bumper_fire_maximum, 3) - math.pow(delta, 3)
        # bumper_fire_value= 1/(1 + math.exp(-1*(bumper_fire_maximum + 10))) - 1/(1 + math.exp(-1*(delta + 10))) # sigmoid

        if bumper_fire_maximum - delta <= 1:
            bumper_fire_maximum = 0
    #print("bumper_fire_value:", bumper_fire_value)
    # if bumper_fire_value < 0:
    #    bumper_fire_value = 0
    bumper_fire_value = np.clip(-0.25 * bumper_fire_value, -1000, 1)  # reschedule to -1000 and 0
    """

    "#############################"
    "normalise bumper value into 1000 or -1000 "
    "#############################"
    bumper_fire_value = 50 #1000
    bumper_fire = bumper_data_stock.loc[bumper_data_stock["collision"] == 1].shape[0]
    if bumper_fire >= 2:
        bumper_fire_value = -1000

    pub_bumper_fire_value.publish(float(bumper_fire_value))


def callback_odom(msg):
    global speed_odom_value
    global speed_odom_stock
    global pub_speed_odom_value
    """
    x = msg.twist.twist.linear.x * 100
    y = msg.twist.twist.linear.y * 100
    #speed_odom_value = round(math.sqrt(math.pow(x, 2) + math.pow(y, 2)), 2)
    speed_odom_value = round((math.pow(x, 2) + math.pow(y, 2)), 1) * 4
    """
    x = msg.twist.twist.linear.x
    y = msg.twist.twist.linear.y
    w = msg.twist.twist.angular.z
    #speed_odom_value = round(445 * math.pow(x, 2) + 445 * math.pow(y, 2) + 34 * math.pow(w, 2), 1)  # reschedule to -1000 and 0
    speed_odom_value = round(445 * math.pow(x, 2) + 34 * math.pow(w, 2), 1)  # reschedule to -1000 and 0
    #speed_odom_value = round((math.sqrt(math.pow(x, 2) + math.pow(y, 2)) + 0.25 * math.pow(w, 2)) *50, 1)  # reschedule to -1000 and 0
    speed_odom_value = np.clip(speed_odom_value, 0, 8)  # speed_odom_value are 500 for process reflex, 800 and 1000 for move-base process
    speed_odom_value = math.ceil(speed_odom_value)  # floor bring speed value to 10 states...

    #print("                             instant odom speed:     ", speed_odom_value, "; x=",x, "; y=", y, "; w=", w)
    """
    remove integral of speed
    
    #store for integral
    current_time = rospy.get_rostime()  # math.floor(rospy.get_rostime().secs)
    to_append = pd.DataFrame([[current_time, speed_odom_value]], columns=("time", "velocity"))

    speed_odom_stock = speed_odom_stock.append(to_append)
    "keep the resent 0.2 seconds"
    speed_odom_stock = speed_odom_stock.loc[speed_odom_stock["time"] > (current_time - rospy.Duration(0.5))]
    speed_odom_value = math.ceil(speed_odom_stock["velocity"].mean()) # floor bring speed value to 10 states...
    #print("                             publish mean odom speed: ", speed_odom_value)
    #speed_odom_value = np.clip(speed_odom_value, 0, 4001)
    """
    pub_speed_odom_value.publish(float(speed_odom_value))

def callback_pathPlanning(msg):
    global flag_to_active_path_planning_cmd
    global pub_cmd
    #print("subscribe path planner cmd:", msg)
    if flag_to_active_path_planning_cmd:
        pub_cmd.publish(msg)

def pioneer_take_action(xcs_action):
    global first_collision_flag
    global flag_to_active_path_planning_cmd

    " publich 6 actions"
    pub = rospy.Publisher('/p3dx/cmd_vel', Twist, queue_size=5)
    # test=> xcs_action = np.random.randint(7, 13)
    cmd = set_vel(0, 0)  # default
    "for reflex process"
    if xcs_action == 1:
        cmd = set_vel(0.1, 0)
    if xcs_action == 2:
        cmd = set_vel(0.05, -0.32) # right
    if xcs_action == 3:
        cmd = set_vel(0.05, 0.32) # left

    if xcs_action == 4:
        cmd = set_vel(-0.1, 0)
    if xcs_action == 5:
        cmd = set_vel(-0.05, 0.32)
    if xcs_action == 6:
        cmd = set_vel(-0.05, -0.32)

    "for move_base process"
    """
    if xcs_action == 7:
        cmd = set_vel(0.1, 0)
    if xcs_action == 8:
        cmd = set_vel(0.05, -0.32) # right
    if xcs_action == 9:
        cmd = set_vel(0.05, 0.32) # left

    if xcs_action == 10:
        cmd = set_vel(-0.1, 0)
    if xcs_action == 11:
        cmd = set_vel(-0.05, 0.32)
    if xcs_action == 12:
        cmd = set_vel(-0.05, -0.32)
    """
    if xcs_action == 7:
        cmd = set_vel(0.15, 0)
    if xcs_action == 8:
        cmd = set_vel(0.1, -0.32) # right
    if xcs_action == 9:
        cmd = set_vel(0.1, 0.32) # left

    if xcs_action == 10:
        cmd = set_vel(-0.15, 0)
    if xcs_action == 11:
        cmd = set_vel(-0.1, 0.32)
    if xcs_action == 12:
        cmd = set_vel(-0.1, -0.32)


    "stop"
    if xcs_action == 100:
        cmd = set_vel(0, 0)

    "pathplanning"
    if xcs_action == 111:
        flag_to_active_path_planning_cmd = True
    else:
        flag_to_active_path_planning_cmd = False

    if xcs_action == 111:
        "will be take effect in republish cmd in callback function through flag_to_active_path_planning_cmd"
        return

    "##################"
    "take effect"
    "##################"
    pub.publish(cmd)
    rate = rospy.Rate(15)
    pub_iter = 0
    while pub_iter < 3:

        print(" pioneer_take_action:", xcs_action)
        pub.publish(cmd)
        rate.sleep()
        pub_iter += 1

    "###############################"
    "reset first_collision_flag"
    "###############################"
    #first_collision_flag = False
    #rospy.sleep(0.2)

    "##################"
    "stop"
    "##################"
    #cmd = set_vel(0, 0)
    #pub.publish(cmd)


def set_vel(vel_x, vel_z):
    cmd = Twist()
    cmd.linear.x = vel_x
    cmd.linear.y = 0
    cmd.linear.z = 0
    cmd.angular.x = 0
    cmd.angular.y = 0
    cmd.angular.z = vel_z
    return cmd


def get_perception_client(header_token_seq, index):
    #seq = 0
    #modifier_request_value = 0.3
    print("get_perception_client header_token_seq, index:", header_token_seq, index)
    resp = publich_client(header_token_seq, index)
    preception = [resp.location_X, resp.location_Y, resp.radius_inflation]
    groundTrue = resp.groundTrue
    dataset_length = resp.dataset_length
    print("get perception and groundTrue: ", resp)
    return preception, groundTrue, dataset_length


def get_length_of_dataset(path_dataset):
    data_record = pd.read_csv(path_dataset)
    print("data_record total line:", data_record.shape[0])
    return data_record.shape[0]


def get_a_row_from_dataset(random_mode, row_index, path_dataset):
    #path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/1_PAOC_C.csv"
    data_record = pd.read_csv(path_dataset)
    #print("data_record total line:", data_record.shape[0])

    if random_mode:
        "toselect random one from data_record.shape[0] "
        row_index = np.random.randint(data_record.shape[0])

     #   a = np.random.choice(np.arange(0, maze_iter.getActionSize()), p=probobility_of_actions_normal)
        # print("3. likely action(", a, ") with pai2 (", pai2, ")")
     #   a = np.random.randint(maze_iter.getActionSize())  # todo:remove this line

    print("data_record a row :",  row_index, " from length:",data_record.shape[0])
    #print("data_record location_Y:", data_record.loc[row_index, "location_Y"])
    #print("data_record location_X:", data_record.iloc[row_index, 0])

    #perception = pd.DataFrame(columns=['location_X', 'location_Y', 'location_Y', 'class'])
    print("perception :", data_record.loc[row_index])
    return data_record.loc[row_index]


def get_perception_localFile(path_dataset, random_mode=True, row_index=0):
    data_row = get_a_row_from_dataset(random_mode, row_index, path_dataset)
    perception = [data_row.loc["location_X"], data_row.loc["location_Y"], data_row.loc["radius_inflation"]]
    groundTrue = data_row.loc["class"]
    print("==>", perception)
    print("type of groundTrue", type(groundTrue))
    return perception, groundTrue


def do_nothing_process():
    print("do_nothing_process")
    pass


def do_xcs_action_process():
    global xcs_action_process_flag
    global bumper_data
    global first_collision_flag

    print("do_xcs_action_process")

    "set flag to lock the process"
    xcs_action_process_flag = True

    "get xcs action effect"
    goto_xcs_iterations()

    "reset flags"
    first_collision_flag = False
    xcs_action_process_flag = False


def do_random_action_process():
    #print("do_random_action_process")

    "############################################"
    "pioneer take random action"
    "############################################"
    #print("not collisions")
    random_action = np.random.randint(0, 100)
    if random_action < 30:
        print("#################################################")
        print(" do_random_action_process, randomly take action 1\n")
        pioneer_take_action(7)  # go ahead for 10 seconds
        #rospy.sleep(1)
    if 30 <= random_action < 60:
        print("#################################################")
        print(" do_random_action_process randomly take action 4\n")
        pioneer_take_action(10)  # go back for 10 seconds
        #rospy.sleep(1)

    if 60 <= random_action < 70:
        print("#################################################")
        print(" do_random_action_process randomly take action 2\n")
        pioneer_take_action(8)
        #rospy.sleep(1)
    if 70 <= random_action < 80:
        print("#################################################")
        print(" do_random_action_process, randomly take action 3\n")
        pioneer_take_action(9)
        #rospy.sleep(1)

    if 80 <= random_action < 90:
        print("#################################################")
        print(" do_random_action_process, randomly take action 5\n")
        pioneer_take_action(11)
        #rospy.sleep(1)
    if 90 <= random_action <= 100:
        print("#################################################")
        print(" do_random_action_process, randomly take action 6\n")
        pioneer_take_action(12)
        #rospy.sleep(1)


def do_path_planning():
    global path_planning_cmd
    print("#################################################")
    print(" do_path_planning \n")
    pioneer_take_action(111)

process = {
    0: do_nothing_process,
    1: do_xcs_action_process,
    2: do_path_planning,
    3: do_random_action_process,
    4: do_random_action_process,
    5: do_random_action_process,
    6: do_random_action_process,

}

if __name__ == "__main__":

    global iter_index
    global xcs_model

    global iter_index_hier
    global xcs_model_hier

    "#########################################################" \
    "these two global parameters are for collision detection, if there is a collision, " \
    "set first_collision_flag = true and save the bumper_data." \
    "when first_collision_flag == true, no more bumper data will be perceived."
    global first_collision_flag
    global bumper_data
    "#########################################################" \
    "these two flag are for the main loop frequency check "
    " priority: xcs_action_process_flag > first_collision_flag > random_action_process_flag"
    " xcs_action_process_flag has the highest priority. " \
    " when xcs_action_process_flag = True, the loop pass." \
    " random_action_process_flag has the secondary priority." \
    " When random_action_process_flag = True and xcs_action_process_flag = True, go to the random_action_process"
    global xcs_action_process_flag
    global random_action_process_flag
    global time_inhibition

    global check_bumper_time_integral_flag
    global has_collsion_this_period
    global path_and_folder

    global bumper_fire_value
    global speed_odom_value
    global core_affect_perception

    global bumper_data_stock
    global bumper_fire_maximum
    global pub_bumper_fire_value
    global pub_speed_odom_value
    global speed_odom_stock
    global current_bumper_data
    global project_name_for_director
    global flag_to_active_path_planning_cmd
    global pub_cmd


    "#############################"
    "initiate global parameters"
    "#############################"
    project_name_for_director = "pathPlanning"

    path_and_folder = "./src/robot_nav_plan/result/"+project_name_for_director+"_l/"

    bumper_data = tuple()
    current_bumper_data = tuple()
    flag_to_active_path_planning_cmd = False
    iter_index = 0
    iter_index_hier = 0
    first_collision_flag = False # if true, goto process bumper data
    check_bumper_time_integral_flag = False
    has_collsion_this_period = False

    bumper_fire_value = 0
    speed_odom_value = 0
    core_affect_perception = tuple()

    bumper_data_stock = pd.DataFrame(columns=("time", "collision"))
    bumper_fire_maximum = 0

    speed_odom_stock = pd.DataFrame(columns=("time", "velocity"))

    "#############################"
    "initiate xcs agent"
    "#############################"
    classifiers_population = "lastest"
    xcs_model = XCS_agent_initiate("/Results/doThisTask.csv", "/reflex_hire_l_OnlyBumper/lastest")
    goto_online_dataset = True # this parameter is not useless in this case. it is overrided by learn_from_real_time_affect.
    learn_from_real_time_affect = True # learning form real time affect, instead of from dataset.
    "#############################"
    "initiate xcs hierarchy agent"
    "#############################"
    classifiers_population = None
    #xcs_model_hier = XCS_agent_initiate("/Results/doThisTask2.csv", classifiers_population)
    xcs_model_hier = XCS_agent_initiate("/Results/doThisTask2.csv", "/reflex_equal_speedFunction_corridor_h/lastest")
    "##############################"
    "initiate ros interactions"
    "##############################"
    rospy.init_node("Reflex_online_learning", anonymous=False)

    pub_cmd = rospy.Publisher('/p3dx/cmd_vel', Twist, queue_size=1) #in pioneer_take_action
    pub_bumper_fire_value = rospy.Publisher('/p3dx/bumper_fire_value', Float64, queue_size=5)
    pub_speed_odom_value = rospy.Publisher('/p3dx/speed_odom_value', Float64, queue_size=5)
    pub_which_process = rospy.Publisher('/p3dx/which_process', Float64, queue_size=5)

    rospy.Subscriber('/p3dx/bumper_state_date', Float64MultiArray, callback_bumper)
    rospy.Subscriber('/p3dx/odom', Odometry, callback_odom)
    rospy.Subscriber('/p3dx/Multiplexer_PathPlanner/cmd_vel', Twist, callback_pathPlanning)



    "##############################"
    "goto iteration loop, and wait for bumper callback"
    "##############################"
    rate = rospy.Rate(0.1)
    #iter_index = 0
    inhibition_timer()
    which_process = 1
    last_which_process = 1
    while not rospy.is_shutdown():
        "the main loop waiting for bumper state to call back..."
        "##########################################"
        " apply xcs_model_hier to select which_process that used to be selected by a preset priority"
        "##########################################"
        which_process = get_which_process_from_xcsHier()
        #which_process = 3
        print("\n which_process:", which_process)

        "##########################################"
        " execute process and take effect"
        "##########################################"
        process.get(which_process, 0)()
        pub_which_process.publish(float(which_process))

        """
        
        delay = rospy.Time.from_sec(time.time()) - time_inhibition      

        if which_process == 1:
            process.get(which_process, 0)()

        if which_process == 2:
            if last_which_process == 1:
                inhibition_timer()
                process.get(which_process, 0)()

            elif delay > rospy.Duration(10):
                inhibition_timer()
                process.get(which_process, 0)()

        last_which_process = which_process
        """
        """

        elif delay > rospy.Duration(5):
            inhibition_timer()
            process.get(which_process, 0)()
        else:
            # print("which_process:", which_process)
            process.get(0, 0)()
            pass
        """
        "##########################################"
        "reward update"
        "##########################################"
        if which_process == 1:
            rospy.sleep(1)
            pass
        else:
            rospy.sleep(3)
        "only consider bumper fire value"
        reward_episode = bumper_fire_value + speed_odom_value*100
        #reward_episode = speed_odom_value + bumper_fire_value
        print("\n*******************************")
        print("hierarchy reward:", reward_episode)
        print("hierarchy reward:", reward_episode)
        print("hierarchy reward:", reward_episode)
        print("*******************************")

        reward_method = "even"
        xcs_model_hier.XCS_update_reward_API(reward_episode, reward_method, True, 0.2)

        "##########################################"
        "save hierarchy classifier"
        "##########################################"
        # path_and_folder = "./src/robot_nav_plan/result/reflex_temp/"
        xcs_model_hier.XCS_save_classifiers_API(iter_index_hier, "./src/robot_nav_plan/result/"+project_name_for_director+"_h/")
        xcs_model_hier.XCS_deletion_API(900)  # pop_size =9000

        print("main loop sleeping 2 second...")
        rospy.sleep(2)

        #rate.sleep()




