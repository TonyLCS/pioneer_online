#!/usr/bin/env python

import sys
import rospy
import time
import math
import os
import copy
import csv
import pandas as pd

#   from xcs_api import XCS  # import xcs agent API
#   from visualization import Visualization  # import Visualization to show results
import random
import csv
#   import pandas as pd
#   import numpy as np

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64, Int64, Bool

from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist

from sensor_msgs.msg import LaserScan

from nav_msgs.msg import Odometry

from robot_nav_plan.srv import *


from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io, feature
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped

import subprocess

from xcs_api import XCS  # import xcs agent API

from move_base_msgs.msg import MoveBaseActionResult
from robot_nav_plan.srv import *
from nav_msgs.srv import *
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path
from geometry_msgs.msg import Twist
from actionlib_msgs.msg import GoalStatusArray
from move_base_msgs.msg import MoveBaseActionResult


def costmap_nn_reinforcerNode_client(seq):
    rospy.wait_for_service('expectation_NN_prediction_service')
    try:
        print("try to call service expectation_NN_prediction_service with seq = ", seq)
        getCostmapNNPrediction = rospy.ServiceProxy('expectation_NN_prediction_service', CostMapNNMSG)
        resp1 = getCostmapNNPrediction(seq)
        return resp1.prediction
    except rospy.ServiceException:
        print("Service call failed")



def callback_check_path(path):
    global path_available
    global has_attribute_3

    path_length = len(path.poses)
    if path_length >= 3:
        path_available = 1
    else:
        path_available = 0

    print("path available: ", path_available)
    has_attribute_3 = True

def callback_start_perception(msg):
    global has_attribute_2
    global has_attribute_3
    global path_available
    global pub_waiting_perception

    #has_attribute_3 = False
    print("\n===>starting waiting perception... wait for 6 second for perception.")
    if msg.data:
        "do perceive"
        seq = 1
        print("expectation_nn_prediction client ask for prediction: whether has an obstacle occupied?")
        prediction = costmap_nn_reinforcerNode_client(seq)
        #print("expectation_nn_prediction client receive prediction:", prediction)
        print(" predicted label: ", prediction.index(max(prediction)), " with probability :", max(prediction))
        predict_label = prediction.index(max(prediction))
        if prediction.index(max(prediction)) == 0:
            print("No, ", max(prediction)*100, " probability that it is free of obstacle that occupies the targeted position detected")
        else:
            print("Yes, ", max(prediction)*100, " probability that it has obstacle that occupies the targeted position.")


        #print("waiting 1 second for right perception because of network delay")
        #rospy.sleep(6)
        #if has_attribute_2 and has_attribute_3:
        #if has_attribute_3:
        time_to_wait = np.random.randint(0, 3) * 10  # A2
        #time_to_wait = 0
        #waiting_perception = (path_available, time_to_wait)
        waiting_perception = (predict_label, time_to_wait)
        print("==>waiting perception ( as condition of classifier): has_obstacle", predict_label, "; time_to_wait:",time_to_wait, "\n")
        waiting_perception_1 = Float64MultiArray()
        waiting_perception_1.data = waiting_perception
        pub_waiting_perception.publish(waiting_perception_1)
         #   has_attribute_3 = False


if __name__ == "__main__":
    """
        subsribe topic to establish waiting perception for publish
         A1: time taken; A2: waiting time; A3: Blocking recognision or A4 (path available) 
         
    """
    rospy.init_node("acmca_l1_5_waiting_perception", anonymous=False)

    waiting_perception = tuple()
    pub_waiting_perception = rospy.Publisher('/p3dx/ACMCA/L1_waiting_perception/waiting_perception', Float64MultiArray, queue_size=1)


    time_to_wait = 0 #np.random.randint(1, 15)  # A2
    has_attribute_2 = True


    path_available = 0     # A3
    has_attribute_3 = False
    #rospy.Subscriber("/p3dx/path_planner/navfn/plan", Path, callback_check_path) replaced by costmap_nn_reinforcerNode_client

    rospy.Subscriber('/p3dx/ACMCA/task_iteration/status', Bool, callback_start_perception)


    print("acmca_l1_5_waiting_perception spin...")

    rospy.spin()
    """
        while not rospy.is_shutdown():
        if has_attribute_1 and has_attribute_2 and has_attribute_3:

            time_to_wait = np.random.randint(1, 15)  # A2

            waiting_perception = (time_counting_consumption, time_to_wait, path_available)
            print("waiting perception: ", waiting_perception)
            waiting_perception_1 = Float64MultiArray()
            waiting_perception_1.data = waiting_perception
            pub_waiting_perception.publish(waiting_perception_1)
            has_attribute_1 = False
            has_attribute_3 = False

    """

