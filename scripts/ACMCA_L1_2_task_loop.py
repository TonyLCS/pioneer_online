#!/usr/bin/env python

import sys
import rospy
import time
import math
import os
import copy
import csv
import pandas as pd

#   from xcs_api import XCS  # import xcs agent API
#   from visualization import Visualization  # import Visualization to show results
import random
import csv
#   import pandas as pd
#   import numpy as np

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64, Int64, Bool

from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist

from sensor_msgs.msg import LaserScan

from nav_msgs.msg import Odometry

from robot_nav_plan.srv import *


from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io, feature
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped

import subprocess

from xcs_api import XCS  # import xcs agent API

from move_base_msgs.msg import MoveBaseActionResult
from robot_nav_plan.srv import *
from nav_msgs.srv import *
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path
from geometry_msgs.msg import Twist
from actionlib_msgs.msg import GoalStatusArray
from move_base_msgs.msg import MoveBaseActionResult


def rotate(heading, attitude, bank):
    # Assuming the angles are in radians.

    c1 = cos(heading / 2)
    s1 = sin(heading / 2)
    c2 = cos(attitude / 2)
    s2 = sin(attitude / 2)
    c3 = cos(bank / 2)
    s3 = sin(bank / 2)
    c1c2 = c1 * c2
    s1s2 = s1 * s2

    w = c1c2 * c3 - s1s2 * s3
    x = c1c2 * s3 + s1s2 * c3
    y = s1 * c2 * c3 + c1 * s2 * s3
    z = c1 * s2 * c3 - s1 * c2 * s3
    return x, y, z, w


def combine_position(x, y, angle):
    a_targeted_postition = PoseStamped()
    a_targeted_postition.header.frame_id = 'map'
    a_targeted_postition.pose.position.x = x
    a_targeted_postition.pose.position.y = y
    a_targeted_postition.pose.position.z = 0

    #print "angle -deg = %s"%angle
    angle = radians(angle)
    #print "angle -rad = %s"%angle
    (a_targeted_postition.pose.orientation.x, a_targeted_postition.pose.orientation.y,
     a_targeted_postition.pose.orientation.z, a_targeted_postition.pose.orientation.w) = rotate(0, angle, 0)
    #print "a_targeted_postition.pose.orientation.z= %s, a_targeted_postition.pose.orientation.w= %s"%(a_targeted_postition.pose.orientation.z, a_targeted_postition.pose.orientation.w)
    return a_targeted_postition


def manual_input():
    global do_manual_input
    try:
        position_input_x = float(input("entry position.x (metre):"))
        print("entry targeted position.x = ", position_input_x)
    except ValueError:
        print("entry error")
    try:
        position_input_y = float(input("entry position.y (metre):"))
        print("entry targeted position.y = ", position_input_y)
    except ValueError:
        print("entry error")
    try:
        position_input_ang = float(input("entry position.angular (degree):"))
        print("entry targeted position.angular = ", position_input_ang, "degree")
    except ValueError:
        print("entry error")
    """
    try:
        last_manual = float(input("do manual :True = 1 False =0::"))
        if last_manual == 0:
            do_manual_input = False
        print("do_manual_input = ", do_manual_input)
    except ValueError:
        print("entry error")
    """
    return position_input_x, position_input_y, position_input_ang

def do_continuous_process():
    global action_pattern_pub
    print("cxxxx{|::::::::::::::::::::::> do_continuous_process (path_following)")
    action_pattern_pub.publish(1)

def moving_to_targeted_position_client(token, seq, targeted_position):
    rospy.wait_for_service('multiple_cmdVel_service')
    try:
        print("     client call multiple_cmdVel_service")
        request_the_server = rospy.ServiceProxy('multiple_cmdVel_service', TargetedPositionMSG)
        resp1 = request_the_server(token, seq, targeted_position)  # calls the server with a request
        print("     Service success response.")
        return resp1.current_position   # get a response from the server
    except rospy.ServiceException:
        print("     Service call failed")


def callback_get_reward(msg):
    global flag_complete_an_iteration
    print("reward is :", msg.data)
    flag_complete_an_iteration = True

def callback_get_init_position_reward(msg):
    global flag_complete_initial_position
    print("init reward is :", msg.data)
    flag_complete_initial_position = True


if __name__ == "__main__":
    """
        1. publish the targeted position and monitor result;
        2. watch dog
        3. calculate the reward 
        4. goto initial place when one iteration was completed.
    """
    rospy.init_node("task_loop", anonymous=False)

    flag_complete_initial_position = False
    flag_complete_an_iteration = False

    pub = rospy.Publisher('/p3dx/path_planner/goal', PoseStamped, queue_size=1)  # set the goal for the path planner
    pub_start_xcs_and_timer = rospy.Publisher('/p3dx/ACMCA/task_iteration/status', Bool, queue_size=1)
    action_pattern_pub = rospy.Publisher('/p3dx/ACMCA/L2_waiting/action', Float64MultiArray, queue_size=1) #in pioneer_take_action
    rospy.Subscriber('/p3dx/ACMCA/task_iteration/time_consumption', Float64, callback_get_reward)

    "init position"
    pub_start_init_timer = rospy.Publisher('/p3dx/ACMCA/task_iteration/init_status', Bool, queue_size=1)
    rospy.Subscriber('/p3dx/ACMCA/task_iteration/init_time_consumption', Float64, callback_get_init_position_reward)

    "dynamic obstacle"
    pub_dynamic_obstacle_activate = rospy.Publisher('/p3dx/ACMCA/dynamic_obstacle/time_delay_and_location', Float64MultiArray, queue_size=1)


    print(".......................")
    print("......goto initial  place..........")
    pub_start_init_timer.publish(True)

    targeted_position = combine_position(2, 0, 0)
    pub.publish(targeted_position)

    init_msg = Float64MultiArray()
    time_to_wait = 0  # no waiting
    action_xcs = 4  # path following
    init_msg.data = (time_to_wait, action_xcs)
    action_pattern_pub.publish(init_msg)

    print("\n")
    print("goto initial place by publish: time_to_wait=", time_to_wait, ";  action_xcs=", action_xcs)
    temp_debug_timer = rospy.get_time()
    while not flag_complete_initial_position:
        if rospy.get_time() - temp_debug_timer > 10:
            #waiting for 20 second
            break
        rospy.sleep(0.2)
    flag_complete_initial_position = False

    while not rospy.is_shutdown():



        #rospy.sleep(0.1)
        "1. prepare for iteration, go to initial place"

        print("start a task.\n")
        #targeted_position = combine_position(1, 0, 0)
        #position_input_x, position_input_y, position_input_ang = manual_input()
        #targeted_position = combine_position(position_input_x, position_input_y, position_input_ang)
        targeted_position = combine_position(4, -0.3, 0)
        pub.publish(targeted_position)

        if np.random.randint(0, 100) > 30:
            print("active dynamic obstacle! randomly call dynamic obstacle with 70% probability")
            obstacle_data = (25, 4, -0.3, 4)  # 15+ 6 (perception waiting for updating) + 4 (loop delay)
            obstacle_msg_1 = Float64MultiArray()
            obstacle_msg_1.data = obstacle_data
            pub_dynamic_obstacle_activate.publish(obstacle_msg_1)
            rospy.sleep(3)

        #waiting for perception delay causing by network delay
        rospy.sleep(1) #waiting for the costmap update....

        pub_start_xcs_and_timer.publish(True)



        flag_complete_an_iteration= False
        while not flag_complete_an_iteration:
            rospy.sleep(0.2)

        flag_complete_an_iteration = False

        print(".......complete an iteration of the task...........")
        print("...................................................")
        rospy.sleep(1)
        print("go to starting position")
        targeted_position = combine_position(2, 0, 0)
        pub.publish(targeted_position)


        init_msg = Float64MultiArray()
        time_to_wait= 0 #no waiting
        action_xcs = 4 #path following
        init_msg.data = (time_to_wait, action_xcs)
        action_pattern_pub.publish(init_msg)
        pub_start_init_timer.publish(True)

        print("\n")
        print("goto initial place by publish: time_to_wait=", time_to_wait, ";  action_xcs=", action_xcs)

        while not flag_complete_initial_position:
            rospy.sleep(0.2)
        flag_complete_initial_position = False

        print("reach the initial position, and stop.\n")

        "stop"
        time_to_wait = 0  # no waiting
        action_xcs = 3  # freeze and quit
        init_msg.data = (time_to_wait, action_xcs)
        action_pattern_pub.publish(init_msg)

        rospy.sleep(3)
        print("end of an iteration loop.\n")

