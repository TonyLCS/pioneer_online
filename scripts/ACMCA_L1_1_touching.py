#!/usr/bin/env python

import sys
import rospy
import time
import math
import os
import copy
import csv
import pandas as pd

#   from xcs_api import XCS  # import xcs agent API
#   from visualization import Visualization  # import Visualization to show results
import random
import csv
#   import pandas as pd
#   import numpy as np

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64

from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist

from sensor_msgs.msg import LaserScan

from nav_msgs.msg import Odometry

from robot_nav_plan.srv import *


from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io, feature
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped

import subprocess

from xcs_api import XCS  # import xcs agent API


def callback_bumper(msg):
    "#####################################"
    "set the bumper data stock by updating the current data and keep fresh data within 3 seconds time duration"
    "#####################################"
    global pub_touching_perception # to pub
    global touching_perception_stock #to store
    global pub_touching_stimulus #to pub

    "setup stock to record the touching perception within the time duration"
    current_time = rospy.get_rostime()  # math.floor(rospy.get_rostime().secs)
    is_collision = 0
    for a_bamper_data in msg.data:
        if a_bamper_data > 0.5:
            is_collision = 1
    to_append = pd.DataFrame([[current_time, is_collision]], columns=("time", "collision"))
    touching_perception_stock = touching_perception_stock.append(to_append)
    "publish the touching perception when there is a collision. this will initiate the reflex node."
    if is_collision:
        touching_perception = msg.data
        print("touching_perception:", touching_perception)
        pub_touching_perception.publish(msg)

    "update/keep the resent 3 seconds"
    touching_perception_stock = touching_perception_stock.loc[touching_perception_stock["time"] > (current_time - rospy.Duration(3))]
    # => print("size of bumper_data_stock", bumper_data_stock.shape[0])
    """
    "#####################################"
    "establish fire curve"
    "#####################################"

    bumper_fire_value = 0
    bumper_fire = bumper_data_stock.loc[bumper_data_stock["collision"] == 1].shape[0]
    # print("ros current_time: ", current_time, "bumper_fire", bumper_fire, "bumper_fire_maximum", bumper_fire_maximum)
    if bumper_fire >= bumper_fire_maximum:
        "ignite curve"
        bumper_fire_maximum = bumper_fire
        bumper_fire_value = math.pow(bumper_fire, 3)  # poly
        # bumper_fire_value = 1/(1 + math.exp(-1*(bumper_fire + 10))) - 1/(1 + math.exp(-10)) # sigmoid
    else:
        "decay curve"
        delta = bumper_fire_maximum - bumper_fire
        bumper_fire_value = math.pow(bumper_fire_maximum, 3) - math.pow(delta, 3)
        # bumper_fire_value= 1/(1 + math.exp(-1*(bumper_fire_maximum + 10))) - 1/(1 + math.exp(-1*(delta + 10))) # sigmoid

        if bumper_fire_maximum - delta <= 1:
            bumper_fire_maximum = 0
    #print("bumper_fire_value:", bumper_fire_value)
    # if bumper_fire_value < 0:
    #    bumper_fire_value = 0
    bumper_fire_value = np.clip(-0.25 * bumper_fire_value, -1000, 1)  # reschedule to -1000 and 0
    """

    "#############################"
    "normalise bumper value into 1000 or -1000 "
    "#############################"
    touching_stimulus = 50  # 1000
    bumper_fire = touching_perception_stock.loc[touching_perception_stock["collision"] == 1].shape[0]
    if bumper_fire >= 2:
        touching_stimulus = -1000
    print("bumper_fire_value:", touching_stimulus, "; bumper_fire:", bumper_fire)
    pub_touching_stimulus.publish(float(bumper_fire))



if __name__ == "__main__":
    "transfer bumper reading to touching perception and stimulus"

    "##############################"
    "initiate ros interactions"
    "##############################"
    rospy.init_node("acmca_l1_touching", anonymous=False)

    pub_touching_perception = rospy.Publisher('/p3dx/ACMCA/touching/perception', Float64MultiArray, queue_size=1)

    touching_perception_stock = pd.DataFrame(columns=("time", "collision"))
    pub_touching_stimulus = rospy.Publisher('/p3dx/ACMCA/touching/stimulus', Float64, queue_size=1)

    "this node is active by callback_bumper"
    rospy.Subscriber('/p3dx/bumper_state_date', Float64MultiArray, callback_bumper)

    print("acmca_l1_touching spin...")
    rospy.spin()





