#!/usr/bin/env python

import sys
import rospy
import time
import math
import os
import copy
import csv
import pandas as pd

#   from xcs_api import XCS  # import xcs agent API
#   from visualization import Visualization  # import Visualization to show results
import random
import csv
#   import pandas as pd
#   import numpy as np

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64, Int64, Bool

from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist

from sensor_msgs.msg import LaserScan

from nav_msgs.msg import Odometry

from robot_nav_plan.srv import *


from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io, feature
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped

import subprocess

from xcs_api import XCS  # import xcs agent API

from move_base_msgs.msg import MoveBaseActionResult
from robot_nav_plan.srv import *
from nav_msgs.srv import *
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path
from geometry_msgs.msg import Twist
from actionlib_msgs.msg import GoalStatusArray
from move_base_msgs.msg import MoveBaseActionResult


def rotate(heading, attitude, bank):
    # Assuming the angles are in radians.

    c1 = cos(heading / 2)
    s1 = sin(heading / 2)
    c2 = cos(attitude / 2)
    s2 = sin(attitude / 2)
    c3 = cos(bank / 2)
    s3 = sin(bank / 2)
    c1c2 = c1 * c2
    s1s2 = s1 * s2

    w = c1c2 * c3 - s1s2 * s3
    x = c1c2 * s3 + s1s2 * c3
    y = s1 * c2 * c3 + c1 * s2 * s3
    z = c1 * s2 * c3 - s1 * c2 * s3
    return x, y, z, w


def combine_position(x, y, angle):
    a_targeted_postition = PoseStamped()
    a_targeted_postition.header.frame_id = 'map'
    a_targeted_postition.pose.position.x = x
    a_targeted_postition.pose.position.y = y
    a_targeted_postition.pose.position.z = 0

    #print "angle -deg = %s"%angle
    angle = radians(angle)
    #print "angle -rad = %s"%angle
    (a_targeted_postition.pose.orientation.x, a_targeted_postition.pose.orientation.y,
     a_targeted_postition.pose.orientation.z, a_targeted_postition.pose.orientation.w) = rotate(0, angle, 0)
    #print "a_targeted_postition.pose.orientation.z= %s, a_targeted_postition.pose.orientation.w= %s"%(a_targeted_postition.pose.orientation.z, a_targeted_postition.pose.orientation.w)
    return a_targeted_postition


def calculate_reward(duration):
    global pub_time_consumption
    global watchdog

    reward = 100000/(duration + 50) - 1000

    print("total reward =", reward, " based on time_consumption:", duration)
    pub_time_consumption.publish(reward)

def callback_slammed_position(msg):
    # level 5
    global slam_current_position
    global has_currentposition
    slam_current_position = msg
    has_currentposition = True
    #print "get slam_current_position "

def callback_move_base_result_check(move_base_result):
    global goalStatuesArray_has_active  # input
    global achieve_targeted_position  # output: flag for go to a targeted position
    global slam_current_position
    global pub_goal

    move_base_result_status = move_base_result.status.status
    if goalStatuesArray_has_active:
        if move_base_result_status == 3:
            if (slam_current_position.pose.position.x - 2 ) * (slam_current_position.pose.position.y) <0.5:
                achieve_targeted_position = True
            else:
                targeted_position = combine_position(2, 0, 0)
                pub_goal.publish(targeted_position)



def callback_state_check(goalStatuesArray):
    # level 5
    global goalStatuesArray_has_active  # output result

    statues_length = len(goalStatuesArray.status_list)

    if statues_length >= 1:
        if goalStatuesArray.status_list[statues_length - 1].status == 1 \
                or goalStatuesArray.status_list[statues_length - 1].status == 2:
            goalStatuesArray_has_active = True


def callback_start_init_timer(msg):
    global achieve_targeted_position  # flag for go to a targeted position
    global goalStatuesArray_has_active
    global watchdog
    print("in callback_start_init_timer")
    if msg.data:
        achieve_targeted_position = False
        goalStatuesArray_has_active = False

        time_start = rospy.get_time()

        # sleep until it has achieve the target position
        count = 1
        rospy.Subscriber('/p3dx/move_base/result', MoveBaseActionResult,
                         callback_move_base_result_check)  # make sure robot achieve the new goal

        while not achieve_targeted_position:


            rospy.Subscriber('/p3dx/move_base/status', GoalStatusArray, callback_state_check)  # make sure it is a new goal

            rospy.sleep(0.2)
            duration = rospy.get_time() - time_start
            #pub_time_counting_consumption.publish(duration)
            print("=> take time: ", duration, " s")
            if duration > watchdog:
                print("service timeout!!!")
                #return duration
                break

        print("\n\n total duration => take time: ", duration, " s\n")

        calculate_reward(duration)

if __name__ == "__main__":
    """
        1. calculate the reward by total time consumption
        2. watch dog
        return duration
    """
    rospy.init_node("initialPosition_time_consumption_calculator", anonymous=False)

    achieve_targeted_position = False
    goalStatuesArray_has_active = False
    duration = 0
    watchdog = 200 #go back

    pub_time_consumption = rospy.Publisher('/p3dx/ACMCA/task_iteration/init_time_consumption', Float64, queue_size=1)
    rospy.Subscriber('/p3dx/ACMCA/task_iteration/init_status', Bool, callback_start_init_timer)
    rospy.Subscriber('/p3dx/pose_listener/slammed_pose', PoseStamped, callback_slammed_position)
    pub_goal = rospy.Publisher('/p3dx/path_planner/goal', PoseStamped, queue_size=1)  # set the goal for the path planner

    print("time_consumption_calculator spin... \n")
    rospy.spin()

