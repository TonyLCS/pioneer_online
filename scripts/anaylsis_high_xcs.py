#!/usr/bin/env python
"""
This program is to test the response of the Reflex.
It load the XCS agent trained for the reflex process.
The perception also comes from the value attributes in the classifiers popluation.
When each perception is given to the XCS agent, the XCS agent will make a prediction (action).
Both the perception and the the action will show in a plot.
"""
import sys
import rospy
import time
import math

from xcs_api import XCS  # import xcs agent API
from visualization import Visualization  # import Visualization to show results
import random
import csv
import pandas as pd
import numpy as np

from geometry_msgs.msg import Twist
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry


from robot_nav_plan.srv import *

from geometry_msgs.msg import Twist
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray


import matplotlib.pyplot as plt



def get_perception_localFile(path_dataset, random_mode=False, row_index=0):
    data_row = get_a_row_from_dataset(random_mode, row_index, path_dataset)
    perception = [data_row.loc["C1V"], data_row.loc["C2V"]]
    #perception = [data_row.loc["Bumper_1"], data_row.loc["Bumper_2"], data_row.loc["Bumper_3"], data_row.loc["Bumper_4"], data_row.loc["Bumper_5"], data_row.loc["Bumper_6"], data_row.loc["Bumper_7"], data_row.loc["Bumper_8"], data_row.loc["Bumper_9"], data_row.loc["Bumper_10"]]

    groundTrue = data_row.loc["ACT"]
    print("==>", perception)
    print("type of groundTrue", type(groundTrue))
    return perception, groundTrue


def get_a_row_from_dataset(random_mode, row_index, path_dataset):
    #path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/1_PAOC_C.csv"
    data_record = pd.read_csv(path_dataset)
    #print("data_record total line:", data_record.shape[0])

    if random_mode:
        "toselect random one from data_record.shape[0] "
        row_index = np.random.randint(data_record.shape[0])

     #   a = np.random.choice(np.arange(0, maze_iter.getActionSize()), p=probobility_of_actions_normal)
        # print("3. likely action(", a, ") with pai2 (", pai2, ")")
     #   a = np.random.randint(maze_iter.getActionSize())  # todo:remove this line

    print("data_record a row :",  row_index, " from length:",data_record.shape[0])
    #print("data_record location_Y:", data_record.loc[row_index, "location_Y"])
    #print("data_record location_X:", data_record.iloc[row_index, 0])

    #perception = pd.DataFrame(columns=['location_X', 'location_Y', 'location_Y', 'class'])
    print("perception :", data_record.loc[row_index])
    return data_record.loc[row_index]



def XCS_agent_initiate(task_path=None, classifiers_population=None):

    # Initialize game replay object
    xcs_model = XCS()
    xcs_model.set_parameters(task_path)
    xcs_model.XCS_initiate_test_fiters_API()
    #xcs_model.XCS_initiate_fiters_API()


    #xcs_model.set_learning_mode()
    xcs_model.set_testing_mode()

    if classifiers_population is not None:
        xcs_model.XCS_loading_classifiers_population_from_csv(classifiers_population) # loading classifiers [P]

    return xcs_model


def add_state_strength_record(perception, actions_anticipated_reward_stack, which_iteration, state_strength_record):
    #state_strength_record = pd.DataFrame(columns=("iter", "state", "process_strength_one", "process_strength_two"))
    "############################################################"
    "convert perception into state index in the core affect space"
    "############################################################"
    if perception[0] > 900:
        state_index = perception[1] + 1
    if perception[0] < 900:
        state_index = -1 * ( perception[1] +1 )

    existing_record = state_strength_record[state_strength_record["iter"] == which_iteration]

    if existing_record.shape[0] > 0:
        existing_record = existing_record[existing_record["state"] == state_index]
    else:
        already_contain = False

    if existing_record.shape[0] > 0:
        already_contain = True
    else:
        already_contain = False

    if not already_contain:
        to_append = pd.DataFrame([[which_iteration, state_index, actions_anticipated_reward_stack[0], actions_anticipated_reward_stack[1]]], columns=("iter", "state", "process_strength_one", "process_strength_two"))
        state_strength_record = state_strength_record.append(to_append)
        print("add state ( ", str(state_index) + " ) into state_strength_record")
    else:
        print("state ( ", str(state_index) + " ) is in state_strength_record")
    return state_strength_record


def draw_processes_strength_through_iterations():

    state_strength_record = pd.DataFrame(columns=("iter", "state", "process_strength_one", "process_strength_two"))

    "#################################"
    "extract state_strength_record from iterations"
    "#################################"
    for which_iteration in range(1, 16):

        "#################################"
        " load classifiers population"
        "#################################"
        relative_path = "/reflex_equal_speedFunction_suspend"+"_h/"
        #classifiers_population = "lastest"  # "instance8"  #
        classifiers_population = "instance"+ str(which_iteration)

        task_path = "/Results/doThisTask2.csv"

        xcs_model = XCS_agent_initiate(task_path, relative_path + classifiers_population)

        print("finish loading XCS agent")

        "#################################"
        " load local testing data"
        "#################################"
        offline_path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result" + relative_path + classifiers_population + ".csv"
        length_data = pd.read_csv(offline_path_dataset).shape[0]
        print("length_data:", length_data)

        for row_index in range(0, length_data):
            "#################################"
            "load state strength record in each iteration"
            "#################################"
            perception, groundTrue = get_perception_localFile(offline_path_dataset, False, row_index)
            print("perception: ", perception)

            action_xcs = xcs_model.XCS_get_prediction_API(perception)
            print("action_xcs: ", action_xcs)

            actions_anticipated_reward_stack = xcs_model.XCS_get_actions_reward_hashmap("learning")
            print("actions_anticipated_reward_stack:", actions_anticipated_reward_stack)

            state_strength_record = add_state_strength_record(perception, actions_anticipated_reward_stack, which_iteration, state_strength_record)

    print("state_strength_record:")
    print(state_strength_record)

    "#################################"
    "plot state_strength_record"
    "#################################"
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(state_strength_record["iter"], state_strength_record["state"], state_strength_record["process_strength_one"], marker='o')

    plt.draw()
    plt.show()

def draw_an_iteration(which_iteration):
    "#################################"
    " load classifiers population"
    "#################################"
    relative_path = "/reflex_equal_speedFunction_corridor"+"_h/"
    #classifiers_population = "lastest"  # "instance8"  #
    classifiers_population = "instance"+ str(which_iteration)

    task_path = "/Results/doThisTask2.csv"

    xcs_model = XCS_agent_initiate(task_path, relative_path + classifiers_population)

    print("finish loading XCS agent")

    "#################################"
    " load local testing data"
    "#################################"
    offline_path_dataset = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result" + relative_path + classifiers_population + ".csv"
    length_data = pd.read_csv(offline_path_dataset).shape[0]
    print("length_data:", length_data)

    "#################################"
    " plot"
    "#################################"

    # Create new Figure and an Axes which fills it.
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111, projection='3d')
    # ax = fig.add_axes([0, 0, 1, 1], frameon=False)
    ax.set_xlim(-1100, 1100)  # , ax.set_xticks([])
    ax.set_xlabel('bumper value')
    ax.set_ylim(-1, 10)  # , ax.set_yticks([])
    ax.set_ylabel('speed value')
    ax.set_zlim(-1000, 2000)  # , ax.set_yticks([])
    ax.set_zlabel('strength')
    # ax.zaxis.set_rotate_label(False)
    # ax.set_zlabel('strength', rotation=-30)
    plt.title("Core Affect Space ( iteration= "+str(which_iteration)+" )", y=-0.15)

    for row_index in range(0, length_data):

        perception, groundTrue = get_perception_localFile(offline_path_dataset, False, row_index)
        print("perception: ", perception)

        action_xcs = xcs_model.XCS_get_prediction_API(perception)
        print("action_xcs: ", action_xcs)

        actions_anticipated_reward_stack = xcs_model.XCS_get_actions_reward_hashmap("learning")
        print("actions_anticipated_reward_stack:", actions_anticipated_reward_stack)

        """
        "#########################"
        " set up transparency"
        "#########################"
        transparency = 0

        if action_xcs == 1:
            #print("diff = ", actions_anticipated_reward_stack[0] - actions_anticipated_reward_stack[1])
            if actions_anticipated_reward_stack[0] > 200: # set a showing threshold = 200
                if actions_anticipated_reward_stack[0] - actions_anticipated_reward_stack[1] >= 200:
                    transparency = 1
                else:
                    transparency = (actions_anticipated_reward_stack[0] - actions_anticipated_reward_stack[1]) / 200

        if action_xcs == 2:
            if actions_anticipated_reward_stack[1] > 200:
                #print("diff = ", actions_anticipated_reward_stack[1] - actions_anticipated_reward_stack[0])
                if actions_anticipated_reward_stack[1] - actions_anticipated_reward_stack[0] >= 200:
                    transparency = 1
                else:
                    transparency = (actions_anticipated_reward_stack[1] - actions_anticipated_reward_stack[0]) / 200


        if action_xcs == 1:
            #print("diff = ", actions_anticipated_reward_stack[0] - actions_anticipated_reward_stack[1])
            if actions_anticipated_reward_stack[0] <= -200: # set a showing threshold = 200
                if actions_anticipated_reward_stack[0] - actions_anticipated_reward_stack[1] >= 200:
                    transparency = 1
                else:
                    transparency = (actions_anticipated_reward_stack[0] - actions_anticipated_reward_stack[1]) / 200
        if action_xcs == 2:
            # print("diff = ", actions_anticipated_reward_stack[0] - actions_anticipated_reward_stack[1])
            if actions_anticipated_reward_stack[1] <= -200:  # set a showing threshold = 200
                if actions_anticipated_reward_stack[1] - actions_anticipated_reward_stack[0] >= 200:
                    transparency = 1
                else:
                    transparency = (actions_anticipated_reward_stack[1] - actions_anticipated_reward_stack[0]) / 200


        transparency = np.clip(transparency, 0, 1)

        transparency = 1
        if action_xcs == 1:
            emotion_color = (229 / 255, 134 / 255, 18 / 255, transparency)  # orange, for reflex process
        elif action_xcs == 2:
            emotion_color = (25 / 255, 14 / 255, 214 / 255, transparency)  # blue, for move base process

        """
        if action_xcs == 1:
            strength = actions_anticipated_reward_stack[0]
            transparency = abs(actions_anticipated_reward_stack[0]) / 2000
            if abs(strength) <= 370:
                transparency = 0
            emotion_color = (229 / 255, 134 / 255, 18 / 255, transparency)  # orange, for reflex process
            #ax.scatter(perception[0], perception[1], strength, color=emotion_color, marker='o')
            ax.bar3d(perception[0]-12, perception[1]-0.1, -1000, 24, 0.1, (strength+1000), zsort='average', color=emotion_color)
            if transparency >0:
                ax.text(perception[0], perception[1], (strength+100), '%d' % int(strength), ha='center', va='center', fontsize=8)

            strength = actions_anticipated_reward_stack[1]
            transparency = abs(actions_anticipated_reward_stack[1]) / 2000
            if abs(strength) <= 370:
                transparency = 0
            emotion_color = (25 / 255, 14 / 255, 214 / 255, transparency)  # blue, for move base process
            #ax.scatter(perception[0], perception[1], strength, color=emotion_color, marker='o')
            ax.bar3d(perception[0]-12, perception[1] + 0.2, -1000, 24, 0.1, (strength+1000), zsort='average', color=emotion_color)
            if transparency > 0:
                ax.text(perception[0], perception[1] + 0.2, (strength + 100), '%d' % int(strength), ha='center', va='center', fontsize=8)

        if action_xcs == 2:

            strength = actions_anticipated_reward_stack[0]
            transparency = abs(actions_anticipated_reward_stack[0]) / 2000
            if abs(strength) <= 370:
                transparency = 0
            emotion_color = (229 / 255, 134 / 255, 18 / 255, transparency)  # orange, for reflex process
            #ax.scatter(perception[0], perception[1], strength, color=emotion_color, marker='o')
            ax.bar3d(perception[0]-12, perception[1]-0.1, -1000, 24, 0.1, (strength+1000), zsort='average', color=emotion_color)
            if transparency >0:
                ax.text(perception[0], perception[1], (strength+100), '%d' % int(strength), ha='center', va='center', fontsize=8)


            strength = actions_anticipated_reward_stack[1]
            transparency = abs(actions_anticipated_reward_stack[1]) / 2000
            if abs(strength) <= 370:
                transparency = 0
            emotion_color = (25 / 255, 14 / 255, 214 / 255, transparency)  # blue, for move base process
            #ax.scatter(perception[0], perception[1], strength, color=emotion_color, marker='o')
            ax.bar3d(perception[0]-12, perception[1] + 0.2, -1000, 24, 0.1, (strength+1000), zsort='average', color=emotion_color)
            if transparency > 0:
                ax.text(perception[0], perception[1] + 0.2, (strength + 100), '%d' % int(strength), ha='center', va='center', fontsize=8)


        # ax.scatter(perception[0], perception[1], color=emotion_color, marker='o')
    ax.view_init(45, -30)
    plt.pause(3)
    fig.savefig(
        "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result" + relative_path + str(which_iteration) + "twoProcesses.png")
    # ax.view_init(5, -90)
    # ax.view_init(90, -90)
    """

    for angle_draw in range(90, -5, -5):
        ax.view_init(angle_draw, -90)
        plt.draw()
        # acc_png_complete_name = self.result_path + "3_dataSet_distribution_yAxis.png"
        fig.savefig(
            "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result" + relative_path + str(which_iteration) +"ang"+ str(
                angle_draw) + ".png")
        if angle_draw == 0 or angle_draw == 90:
            plt.pause(2.5)
        else:
            plt.pause(0.005)
    """

    # plt.draw()
    # plt.show()
    # plt.pause(5)
    plt.close()

def test_subsumption():
    "#################################"
    " load classifiers population"
    "#################################"
    relative_path = "/reflex_equal_speedFunction_corridor" + "_h/"
    classifiers_population = "lastest"  # "instance8"  #

    task_path = "/Results/doThisTask2.csv"

    xcs_model = XCS_agent_initiate(task_path, relative_path + classifiers_population)

    xcs_model.XCS_subsumption_API()

    xcs_model.XCS_save_classifiers_manually_API("./src/robot_nav_plan/result/subsumption/")

if __name__ == "__main__":
    test_subsumption()

    """
    draw_processes_strength_through_iterations()
    
    for which_iteration in range(22, 24):
        draw_an_iteration(which_iteration)
    """
