#!/usr/bin/env python

import sys
import rospy
import time
import math
import os
import copy
import csv
import pandas as pd

#   from xcs_api import XCS  # import xcs agent API
#   from visualization import Visualization  # import Visualization to show results
import random
import csv
#   import pandas as pd
#   import numpy as np

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64, Int64, Bool

from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist

from sensor_msgs.msg import LaserScan

from nav_msgs.msg import Odometry

from robot_nav_plan.srv import *


from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io, feature
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped

import subprocess

from xcs_api import XCS  # import xcs agent API



def callback_reflex(msg):
    global action_pattern
    global reflex_action
    global do_reflex_in_progress

    do_reflex_in_progress = True
    "interrupted procedural, interrupt the main procedural"
    reflex_action = msg.data
    print("\n\n\n")
    print("###############################")
    print("in layer 4 ")
    print("receive reflex_action= ", reflex_action)
    print("###############################")
    print("\n\n\n")

    reflex_action_pub.publish(reflex_action)
    action_pattern = 2  # reflex action
    action_pattern_pub.publish(action_pattern)
    rospy.sleep(10)

    do_reflex_in_progress = False



if __name__ == "__main__":
    "output: '/p3dx/ACMCA/action_modifier/action'"
    rospy.init_node("acmca_l4_1_action_tendency", anonymous=False)
    reflex_action = -1
    rospy.Subscriber('/p3dx/ACMCA/reflex/action', Int64, callback_reflex) # subscrib path planning

    action_pattern_pub = rospy.Publisher('/p3dx/ACMCA/action_modifier/action', Int64, queue_size=5) #in pioneer_take_action
    reflex_action_pub = rospy.Publisher('/p3dx/ACMCA/L4_reflex/action', Int64, queue_size=5) #reflex action

    do_reflex_in_progress = False

    while not rospy.is_shutdown():
        if not do_reflex_in_progress:
            "main procedural"
            action_pattern = 6 #random action
            action_pattern = 1
            action_pattern_pub.publish(action_pattern)
            print("do path following action")

            rospy.sleep(10)
        else:
            pass
            #rospy.sleep(10)



