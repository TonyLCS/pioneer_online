#!/usr/bin/env python

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

setup_args = generate_distutils_setup(
    packages=['robot_nav_plan'],
    package_dir={'': 'src','':'/home/tony/.local/lib/python2.7/site-packages'},
    install_requires=['skimage']

)

setup(**setup_args)
