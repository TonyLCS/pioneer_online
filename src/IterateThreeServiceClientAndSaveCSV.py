#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *
from geometry_msgs.msg import PoseStamped
import random
import csv

def targeted_position_client(header_token_seq, seq, targeted_position):
    rospy.wait_for_service('targeted_position_service')
    try:
        print "client call targeted_position_service"
        request_the_server = rospy.ServiceProxy('targeted_position_service', TargetedPositionMSG)
        resp1 = request_the_server(header_token_seq, seq, targeted_position)  # calls the server with a request
        return resp1.current_position   # get a response from the server
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def planner_path_length_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('planner_path_length_service')
    try:
        print "client call planner_path_length_service"
        ask_to_publich_modifier = rospy.ServiceProxy('planner_path_length_service', PublishModifierMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def publich_modifier_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('publish_modifier')
    try:
        ask_to_publich_modifier = rospy.ServiceProxy('publish_modifier', PublishModifierMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


if __name__ == "__main__":

    """" write csv file header """
    csvfilename_1 = "./src/robot_nav_plan/result/x_y_mod_data_area4_0603_1.csv"
    csvfilename_2 = "./src/robot_nav_plan/result/x_y_mod_data_area4_0603_2.csv"

    header_line_1 = ["id", "location_X", "location_Y", "radius_inflation", "path_length", "class"]
    header_line_2 = ["location_X", "location_Y", "radius_inflation", "class"]

    with open(csvfilename_1, 'wb') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(header_line_1)

    with open(csvfilename_2, 'wb') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(header_line_2)


    for seq in range(1, 151, 1):
        print ""
        """##################### 1 ##################################"""
        print "1. PubModClient active to call PubModService"
        token = 1
        modifier_request_value = random.uniform(0, 8) / 10  # random method between 0-0.8
        #modifier_request_value = float(seq) / 150.0 * 0.8  # sequential method, between 0-0.8
        print modifier_request_value
        print "1. token %s/ seq %s/ mod-req %s"\
              % (token, seq, modifier_request_value)

        modifier_response_value = publich_modifier_client(token, seq, modifier_request_value)
        print "1. token %s/ seq %s/ mod-req %s/ mod-rsp %s" \
              % (token, seq, modifier_request_value, str(modifier_response_value))
        print ""

        """##################### 2  ##################################"""
        print "2. PubModClient active to call targeted_position_service"
        token = 2

        targeted_generated_x = -9 - random.uniform(0, 3)  # random method between (-9, -6)
        targeted_generated_y = -9 - random.uniform(0, 3)  # random method between (-6, -4)
        targeted_position = PoseStamped()
        targeted_position.header.frame_id = 'odom'
        targeted_position.pose.position.x = targeted_generated_x  # 0  # 4
        targeted_position.pose.position.y = targeted_generated_y  # -4  # -7
        targeted_position.pose.position.z = 0
        targeted_position.pose.orientation.x = 0
        targeted_position.pose.orientation.y = 0
        targeted_position.pose.orientation.z = 0
        targeted_position.pose.orientation.w = 1

        current_position = targeted_position_client(token, seq, targeted_position)
        print "2. token %s/ seq %s/ " % (token, seq)
        print "2. target_position ( x = %s, y = %s)" % (
        str(targeted_position.pose.position.x), str(targeted_position.pose.position.y))
        print "2. current_position( x = %s, y = %s)" % (
        str(current_position.pose.position.x), str(current_position.pose.position.y))
        print ""

        """##################### 3  ##################################"""
        print "3. PubModClient active to call planner_path_length_service"
        token = 3
        # modifier_request_value = 1000
        length_return = planner_path_length_client(token, seq, modifier_request_value)
        print "3. token %s/ seq %s/ the length of the path %s" % (token, seq, str(length_return))
        print "###########################################################################"
        print ""

        """##################### 4  ##################################"""
        print "4. save to csv file"

        if length_return >= 2:
            class_result = 1
        else:
            class_result = 2

        print "class_result: %s" % class_result

        row_1 = [seq, targeted_generated_x, targeted_generated_y, modifier_response_value, length_return, class_result]
        row_2 = [(targeted_generated_x+12), (targeted_generated_y+12), modifier_response_value, class_result]

        with open(csvfilename_1, 'a') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(row_1)

        with open(csvfilename_2, 'a') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(row_2)

        print "###########################################################################"
        print ""



