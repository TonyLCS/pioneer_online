#!/usr/bin/env python


from __future__ import print_function

import sys
import os
import rospy

#### keras
import keras
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.models import load_model
import numpy as np

from keras.datasets import mnist
import matplotlib.pyplot as plt

#####ros
import copy
from robot_nav_plan.srv import *
from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import LaserScan


class SecondaryReinforcer_NN(object):
    def __init__(self):

        self.slam_current_position = PoseStamped()
        self.get_current_slam_current_position = False

        self.token = 3
        self.seq = 2
        self.modifier_request_value = 3

        self.fig_index = 0
        self.save_dir = os.path.join(os.getcwd(), 'src/robot_nav_plan/result/saved_Lidar_figure')

        self.model_save_dir = os.path.join(os.getcwd(), 'saved_NN_models')
        self.model_name = 'elevator_model.h5'#'keras_moving_object_trained_model.h5'  # this is the output model that can be applied in futue.

        self.model = self.loading_NN_model(self.model_save_dir, self.model_name)

    def loading_NN_model(self, save_dir, model_name):
        model_path = os.path.join(save_dir, model_name)
        model = load_model(model_path)
        print('Load model from %s ' % model_path)
        return model

    def NN_prediction(self):
        #figure_file_part_2 = "CurrentFigure.png"
        test_index = 0
        #figure_path = os.path.join(self.save_dir, figure_file_part_2)
        #fig_save_dir = os.path.join(os.getcwd(), 'src/robot_nav_plan/result/saved_Ladar_figure')
        fig_save_dir = os.path.join(self.save_dir, "CurrentFigure.png")
        img = io.imread(fig_save_dir)  # skimage package
        my_x_test = np.ndarray(shape=(1, img.shape[0], img.shape[1], img.shape[2]), dtype=np.uint8)
        #my_x_test = np.ndarray(shape=(1, 126, 126, 4), dtype=np.uint8)

        my_x_test[test_index] = img
        currentCostmapFigure = my_x_test

        out = self.model.predict(currentCostmapFigure, batch_size=1)
        # print("x_plot:", x_plot)
        #print("out:", out.shape)
        print("out:", out)
        #print("out:", out[0][0])

        fig, ax = plt.subplots()
        for i in range(0, out.shape[0]):
            for j in range(0, out.shape[1]):
                plt.scatter(i, j, s=(50 * out[i][j]) ** 2, c="b")

        ax.set_xlabel('Instances (0-3)')
        ax.set_ylabel('Classes (1-2)')
        #plt.show()
        plt.pause(3)
        # plt.show()
        plt.close()
        return out

    def save_current_figure(self):
        global has_ladar_ranges

        if has_ladar_ranges:
            has_ladar_ranges =False
            x = np.arange(len(ladar_ranges))
            fig, ax = plt.subplots(figsize=(1.2, 1.2), dpi=100)
            plt.bar(x, ladar_ranges)

            plt.draw()
            plt.pause(2)

            fig_save_dir = os.path.join(os.getcwd(), 'src/robot_nav_plan/result/saved_Lidar_figure')
            if not os.path.isdir(fig_save_dir):
                os.makedirs(fig_save_dir)
                self.save_dir = fig_save_dir

            png_part_name = os.path.join(fig_save_dir, "CurrentFigure.png")

            extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
            fig.savefig(png_part_name, bbox_inches=extent, dpi=100)
            plt.close()


if __name__ == "__main__":
    global ladar_ranges
    global has_ladar_ranges

    secondaryReinforcer_nn_object = SecondaryReinforcer_NN()

    rospy.init_node("LaserScan_Apply_Reinforcer_Node", anonymous=False)

    def callback_laser(msg):
        global ladar_ranges
        global has_ladar_ranges
        #print("laser range max:", msg.range_max, "; range_nim:", msg.range_min)
        #print("size of ranges:", len(msg.ranges))
        ladar_ranges = msg.ranges
        has_ladar_ranges = True

    rospy.Subscriber('/p3dx/laser/scan', LaserScan, callback_laser)

    print("LaserScan_Get_Dataset node init...")
    rate = rospy.Rate(0.5)
    iter_index = 0
    fig_index = 0
    has_ladar_ranges = False
    call_from_client = False


    while has_ladar_ranges:
        rate.sleep()

    print("LaserScan_Apply_Reinforcer_Node is active")


    def activeService(req):
        print("request req:", req.seq)
        # secondaryReinforcer_nn_object.get_costmap()
        global call_from_client
        global prediction
        call_from_client = True
        rate = rospy.Rate(1)
        while call_from_client:
            print("service thread is waiting until get prediction from the main thread")
            rate.sleep()
        # prediction1 = [1.0, 0.85]
        prediction1 = prediction[0]
        print("NN prediction[0]:", prediction1)
        return CostMapNNMSGResponse(prediction1)

    s = rospy.Service('LIDAR_NN_prediction_service', CostMapNNMSG, activeService)
    print("ready for LIDAR_NN_prediction_service")

    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        if call_from_client:
            secondaryReinforcer_nn_object.save_current_figure()
            prediction = secondaryReinforcer_nn_object.NN_prediction()
            print("NN prediction:", prediction)
            call_from_client = False
            print("end of a service, go to sleep~")

        rate.sleep()
