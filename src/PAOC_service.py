#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *
from geometry_msgs.msg import PoseStamped
import random
import csv
from math import cos, sin, pi

def targeted_position_client(header_token_seq, seq, targeted_position):
    rospy.wait_for_service('targeted_position_service')
    try:
        print "client call targeted_position_service"
        request_the_server = rospy.ServiceProxy('targeted_position_service', TargetedPositionMSG)
        resp1 = request_the_server(header_token_seq, seq, targeted_position)  # calls the server with a request
        return resp1.current_position   # get a response from the server
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def planner_path_length_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('planner_path_length_service')
    try:
        print "client call planner_path_length_service"
        ask_to_publich_modifier = rospy.ServiceProxy('planner_path_length_service', PublishModifierMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e



def publich_modifier_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('publish_modifier')
    try:
        ask_to_publich_modifier = rospy.ServiceProxy('publish_modifier', PublishModifierMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def PAOC_service_callback(req):

    window_id = req.window_id
    window_width = req.window_width
    facing_position = req.facing_position
    csv_seq = req.csv_seq
    title_x = str(round(facing_position.pose.position.x, 2))
    title_y = str(round(facing_position.pose.position.y, 2))

    """" write csv file header """
    csvfilename_1 = "./src/robot_nav_plan/result/path_nav_600/" + str(csv_seq) + "_PAOC_A_" + title_x + "_" + title_y+".csv"
    csvfilename_2 = "./src/robot_nav_plan/result/path_nav_600/" + str(csv_seq) + "_PAOC_B_" + title_x + "_" + title_y+".csv"
    csvfilename_3 = "./src/robot_nav_plan/result/path_nav_600/" + str(csv_seq) + "_PAOC_C.csv"

    header_line_1 = ["id", "location_X", "location_Y", "radius_inflation", "path_length", "class"]
    header_line_2 = ["location_X", "location_Y", "radius_inflation", "class"]


    with open(csvfilename_1, 'wb') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(header_line_1)

    with open(csvfilename_2, 'wb') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(header_line_2)

    with open(csvfilename_3, 'wb') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(header_line_2)

    for seq in range(1, 901, 1):
        print ""
        """##################### 1 ##################################"""
        print "1. PubModClient active to call PubModService"
        token = 1
        modifier_request_value = random.uniform(0, 8) / 10  # random method between 0-0.8
        # modifier_request_value = float(seq) / 150.0 * 0.8  # sequential method, between 0-0.8
        print modifier_request_value
        print "1. token %s/ seq %s/ mod-req %s" \
              % (token, seq, modifier_request_value)

        modifier_response_value = publich_modifier_client(token, seq, modifier_request_value)
        print "1. token %s/ seq %s/ mod-req %s/ mod-rsp %s" \
              % (token, seq, modifier_request_value, str(modifier_response_value))
        print ""

        """##################### 2  ##################################"""
        print "2. PubModClient active to call targeted_position_service"
        token = 2
        if window_id == 0 or window_id == 2 or window_id == 4:
            targeted_generated_x = facing_position.pose.position.x + cos(pi*window_id*0.5)*random.uniform(0, window_width)  #
            targeted_generated_y = facing_position.pose.position.y - cos(pi*window_id*0.5)*(0.5*window_width-random.uniform(0, window_width))

        if window_id == 1 or window_id == 3:
            targeted_generated_x = facing_position.pose.position.x - sin(pi * window_id * 0.5) * (0.5 * window_width - random.uniform(0, window_width))
            targeted_generated_y = facing_position.pose.position.y + sin(pi * window_id * 0.5) * random.uniform(0, window_width)

        # targeted_generated_x = -9 - random.uniform(0, 3)  # random method between (-9, -6)
        # targeted_generated_y = -9 - random.uniform(0, 3)  # random method between (-6, -4)
        targeted_position = PoseStamped()
        targeted_position.header.frame_id = 'map'
        targeted_position.pose.position.x = targeted_generated_x  # 0  # 4
        targeted_position.pose.position.y = targeted_generated_y  # -4  # -7
        targeted_position.pose.position.z = 0
        targeted_position.pose.orientation.x = facing_position.pose.orientation.x
        targeted_position.pose.orientation.y = facing_position.pose.orientation.y
        targeted_position.pose.orientation.z = facing_position.pose.orientation.z
        targeted_position.pose.orientation.w = facing_position.pose.orientation.w

        current_position = targeted_position_client(token, seq, targeted_position)
        print "2. token %s/ seq %s/ " % (token, seq)
        print "2. target_position ( x = %s, y = %s)" % (
            str(targeted_position.pose.position.x), str(targeted_position.pose.position.y))
        print "2. current_position( x = %s, y = %s)" % (
            str(current_position.pose.position.x), str(current_position.pose.position.y))
        print ""

        """##################### 3  ##################################"""
        print "3. PubModClient active to call planner_path_length_service"
        token = 3
        # modifier_request_value = 1000
        length_return = planner_path_length_client(token, seq, modifier_request_value)
        print "3. token %s/ seq %s/ the length of the path %s" % (token, seq, str(length_return))
        print "###########################################################################"
        print ""

        """##################### 4  ##################################"""
        print "4. save to csv file"
        class_result_2 = 0

        if length_return >= 2:

            class_result = 1
            class_result_2 = 1
            if length_return >= 88:  # 91 > 89 = 20* sqrt(2*2+4*4)
                class_result_2 = 3
                class_result = 2
        else:
            class_result = 2
            class_result_2 = 2

        print "class_result: %s" % class_result

        row_1 = [seq, targeted_generated_x, targeted_generated_y, modifier_response_value, length_return, class_result]
        # row_2 = [(targeted_generated_x + 12), (targeted_generated_y + 12), modifier_response_value, class_result]
        row_2 = [targeted_generated_x, targeted_generated_y, modifier_request_value, class_result_2]  # change the modifier_response_value into modifier_request_value
        row_3 = [targeted_generated_x, targeted_generated_y, modifier_request_value, class_result]  # change the modifier_response_value into modifier_request_value

        with open(csvfilename_1, 'a') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(row_1)

        with open(csvfilename_2, 'a') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(row_2)

        with open(csvfilename_3, 'a') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(row_3)


        print "###########################################################################"
        print ""
    print "end of one PAOC process"
    return PAOCMSGResponse(facing_position)  # response the request




if __name__ == "__main__":
    rospy.init_node('PAOC_service')
    print "PAOC_service ready for client."
    rospy.Service('PAOC_service', PAOCMSG, PAOC_service_callback)
    rospy.spin()





