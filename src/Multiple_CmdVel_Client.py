#!/usr/bin/env python

from __future__ import division
import sys
import time

import rospy
from robot_nav_plan.srv import *
from geometry_msgs.msg import PoseStamped
from math import sin, cos, pi, atan, floor
import random


def calculate_goal_within_window(goal_x, goal_y):

    theta_id = atan(float(goal_y/goal_x))  # -pi/2, pi/2
    if goal_x < 0:
        theta_id = theta_id + pi
    if theta_id < 0:
        theta_id = theta_id + 2*pi  #translate theta to a scope from (0, 2*pi)
    print "goal theta is "
    print ((theta_id/pi) * 180)

    # add bias
    theta_id = theta_id + pi/16  # for floor
    # calculate goal id
    goal_id = floor(theta_id/((2*pi)/16))
    print "x=%s, y=%s goal theta is "%(goal_x, goal_y)
    print ((theta_id/pi)*180)
    print "goal_id is (%s). "%goal_id
    tempt = atan(0.5)
    print "temp= %s"%tempt


def targeted_position_client(header_token_seq, seq, targeted_position):
    rospy.wait_for_service('multiple_cmdVel_service', timeout=5.0)
    try:
        print "     client call multiple_cmdVel_service"
        request_the_server = rospy.ServiceProxy('multiple_cmdVel_service', TargetedPositionMSG)
        resp1 = request_the_server(header_token_seq, seq, targeted_position)  # calls the server with a request
        print "     Service success response."
        return resp1.current_position   # get a response from the server
    except rospy.ServiceException, e:
        print "     Service call failed: %s"%e



if __name__ == "__main__":

    print "PubModClient active to call multiple_cmdVel_service"
    token = 5
    seq = 2

    x = 1
    y = 0

    theta_targeted = -1 * atan(y/x) / pi * 180
    # print theta_targeted
    theta = theta_targeted/180 * pi
    if x < 0:
        theta = theta + pi

    # theta :(-pi/2 , 3/2*pi)
    # print theta

    calculate_goal_within_window(x, y)

    orientation_z = sin(theta / 2)
    orientation_w = cos(theta / 2)

    targeted_position = PoseStamped()
    targeted_position.header.frame_id = 'odom'
    targeted_position.pose.position.x = -12.5  # 0  # 4
    targeted_position.pose.position.y = -10.5  # -4  # -7
    targeted_position.pose.position.z = 0
    targeted_position.pose.orientation.x = 0
    targeted_position.pose.orientation.y = 0
    targeted_position.pose.orientation.z = -orientation_z  # 0
    targeted_position.pose.orientation.w = orientation_w  # -1

    # targeted_position2 = targeted_position

    """
    theta=(theta/180)*3.14;
    orientation_w= Math.sin(theta/2);# blog.csdn.net/candycat1992/article/details/41254799 // www. //calculate theta
	orientation_z= Math.cos(theta/2);
    """

    print "target_position: token %s/ seq %s/ position.x-req %s/ position.x-rsp %s"\
          %(token, seq, str(targeted_position.pose.position.x), str(targeted_position.pose.position.y))

    current_position = targeted_position_client(token, seq, targeted_position)

    print "current_position: token %s/ seq %s/ position.x-req %s/ position.x-rsp %s"\
          %(token, seq, str(current_position.pose.position.x), str(current_position.pose.position.y))

    rospy.init_node('watchdog')
    while not rospy.is_shutdown():
        print "in watchdog"
        seq = seq + 1

        small_random_distance = random.uniform(0, 1)/5
        targeted_position2 = PoseStamped()
        targeted_position2.header.frame_id = targeted_position.header.frame_id
        targeted_position2.pose.position.x = targeted_position.pose.position.x
        targeted_position2.pose.position.y = targeted_position.pose.position.y
        targeted_position2.pose.position.z = targeted_position.pose.position.z
        targeted_position2.pose.orientation.x = targeted_position.pose.orientation.x
        targeted_position2.pose.orientation.y = targeted_position.pose.orientation.y
        targeted_position2.pose.orientation.z = targeted_position.pose.orientation.z
        targeted_position2.pose.orientation.w = targeted_position.pose.orientation.w

        if random.uniform >= 0.5:
            targeted_position2.pose.position.x = targeted_position.pose.position.x + small_random_distance  # 0  # 4
            targeted_position2.pose.position.y = targeted_position.pose.position.y + small_random_distance  # -4  # -7
        else:
            targeted_position2.pose.position.x = targeted_position.pose.position.x - small_random_distance  # 0  # 4
            targeted_position2.pose.position.y = targeted_position.pose.position.y - small_random_distance  # -4  # -7
        



        print "target_position: token %s/ seq %s/  x= %s/ , y= %s/ ." \
              % (token, seq, str(targeted_position2.pose.position.x), str(targeted_position2.pose.position.y))

        current_position = targeted_position_client(token, seq, targeted_position2)

        print "current_position: token %s/ seq %s/, x= %s/ , y= %s/ ." \
              % (token, seq, str(current_position.pose.position.x), str(current_position.pose.position.y))
        print "client wait for 60s, sleep."
        print "\n"
        rospy.sleep(20)

