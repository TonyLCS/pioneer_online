#!/usr/bin/env python

import sys
import os
import rospy
import copy
from robot_nav_plan.srv import *
from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped


global slam_current_position
global get_current_slam_current_position


def callback_slammed_position(msg):
    global slam_current_position
    global get_current_slam_current_position

    slam_current_position = msg
    get_current_slam_current_position = True
    pass

def convert_map_data(data):
    height_ori = 448
    width_ori = 320
    mapdata = np.zeros([height_ori, width_ori])
    print ("convert into map data")
    for line_index in range(0, height_ori):
        width_index = line_index * width_ori
        mapdata[line_index] = data[width_index: (width_index+width_ori)]
    print(mapdata)
    io.imshow(data)
    io.show()
    print("end of showing a map")


def convert_map_data_3(data, height_ori, width_ori):
    #height_ori = 320
    #width_ori = 480
    mapdata = np.zeros([height_ori, width_ori])
    print ("convert into map data")
    for line_index in range(0, height_ori):
        width_index = line_index * width_ori
        mapdata[height_ori-line_index-1] = data[width_index: (width_index+width_ori)]
    return mapdata

def add_scope_to_map(mapdata_ori, height_ori, width_ori,origin_x, origin_y,position_x, position_y, x_range, y_range):
    #print(mapdata)
    mapdata = copy.deepcopy(mapdata_ori)
    """
    mapdata[height_ori-228-1, 100] = 150 # this is original point of coordinary
    #mapdata[height_ori-228-1-10, 100+10] = 150 # (1,1)
    "draw scope"
    y_range = 20
    x_range = 10

    mapdata[(height_ori-228-1-y_range),(100-x_range):(100+x_range)] = 250
    mapdata[(height_ori-228-1+y_range),(100-x_range):(100+x_range+1)] = 250
    mapdata[(height_ori-228-1-y_range):(height_ori-228-1+y_range),(100-x_range)] = 250
    mapdata[(height_ori-228-1-y_range):(height_ori-228-1+y_range),(100+x_range)] = 250
    """
    mapdata[height_ori-origin_y-1, origin_x] = 250 # this is original point of coordinary
    #mapdata[height_ori-228-1-10, 100+10] = 150 # (1,1)
    "draw scope"

    """
    mapdata[(height_ori-origin_y-1-y_range),(origin_x-x_range):(origin_x+x_range)] = 250
    mapdata[(height_ori-origin_y-1+y_range),(origin_x-x_range):(origin_x+x_range+1)] = 250
    mapdata[(height_ori-origin_y-1-y_range):(height_ori-origin_y-1+y_range),(origin_x-x_range)] = 250
    mapdata[(height_ori-origin_y-1-y_range):(height_ori-origin_y-1+y_range),(origin_x+x_range)] = 250
    
    """

    mapdata[int(height_ori - position_y - 1 - y_range):int(height_ori - position_y - 1 + y_range), int(position_x - x_range)] = 250
    mapdata[int(height_ori - position_y - 1 - y_range):int(height_ori - position_y - 1 + y_range), int(position_x + x_range)] = 250
    mapdata[int(height_ori - position_y - 1 - y_range), int(position_x - x_range):int(position_x + x_range)] = 250
    mapdata[int(height_ori - position_y - 1 + y_range), int(position_x - x_range):int(position_x + x_range)] = 250

    return mapdata
    #io.imshow(mapdata)
    #io.show()

def slice_map(mapdata, height_ori, position_x, position_y, x_range, y_range):

    submap = mapdata[int(height_ori-position_y-1-y_range):int(height_ori-position_y-1+y_range),int(position_x-x_range):int(position_x+x_range)]
    return submap

def costmap_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('costmap_cell_service')
    try:
        print("costmap_cell_service")
        ask_to_publich_modifier = rospy.ServiceProxy('costmap_cell_service', CostMapMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value
    except rospy.ServiceException or e:
        print("Service call failed: %s", e)


def costmap_client_3(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('costmap_cell_service')
    try:
        print("costmap_cell_service")
        ask_to_publich_modifier = rospy.ServiceProxy('costmap_cell_service', CostMapMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value, resp1.height, resp1.width, resp1.origin_x, resp1.origin_y
    except rospy.ServiceException:
        print("Service call failed: %s")


if __name__ == "__main__":

    global get_current_slam_current_position
    global slam_current_position
    rospy.init_node('costmap_around_service')

    print("PubModClient active to call costmap_cell_service")
    token = 3
    seq = 2
    modifier_request_value = 3
    #costmap_data = costmap_client(token, seq, modifier_request_value)
    #print("token %s/ seq %s/ mod-req %s/ mod-rsp %s"%(token, seq, modifier_request_value, str(costmap_data)))

    get_current_slam_current_position = False

    sub = rospy.Subscriber('/p3dx/pose_listener/slammed_pose', PoseStamped, callback_slammed_position)
    rate = rospy.Rate(1)
    while not get_current_slam_current_position:
        rate.sleep()

    fig_index = 0
    save_dir = os.path.join(os.getcwd(), 'src/robot_nav_plan/result/saved_figures')

    rate = rospy.Rate(1)
    for iteration_loop in range(100):
        fig_index = fig_index + 1

        (costmap_data, height, width, origin_x, origin_y) = costmap_client_3(token, seq, modifier_request_value)
        # print "data %s" %str(costmap_data)
        print("get costmap with height %s/ width %s/ " % (str(height), str(width)))

        resolution = 10
        original_x = int(-1*resolution*origin_x)
        original_y = int(-1*resolution*origin_y)
        world_x = int(slam_current_position.pose.position.x * resolution)
        world_y = int(slam_current_position.pose.position.y * resolution)
        print("current position : x=", slam_current_position.pose.position.x, "; y=", slam_current_position.pose.position.y)

        x_matrix = original_x + world_x
        y_matrix = original_y + world_y

        # convert_map_data(costmap_data)
        mapdata_converted = convert_map_data_3(costmap_data, height, width)
        x_range = 20
        y_range = 20
        mapdata = add_scope_to_map(mapdata_converted, height, width, int(-10*origin_x), int(-10*origin_y), x_matrix, y_matrix, x_range, y_range)
        #position_x = 10
        #position_y = 22.8
        #submapdata = slice_map(mapdata, height, position_x, position_y)
        submapdata = slice_map(mapdata_converted, height, x_matrix, y_matrix, x_range, y_range)


        print("plt plot")
        #plt.imshow(mapdata)
        #plt.draw()
        #plt.pause(1)

        fig = plt.figure(figsize=(3.4, 1.7))
        ax1 = fig.add_subplot(1,2,1)
        ax2 = fig.add_subplot(1,2,2)
        ax1.imshow(mapdata)
        ax2.imshow(submapdata)

        plt.draw()

        fig_name = str(fig_index)
        # Save model and weights
        if not os.path.isdir(save_dir):
            os.makedirs(save_dir)

        png_complete_name = os.path.join(save_dir, "full"+str(60+fig_index)+".png")
        fig.savefig(png_complete_name)

        extent = ax2.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        png_part_name = os.path.join(save_dir, str(60+fig_index)+".png")

        fig.savefig(png_part_name, bbox_inches=extent)

        plt.pause(2)
        #plt.show()
        plt.close()

        rate.sleep()

