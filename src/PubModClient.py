#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *



def publich_modifier_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('publish_modifier')
    try:
        ask_to_publich_modifier = rospy.ServiceProxy('publish_modifier', PublishModifierMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


if __name__ == "__main__":

    print "PubModClient active to call PubModService"
    token = 1
    seq = 0
    modifier_request_value = 0.3
    modifier_response_value = publich_modifier_client(token, seq, modifier_request_value)
    print "token %s/ seq %s/ mod-req %s/ mod-rsp %s"%(token, seq, modifier_request_value, str(modifier_response_value))

