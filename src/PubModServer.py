#!/usr/bin/env python

from robot_nav_plan.srv import *
import rospy
import dynamic_reconfigure.client




def callback_modifier_dynamic_reconfigure(config):
    # do nothing
    pass
    """
     rospy.loginfo("THE SERVICE HAS UPDATE THE DYNAMIC PARAM: inflation_radius= {inflation_radius},"
                  " cost_scaling_factor= {cost_scaling_factor}, robot_radius= {robot_radius},"
                  " enabled={enabled} .".format(**config))

    """


def modifier_dynamic_reconfigure(modifier_value):

    rospy.loginfo(""" dynamic_modifier :: client main start""")
    print "modifier value:"
    print modifier_value

    try:
        rospy.loginfo(""" client: wait_for_service""")
        rospy.wait_for_service("/p3dx/laser_mapper/costmap/inflation_layer", 1.0)
    except rospy.exceptions.ROSException:
        rospy.loginfo(""" service fail""")

    client = dynamic_reconfigure.client.Client("/p3dx/laser_mapper/costmap/inflation_layer", timeout=30,
                                               config_callback=callback_modifier_dynamic_reconfigure)

    # /p3dx/move_base/global_costmap/inflation_layer
    r = rospy.Rate(1)  # 1/0.125= 8s period
    x = modifier_value
    cost_scaling_factor = 10
    robot_radius = 0.46
    enabled = True
    public_count = 0

    while public_count < 5:
        print "publish loop: publish for 5 times"

        public_count = public_count + 1
        print "1. set inflation_radius :"
        print x
        client.update_configuration({"inflation_radius": x, "cost_scaling_factor": cost_scaling_factor,
                                     "robot_radius": robot_radius,
                                     "enabled": enabled})
        rospy.sleep(2)
        print "2. inflation_radius_return: "
        return_config = client.get_configuration()
        inflation_radius_return = return_config.inflation_radius
        print inflation_radius_return
        r.sleep()

    return inflation_radius_return






def publish_modifier_service_callback(req):
    # level 3
    #print "service receives request of "%(req.header_token_seq, req.seq, str(req.modifier_request_value) )
    print "publish_modifier_service:: service receives request: "
    print req.header_token_seq, req.seq, req.modifier_request_value
    modifier_return_value = modifier_dynamic_reconfigure(req.modifier_request_value)
    return PublishModifierMSGResponse(modifier_return_value)


def spin_publish_modifier_service():
    # level 2
    rospy.init_node('publish_modifier_service')
    s = rospy.Service('publish_modifier', PublishModifierMSG, publish_modifier_service_callback)
    print "publish_modifier_service:: Ready to publish modifier."
    rospy.spin()

if __name__ == "__main__":
    # level 1
    spin_publish_modifier_service()