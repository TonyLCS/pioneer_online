#!/usr/bin/env python

import sys
import time

import rospy
from robot_nav_plan.srv import *
from nav_msgs.srv import *
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path
from geometry_msgs.msg import Twist
from actionlib_msgs.msg import GoalStatusArray
from move_base_msgs.msg import MoveBaseActionResult


global new_movement_result
global token_active_flag

global achieve_targeted_position  #flag for go to a targeted position
global slam_current_position

global targeted_position
global goalStatuesArray_has_active


"""
def watchdog_procedural():
    global targeted_position
    rospy.init_node('watchdog')
    while not rospy.is_shutdown():
        execute_goto_targeted_position_procedural(targeted_position)
        rospy.sleep(5)

"""



def execute_stop_procedural():
    # level 4
    pub = rospy.Publisher('/p3dx/cmd_vel', Twist)
    cmd1 = Twist()
    cmd1.linear.x = 0
    cmd1.linear.y = 0
    cmd1.linear.z = 0

    cmd1.angular.x = 0
    cmd1.angular.y = 0
    cmd1.angular.z = 0

    pub.publish(cmd1)


def callback_move_base_result_check(move_base_result):
    global goalStatuesArray_has_active  # input
    global achieve_targeted_position  # output: flag for go to a targeted position

    move_base_result_status = move_base_result.status.status
    if goalStatuesArray_has_active:
        if move_base_result_status == 3:
            achieve_targeted_position = True


def callback_state_check(goalStatuesArray):
    # level 5
    global goalStatuesArray_has_active  # output result

    # print "GoalStatuesArray.header: "
    # print GoalStatuesArray.header
    statues_length = len(goalStatuesArray.status_list)
    # print "GoalStatuesArray.status_list: "
    # print statues_length
    # print " GoalStatuesArray.status_list[statues_length-1].status: "
    # print GoalStatuesArray.status_list[statues_length-1].status
    # print GoalStatuesArray.status_list[statues_length-1].text
    if statues_length >= 1:
        if goalStatuesArray.status_list[statues_length - 1].status == 1 \
                or goalStatuesArray.status_list[statues_length - 1].status == 2:
            goalStatuesArray_has_active = True


    """
    replaced by /p3dx/move_base/result
        if statues_length == 1 and GoalStatuesArray.status_list[statues_length-1].status == 3:
        achieve_targeted_position = True
        # print " achieve the target!!!  achieve_targeted_position: "
        # print achieve_targeted_position
    """

    pass


def callback_path_planner(cmd_msg):
    # level 5
    pub = rospy.Publisher('/p3dx/cmd_vel', Twist)
    pub.publish(cmd_msg)



def callback_slammed_position(msg):
    # level 5
    global slam_current_position
    global has_currentposition
    slam_current_position = msg
    has_currentposition = True
    #print "get slam_current_position "


def execute_goto_targeted_position_procedural(targeted_position):
    # level 4
    global achieve_targeted_position  # flag for go to a targeted position
    global goalStatuesArray_has_active

    achieve_targeted_position = False
    goalStatuesArray_has_active = False
    # publish targeted position
    pub = rospy.Publisher('/p3dx/path_planner/goal', PoseStamped)  # set the goal for the path planner
    pub.publish(targeted_position)

    """
    "publish targeted_position"
    pub_count = 0
    r = rospy.Rate(0.2)
    while True:
        print "     publish targeted_position"
        pub.publish(targeted_position)
        pub_count = pub_count + 1
        if pub_count >= 5:
            break
        r.sleep()

    print "finish publish targeted position"    
    """

    # sub_nav_cmd = rospy.Subscriber('/p3dx/Multiplexer_PathPlanner/cmd_vel', Twist, callback_path_planner)
    # print "finish cmd"

    # sub_pose = rospy.Subscriber('/p3dx/pose_listener/slammed_pose', PoseStamped, callback_slammed_position)
    # print "finish subscribe current position"


    rospy.sleep(1)  # allow service to receive the goal
    # pub.publish(targeted_position)
    # rospy.sleep(1)  # allow service to receive the goal

    # sleep until it has achieve the target position
    rate = rospy.Rate(0.05)  # chech every 10 sce( Rate(0.1))
    count = 1
    while not achieve_targeted_position:
        print "not achieve targeted position yet, in loop"
        pub_count = 0
        r = rospy.Rate(0.5)
        while True:
            print "     publish targeted_position"
            pub.publish(targeted_position)
            pub_count = pub_count + 1
            if pub_count >= 5:
                break
            r.sleep()
        # sub_nav_cmd = rospy.Subscriber('/p3dx/Multiplexer_PathPlanner/cmd_vel', Twist, callback_path_planner)
        # subscribe to move_base state to check whether the robot achieve its target
        #rospy.sleep(0.5)
        rospy.Subscriber('/p3dx/move_base/status', GoalStatusArray, callback_state_check) # make sure it is a new goal
        rospy.Subscriber('/p3dx/move_base/result', MoveBaseActionResult, callback_move_base_result_check) # make sure robot achieve the new goal
        rospy.Subscriber('/p3dx/pose_listener/slammed_pose', PoseStamped, callback_slammed_position)  #reponse current position

        #print " achieve the target!!!  achieve_targeted_position: "
        #print achieve_targeted_position

        count = count + 1
        print "count(%s)" %count
        if count > 6:
            print "service timeout!!!"
            return
        rate.sleep()
    

def execute_spinning_cmd_procedural(targeted_position):
    # level 4
    global new_movement_result
    global token_active_flag
    global has_finish_spinning

    if token_active_flag:

        # publish a cmd
        pub = rospy.Publisher('/p3dx/cmd_vel', Twist)
        cmd1 = Twist()
        cmd1.linear.x = 0
        cmd1.linear.y = 0
        cmd1.linear.z = 0

        cmd1.angular.x = 0
        cmd1.angular.y = 0
        cmd1.angular.z = 1.5

        pub.publish(cmd1)
        has_finish_spinning = False

        new_movement_result = True
        rate = rospy.Rate(1)
        time_inhibition = rospy.Time.from_sec(time.time())
        while not has_finish_spinning:
            pub.publish(cmd1)

            delay = rospy.Time.from_sec(time.time()) - time_inhibition
            if delay > rospy.Duration(10):
                has_finish_spinning = True
            rate.sleep()

        # stop
        cmd1.linear.x = 0
        cmd1.linear.y = 0
        cmd1.linear.z = 0

        cmd1.angular.x = 0
        cmd1.angular.y = 0
        cmd1.angular.z = 0
        pub.publish(cmd1)


def multiple_cmdVel_service_callback(req):
    # level 3
    global new_movement_result
    global token_active_flag
    global targeted_position
    global slam_current_position
    global has_currentposition
    global achieve_targeted_position

    has_currentposition = False
    new_movement_result = False
    token_active_flag = True  # set flag to start the exhibition of result

    print "multiple_cmdVel_service:: service receives request: "  # request from the client node comes from here
    print req.header_token_seq, req.seq, req.targeted_position

    """######## 1 #########"""
    "multiple action1: spinning"
    """
    execute_spinning_cmd_procedural(req.targeted_position)  # go to procedural to process the request

    rate = rospy.Rate(1)            # wait for until the procedural is done
    while not new_movement_result:
        rate.sleep()
    print "new_movement_result"
    print new_movement_result
    token_active_flag = False  # set flag to stop the exhibition of result
        
    
    
    """

    """######## 2 #########"""
    "multiple action2: goto target location"
    execute_goto_targeted_position_procedural(req.targeted_position)
    targeted_position = req.targeted_position

    if achieve_targeted_position:
        print " achieve the target!!!"
    else:
        print "timeout and quit!!!"

    """######## 3 #########"""
    "multiple action 3: stop"
    # execute_stop_procedural()

    """######## 4 #########"""
    "watchdog = 8 sec"
    rate = rospy.Rate(0.125)
    while not has_currentposition:
        print "does not have current position, sleep"
        rate.sleep()
    print "multiple_cmdVel_service:: finish a service"
    return TargetedPositionMSGResponse(slam_current_position)  # response the request


def spin_multiple_cmdVel_service():
    # level 2
    global targested_position
    rospy.init_node('multiple_cmdVel_service')
    s = rospy.Service('multiple_cmdVel_service', TargetedPositionMSG, multiple_cmdVel_service_callback)
    print "multiple_cmdVel_service:: Service spin. Ready to response to the client."
    rospy.spin()


if __name__ == "__main__":

    spin_multiple_cmdVel_service()
    print "multiple_cmdVel_service  finish"

