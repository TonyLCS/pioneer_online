#!/usr/bin/env python

from __future__ import division
import sys
import time

import rospy
from robot_nav_plan.srv import *
from geometry_msgs.msg import PoseStamped
from math import sin, cos, pi, atan, floor
import random


global has_current_position
global slam_current_position
global scenario_targeted_position
global goal_id
global window_id

def calculate_window_points(window_width):
    global slam_current_position
    global goal_id
    global window_id
    facing_angel = window_id*90
    """
    
        if goal_id == 0:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + window_width,
                                                     slam_current_position.pose.position.y,
                                                     0)
    if goal_id == 1:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + window_width,
                                                     slam_current_position.pose.position.y + 0.5*window_width,
                                                     45)
    if goal_id == 2:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + 0.5 * window_width,
                                                     slam_current_position.pose.position.y + 0.5 * window_width,
                                                     45)
    if goal_id == 3:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + 0.5 * window_width,
                                                     slam_current_position.pose.position.y + window_width,
                                                     45)
    if goal_id == 4:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x,
                                                     slam_current_position.pose.position.y + window_width,
                                                     90)
    if goal_id == 5:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - 0.5 * window_width,
                                                     slam_current_position.pose.position.y + window_width,
                                                     135)
    if goal_id == 6:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - 0.5 * window_width,
                                                     slam_current_position.pose.position.y + 0.5 * window_width,
                                                     135)
    if goal_id == 7:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - window_width,
                                                     slam_current_position.pose.position.y + 0.5 * window_width,
                                                     135)
    if goal_id == 8:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - window_width,
                                                     slam_current_position.pose.position.y,
                                                     180)
    if goal_id == 9:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - window_width,
                                                     slam_current_position.pose.position.y - 0.5 * window_width,
                                                     225)
    if goal_id == 10:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - 0.5 * window_width,
                                                     slam_current_position.pose.position.y - 0.5 * window_width,
                                                     225)
    if goal_id == 11:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - 0.5 * window_width,
                                                     slam_current_position.pose.position.y - window_width,
                                                     225)
    if goal_id == 12:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x,
                                                     slam_current_position.pose.position.y - window_width,
                                                     270)
    if goal_id == 13:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + 0.5*window_width,
                                                     slam_current_position.pose.position.y - window_width,
                                                     315)
    if goal_id == 14:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + 0.5*window_width,
                                                     slam_current_position.pose.position.y - 0.5*window_width,
                                                     315)
    if goal_id == 15:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + window_width,
                                                     slam_current_position.pose.position.y - 0.5*window_width,
                                                     315)
    
    """
    if goal_id == 0:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + window_width,
                                                     slam_current_position.pose.position.y,
                                                     facing_angel)
    if goal_id == 1:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + window_width,
                                                     slam_current_position.pose.position.y + 0.5*window_width,
                                                     facing_angel)
    if goal_id == 2:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + 0.5 * window_width,
                                                     slam_current_position.pose.position.y + 0.5 * window_width,
                                                     facing_angel)
    if goal_id == 3:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + 0.5 * window_width,
                                                     slam_current_position.pose.position.y + window_width,
                                                     facing_angel)
    if goal_id == 4:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x,
                                                     slam_current_position.pose.position.y + window_width,
                                                     facing_angel)
    if goal_id == 5:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - 0.5 * window_width,
                                                     slam_current_position.pose.position.y + window_width,
                                                     facing_angel)
    if goal_id == 6:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - 0.5 * window_width,
                                                     slam_current_position.pose.position.y + 0.5 * window_width,
                                                     facing_angel)
    if goal_id == 7:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - window_width,
                                                     slam_current_position.pose.position.y + 0.5 * window_width,
                                                     facing_angel)
    if goal_id == 8:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - window_width,
                                                     slam_current_position.pose.position.y,
                                                     facing_angel)
    if goal_id == 9:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - window_width,
                                                     slam_current_position.pose.position.y - 0.5 * window_width,
                                                     facing_angel)
    if goal_id == 10:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - 0.5 * window_width,
                                                     slam_current_position.pose.position.y - 0.5 * window_width,
                                                     facing_angel)
    if goal_id == 11:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x - 0.5 * window_width,
                                                     slam_current_position.pose.position.y - window_width,
                                                     facing_angel)
    if goal_id == 12:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x,
                                                     slam_current_position.pose.position.y - window_width,
                                                     facing_angel)
    if goal_id == 13:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + 0.5*window_width,
                                                     slam_current_position.pose.position.y - window_width,
                                                     facing_angel)
    if goal_id == 14:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + 0.5*window_width,
                                                     slam_current_position.pose.position.y - 0.5*window_width,
                                                     facing_angel)
    if goal_id == 15:
        single_step_targeted_goal = combine_position(slam_current_position.pose.position.x + window_width,
                                                     slam_current_position.pose.position.y - 0.5*window_width,
                                                     facing_angel)
    return single_step_targeted_goal


def calculate_goal_within_window(goal_x, goal_y):
    global goal_id
    global window_id

    theta_id = atan(float(goal_y/goal_x))  # -pi/2, pi/2
    if goal_x < 0:
        theta_id = theta_id + pi
    if theta_id < 0:
        theta_id = theta_id + 2*pi  #translate theta to a scope from (0, 2*pi)
    print "goal theta is "
    print ((theta_id/pi) * 180)

    # add bias
    theta_id_bias1 = theta_id + pi/16  # for floor
    # calculate goal id
    goal_id = floor(theta_id_bias1/((2*pi)/16))
    print "x=%s, y=%s goal theta is "%(goal_x, goal_y)
    print ((theta_id_bias1/pi)*180)
    print "goal_id is (%s). "%goal_id

    window_id = floor((goal_id+2)/4)
    if window_id > 4:
        window_id = 0
    print "window_id = %s"%window_id



def callback_slammed_position(msg):
    global has_current_position
    global slam_current_position
    # print "get slam_current_position. "
    slam_current_position = msg
    has_current_position = True


def generate_target_for_single_step():
    """current position"""
    global has_current_position
    global slam_current_position
    global scenario_targeted_position

    has_current_position = False
    rospy.Subscriber('/p3dx/pose_listener/slammed_pose', PoseStamped, callback_slammed_position)  # get current position
    rate = rospy.Rate(10)  # 10hz
    while not has_current_position:
        print "wait for subscription of slammed_pose... check every 0.1 second"
        rate.sleep()

    distance_x = scenario_targeted_position.pose.position.x - slam_current_position.pose.position.x
    distance_y = scenario_targeted_position.pose.position.y - slam_current_position.pose.position.y

    calculate_goal_within_window(distance_x, distance_y)  # get goal_id for the scenario window
    window_width = 1
    return calculate_window_points(window_width)  #get 16 window points # return goal_id_point


def combine_position(x, y, angle):
    a_targeted_postition = PoseStamped()
    a_targeted_postition.header.frame_id = 'odom'
    a_targeted_postition.pose.position.x = x
    a_targeted_postition.pose.position.y = y
    a_targeted_postition.pose.position.z = 0

    angle = angle / 180 * pi
    if x < 0:
        angle = angle + pi

    a_orientation_z = sin(angle / 2)
    a_orientation_w = cos(angle / 2)

    a_targeted_postition.pose.orientation.x = 0
    a_targeted_postition.pose.orientation.y = 0
    a_targeted_postition.pose.orientation.z = -a_orientation_z  # 0
    a_targeted_postition.pose.orientation.w = a_orientation_w  # -1

    return a_targeted_postition


def single_step_targeted_position_client(header_token_seq, seq, targeted_position):
    rospy.wait_for_service('multiple_cmdVel_service', timeout=5.0)
    try:
        print "     client call multiple_cmdVel_service"
        request_the_server = rospy.ServiceProxy('multiple_cmdVel_service', TargetedPositionMSG)
        resp1 = request_the_server(header_token_seq, seq, targeted_position)  # calls the server with a request
        print "     Service success response."
        return resp1.current_position   # get a response from the server
    except rospy.ServiceException, e:
        print "     Service call failed: %s"%e


if __name__ == "__main__":

    rospy.init_node('multistep_client')

    global scenario_targeted_position

    print "Multistep client initiate"


    "1. set scenario targeted location and angle"

    "2. generate window target location and angle for single step scenario"

    single_step_test_position = generate_target_for_single_step()  # subscribe current position and it will store in the slam_current position

    print "test position x=%s, y=%s." % (str(single_step_test_position.pose.position.x), str(single_step_test_position.pose.position.y))

    token = 9
    seq = 9

    current_position = single_step_targeted_position_client(token, seq, single_step_test_position)

    print "current_position: token %s/ seq %s/ position.x-req %s/ position.x-rsp %s" \
          % (token, seq, str(current_position.pose.position.x), str(current_position.pose.position.y))

    while not rospy.is_shutdown():
        print "in watchdog"
        seq = seq + 1

        print "test position x=%s, y=%s." % (
            str(single_step_test_position.pose.position.x), str(single_step_test_position.pose.position.y))

        current_position = single_step_targeted_position_client(token, seq, single_step_test_position)

        print "current_position: token %s/ seq %s/, x= %s/ , y= %s/ ." \
              % (token, seq, str(current_position.pose.position.x), str(current_position.pose.position.y))
        print "client wait for 60s, sleep."
        print "\n"
        rospy.sleep(20)





