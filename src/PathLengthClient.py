#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *


def planner_path_length_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('planner_path_length_service')
    try:
        print "client call planner_path_length_service"
        ask_to_publich_modifier = rospy.ServiceProxy('planner_path_length_service', PublishModifierMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


if __name__ == "__main__":

    print "PubModClient active to call planner_path_length_service"
    token = 3
    seq = 2
    modifier_request_value = 3
    length_return = planner_path_length_client(token, seq, modifier_request_value)
    print "token %s/ seq %s/ mod-req %s/ mod-rsp %s"%(token, seq, modifier_request_value, str(length_return))

