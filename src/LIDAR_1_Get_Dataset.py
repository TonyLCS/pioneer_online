#!/usr/bin/env python

import sys
import os
import rospy

from sensor_msgs.msg import LaserScan

import numpy as np
import matplotlib.pyplot as plt


def callback_laser(msg):
    global ladar_ranges
    global has_ladar_ranges

    print("laser range max:", msg.range_max, "; range_nim:", msg.range_min)
    print("size of ranges:", len(msg.ranges))
    ladar_ranges = msg.ranges
    has_ladar_ranges =True




if __name__ == "__main__":
    global ladar_ranges
    global has_ladar_ranges

    rospy.init_node("LaserScan_Get_Dataset", anonymous=False)
    rospy.Subscriber('/p3dx/laser/scan', LaserScan, callback_laser)
    print("LaserScan_Get_Dataset node init...")
    rate = rospy.Rate(0.5)
    iter_index = 0
    fig_index = 0
    has_ladar_ranges = False

    while not rospy.is_shutdown():

        if has_ladar_ranges:
            has_ladar_ranges =False
            x = np.arange(len(ladar_ranges))
            fig, ax = plt.subplots(figsize=(1.2, 1.2), dpi=100)
            plt.bar(x, ladar_ranges)

            plt.draw()
            plt.pause(2)

            fig_save_dir = os.path.join(os.getcwd(), 'src/robot_nav_plan/result/saved_Lidar_figure')
            if not os.path.isdir(fig_save_dir):
                os.makedirs(fig_save_dir)
            png_part_name = os.path.join(fig_save_dir, str(fig_index) + ".png")

            extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
            fig.savefig(png_part_name, bbox_inches=extent, dpi=100)
            plt.close()
            fig_index = fig_index + 1


        rate.sleep()