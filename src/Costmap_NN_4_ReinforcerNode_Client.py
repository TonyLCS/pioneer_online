#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *
from nav_msgs.srv import *
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path


def costmap_nn_reinforcerNode_client(seq):
    rospy.wait_for_service('costmap_NN_prediction_service')
    try:
        getCostmapNNPrediction = rospy.ServiceProxy('costmap_NN_prediction_service', CostMapNNMSG)
        resp1 = getCostmapNNPrediction(seq)
        return resp1.prediction
    except rospy.ServiceException:
        print("Service call failed")


if __name__ == "__main__":
    seq =1
    print("costmap_NN_prediction client ask for prediction, seq:", seq)
    prediction = costmap_nn_reinforcerNode_client(seq)
    print("costmap_NN_prediction client receive prediction:", prediction)

    if prediction[0] >= 0.6:
        print("go~ go~ go~")

