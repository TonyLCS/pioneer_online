#!/usr/bin/env python

import rospy

import dynamic_reconfigure.client


def callback(config):
    # do nothing
    pass
    """
     rospy.loginfo("THE SERVICE HAS UPDATE THE DYNAMIC PARAM: inflation_radius= {inflation_radius},"
                  " cost_scaling_factor= {cost_scaling_factor}, robot_radius= {robot_radius},"
                  " enabled={enabled} .".format(**config))
    
    """

if __name__ == "__main__":
    rospy.init_node("dynamic_modifier")
    rospy.loginfo(""" dynamic_modifier :: client main start""")
    try:
        rospy.loginfo(""" client: wait_for_service""")
        rospy.wait_for_service("/p3dx/laser_mapper/costmap/inflation_layer", 1.0)
    except rospy.exceptions.ROSException:
        rospy.loginfo(""" service fail""")

    client = dynamic_reconfigure.client.Client("/p3dx/laser_mapper/costmap/inflation_layer", timeout=30, config_callback=callback)
    # /p3dx/move_base/global_costmap/inflation_layer
    r = rospy.Rate(0.125)  # 1/0.1= 8s period
    x = 0.1
    cost_scaling_factor = 10
    robot_radius = 0.46
    enabled = True
    while not rospy.is_shutdown():
        rospy.loginfo(""" client: go to loop.""")

        print "1. set inflation_radius :"
        x = x + 0.1
        if x > 1:
            x = 0.1
        print x
        client.update_configuration({"inflation_radius": x, "cost_scaling_factor": cost_scaling_factor,
                                     "robot_radius": robot_radius,
                                     "enabled": enabled})
        rospy.sleep(2)
        print "2. inflation_radius_return: "
        return_config = client.get_configuration()
        inflation_radius_return = return_config.inflation_radius
        print inflation_radius_return
        r.sleep()
