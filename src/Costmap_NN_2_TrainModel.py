#!/usr/bin/env python


from __future__ import print_function

import sys
import os
import rospy

import keras
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.models import load_model
import numpy as np

from keras.datasets import mnist
from skimage import io


batch_size = 20
num_classes = 2
epochs = 70
data_augmentation = True
# num_predictions = 20
save_dir = os.path.join(os.getcwd(), 'saved_NN_models')
model_name = 'elevator_model.h5'#'keras_moving_object_trained_model.h5'  # this is the output model that can be applied in futue.

world_ids_train = np.append(np.arange(10), np.arange(25, 35))#np.asarray([61, 62, 63, 1, 2, 3,  15, 16, 17,  41, 42, 43])
my_y_train = np.append(np.zeros(10), np.ones(10))# np.asarray([0,0,0,  1, 1, 1, 2, 2, 2, 3, 3, 3])

world_ids_test = np.asarray([11, 12, 35, 36, 37])
my_y_test = np.asarray([0, 0, 1, 1, 1])

figure_ori_dir = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/saved_Lidar_figure/"#""/home/tony/PycharmProjects/maze/figures/"  # "C:/Users/zzm/Desktop/report 0704/temp2/"
# figure_file_part_1 = "PAOC_window_background_taskID_10"
# figure_file_part_11 = "PAOC_window_background_taskID_1"
figure_file_part_2 = ".png"

print(world_ids_train.shape[0])
print(world_ids_test.shape[0])

#my_x_train = np.ndarray(shape=(world_ids_train.shape[0], 120, 120, 4), dtype=np.uint8)
#my_x_test = np.ndarray(shape=(world_ids_test.shape[0], 120, 120, 4), dtype=np.uint8)

figure_path = figure_ori_dir + str(world_ids_train[0]) + figure_file_part_2
img = io.imread(figure_path)  # skimage package

my_x_train = np.ndarray(shape=(world_ids_train.shape[0], img.shape[0], img.shape[1], img.shape[2]), dtype=np.uint8)
my_x_test = np.ndarray(shape=(world_ids_test.shape[0], img.shape[0], img.shape[1], img.shape[2]), dtype=np.uint8)


train_index = 0
for window_id in world_ids_train:
    """
    if window_id > 9:
        figure_path = figure_ori_dir + figure_file_part_11 + str(window_id) + figure_file_part_2
    else:
        figure_path = figure_ori_dir + figure_file_part_1 + str(window_id) + figure_file_part_2
    """

    figure_path = figure_ori_dir + str(window_id) + figure_file_part_2

    img = io.imread(figure_path)  # skimage package
    # io.imshow(img)
    # io.show()

    # print(type(img))  # 类型
    # print("img.shape: ", img.shape)  # 形状
    # print("img.width: ", img.shape[0])  # 图片宽度
    # print("img.hight: ", img.shape[1])  # 图片高度
    # print("img.rgb: ", img.shape[2])  # 图片通道数
    # print(img.size)  # 显示总像素个数
    # print(img.max())  # 最大像素值
    # print(img.min())  # 最小像素值
    # print("mean:", img.mean())  # 像素平均值
    my_x_train[train_index] = img
    train_index = train_index + 1

test_index = 0
for window_id in world_ids_test:
    """
    if window_id > 9:
        figure_path = figure_ori_dir + figure_file_part_11 + str(window_id) + figure_file_part_2
    else:
        figure_path = figure_ori_dir + figure_file_part_1 + str(window_id) + figure_file_part_2
    """
    figure_path = figure_ori_dir + str(window_id) + figure_file_part_2

    img = io.imread(figure_path)  # skimage package
    my_x_test[test_index] = img
    test_index = test_index + 1



# print(my_x_train.shape)
# print(my_x_train.dtype)
x_train = my_x_train
x_test = my_x_test
y_train = my_y_train
y_test = my_y_test
##############################################################################
print('x_train shape:', x_train.shape)
print('train samples: ', x_train.shape[0])
print('test samples:', x_test.shape[0])

# Convert class vectors to binary class matrices.
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(Conv2D(32, (3, 3), padding='same',
                 input_shape=x_train.shape[1:]))
model.add(Activation('relu'))
model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same'))
model.add(Activation('relu'))
model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes))
model.add(Activation('softmax'))

# initiate RMSprop optimizer
opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)

# Let's train the model using RMSprop
model.compile(loss='categorical_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

if not data_augmentation:
    print('Not using data augmentation.')
    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              validation_data=(x_test, y_test),
              shuffle=True)
else:
    print('Using real-time data augmentation.')
    # This will do preprocessing and realtime data augmentation:
    datagen = ImageDataGenerator(
        featurewise_center=False,  # set input mean to 0 over the dataset
        samplewise_center=False,  # set each sample mean to 0
        featurewise_std_normalization=False,  # divide inputs by std of the dataset
        samplewise_std_normalization=False,  # divide each input by its std
        zca_whitening=False,  # apply ZCA whitening
        zca_epsilon=1e-06,  # epsilon for ZCA whitening
        rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
        # randomly shift images horizontally (fraction of total width)
        width_shift_range=0.1,
        # randomly shift images vertically (fraction of total height)
        height_shift_range=0.1,
        shear_range=0.,  # set range for random shear
        zoom_range=0.,  # set range for random zoom
        channel_shift_range=0.,  # set range for random channel shifts
        # set mode for filling points outside the input boundaries
        fill_mode='nearest',
        cval=0.,  # value used for fill_mode = "constant"
        horizontal_flip=True,  # randomly flip images
        vertical_flip=False,  # randomly flip images
        # set rescaling factor (applied before any other transformation)
        rescale=None,
        # set function that will be applied on each input
        preprocessing_function=None,
        # image data format, either "channels_first" or "channels_last"
        data_format=None,
        # fraction of images reserved for validation (strictly between 0 and 1)
        validation_split=0.0)

    # Compute quantities required for feature-wise normalization
    # (std, mean, and principal components if ZCA whitening is applied).
    datagen.fit(x_train)

    # Fit the model on the batches generated by datagen.flow().
    model.fit_generator(datagen.flow(x_train, y_train,
                                     batch_size=batch_size),
                        epochs=epochs,
                        validation_data=(x_test, y_test),
                        workers=4)

# Save model and weights
if not os.path.isdir(save_dir):
    os.makedirs(save_dir)
model_path = os.path.join(save_dir, model_name)
model.save(model_path)
print('Saved trained model at %s ' % model_path)

# Score trained model.
scores = model.evaluate(x_test, y_test, verbose=1)
print('Test loss:', scores[0])
print('Test accuracy:', scores[1])

# plot model output
import matplotlib.pyplot as plt

fig, ax = plt.subplots()
# x_plot = np.linspace(0, len(x_test)-1, len(x_test))
out = model.predict(x_test, batch_size=1)
# print("x_plot:", x_plot)
print("out:", out.shape)
print("out:", out[0])
print("out:", out[0][0])
#type(out)

for i in range(0, out.shape[0]):
    for j in range(0, out.shape[1]):
        plt.scatter(i, j, s=(50 * out[i][j]) ** 2, c="b")
# ax.plot(x_plot, out, 'b')
# plt.scatter(x_plot, out)
ax.set_xlabel('Instances (0-3)')
ax.set_ylabel('Classes (1-2)')
plt.show()

