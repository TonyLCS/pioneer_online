#!/usr/bin/env python

import sys
import rospy
import csv
import random
if __name__ == "__main__":

    csvfilename_1 = "./src/robot_nav_plan/result/radius_inflation_data_1.csv"
    csvfilename_2 = "./src/robot_nav_plan/result/radius_inflation_data_2.csv"

    header_line_1 = ["id", "radius_inflation", "path_length", "class"]
    header_line_2 = ["radius_inflation", "class"]

    with open(csvfilename_1, 'wb') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(header_line_1)

    with open(csvfilename_2, 'wb') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(header_line_2)

    """
    
    a_row = ["id", "inflation_radius", "class"]
    a_row_2 = [3, "inflation_radius", "class"]

    filename = "./src/robot_nav_plan/result/test_csv_writing.csv"
    with open(filename, 'wb') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(a_row)
        writer.writerow(a_row_2)
        for iteration in range(1, 10):
            a_row = [iteration, random.uniform(0, 5)]
            writer.writerow(a_row)

    
    
    """

