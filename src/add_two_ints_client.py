#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *
from nav_msgs.srv import *
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path


def add_two_ints_client(x, y):
    rospy.wait_for_service('add_two_ints')
    try:
        add_two_ints = rospy.ServiceProxy('add_two_ints', AddTwoInts)
        resp1 = add_two_ints(x, y)
        return resp1.sum
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def get_plan_client():
    rospy.wait_for_service('/p3dx/move_base/NavfnROS/make_plan')
    # rospy.wait_for_service('/p3dx/move_base/make_plan')
    # rospy.wait_for_service('/p3dx/path_planner/navfn/make_plan')
    start = PoseStamped()
    start.header.frame_id = 'odom'
    start.pose.position.x = 0
    start.pose.position.y = 0
    start.pose.position.z = 0
    start.pose.orientation.x = 0
    start.pose.orientation.y = 0
    start.pose.orientation.z = 0
    start.pose.orientation.w = 1

    goal = PoseStamped()
    goal.header.frame_id = 'odom'
    goal.pose.position.x = 0.5
    goal.pose.position.y = 0.5
    goal.pose.position.z = 0
    goal.pose.orientation.x = 0
    goal.pose.orientation.y = 0
    goal.pose.orientation.z = 0
    goal.pose.orientation.w = 1

    path = Path()

    try:
        get_plan = rospy.ServiceProxy('/p3dx/move_base/NavfnROS/make_plan', GetPlan)
        resp1 = get_plan(start, goal, 0)
        print resp1
        print "show  path.plan.header.frame_id"
        path = resp1
        print path.plan.header.frame_id

        print "show  len(path.plan.poses)"
        print len(path.plan.poses)

    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def usage():
    return "%s [x y]"%sys.argv[0]


if __name__ == "__main__":
    if len(sys.argv) == 3:
        x = int(sys.argv[1])
        y = int(sys.argv[2])
    else:
        print usage()
        sys.exit(1)
    print "Requesting %s+%s"%(x, y)
    print "%s + %s = %s"%(x, y, add_two_ints_client(x, y))

    print "call make_plan service"
    get_plan_client()
    print "call make_plan service finish"

