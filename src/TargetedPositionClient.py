#!/usr/bin/env python

from __future__ import division

import sys
import rospy
from robot_nav_plan.srv import *
from geometry_msgs.msg import PoseStamped
from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees


def targeted_position_client(header_token_seq, seq, targeted_position):
    rospy.wait_for_service('targeted_position_service')
    try:
        print "client call targeted_position_service"
        request_the_server = rospy.ServiceProxy('targeted_position_service', TargetedPositionMSG)
        resp1 = request_the_server(header_token_seq, seq, targeted_position)  # calls the server with a request
        return resp1.current_position   # get a response from the server
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def rotate(heading, attitude, bank):
    # Assuming the angles are in radians.

    c1 = cos(heading / 2)
    s1 = sin(heading / 2)
    c2 = cos(attitude / 2)
    s2 = sin(attitude / 2)
    c3 = cos(bank / 2)
    s3 = sin(bank / 2)
    c1c2 = c1 * c2
    s1s2 = s1 * s2

    w = c1c2 * c3 - s1s2 * s3
    x = c1c2 * s3 + s1s2 * c3
    y = s1 * c2 * c3 + c1 * s2 * s3
    z = c1 * s2 * c3 - s1 * c2 * s3
    return x, y, z, w


def combine_position(x, y, angle):
    a_targeted_postition = PoseStamped()
    a_targeted_postition.header.frame_id = 'map'
    a_targeted_postition.pose.position.x = x
    a_targeted_postition.pose.position.y = y
    a_targeted_postition.pose.position.z = 0

    angle = radians(angle)

    (a_targeted_postition.pose.orientation.x, a_targeted_postition.pose.orientation.y,
     a_targeted_postition.pose.orientation.z, a_targeted_postition.pose.orientation.w) = rotate(0, angle, 0)
    return a_targeted_postition




def to_angle_rad(position):
    x = position.pose.orientation.x
    y = position.pose.orientation.y
    z = position.pose.orientation.z
    w = position.pose.orientation.w
    sqw = w * w
    sqx = x * x
    sqy = y * y
    sqz = z * z
    unit = sqx + sqy + sqz + sqw
    test = x * y + z * w
    print "unit= %s, test=%s"%(unit,test)
    if test > 0.499*unit:
        heading = 2 * atan2(x, w)
        attitude = pi / 2
        bank = 0
        print "1. heading=%s, attitude=%s, bank=%s" % (heading, attitude, bank)
        return heading, attitude, bank
    if test < -0.499 * unit:
        heading = -2 * atan2(x, w)
        attitude = -1 * pi / 2
        bank = 0
        print "2. heading=%s, attitude=%s, bank=%s" % (heading, attitude, bank)
        return heading, attitude, bank

    heading = atan2(2 * y * w - 2 * x * z, 1 - 2 * sqy - 2 * sqz)
    attitude = asin(2 * test)
    bank = atan2(2 * x * w - 2 * y * z, 1 - 2 * sqx - 2 * sqz)
    print "3. heading=%s, attitude=%s, bank=%s"%(heading, attitude, bank)

    if -1 < degrees(heading) < 1:
        angle = degrees(attitude)
    if degrees(heading) > 179:
        angle = degrees(heading) - degrees(attitude)
    if degrees(heading) < -179:
        angle = -1 * (degrees(heading) + degrees(attitude))

    print "final angle :%s" %angle

    return heading, radians(angle), bank


if __name__ == "__main__":

    print "PubModClient active to call targeted_position_service"
    token = 2
    seq = 2

    """"

    targeted_position = PoseStamped()
    targeted_position.header.frame_id = 'odom'
    targeted_position.pose.position.x = -4  # 0  # 4
    targeted_position.pose.position.y = -4  # -4  # -7
    targeted_position.pose.position.z = 0
    targeted_position.pose.orientation.x = 0
    targeted_position.pose.orientation.y = 0
    targeted_position.pose.orientation.z = 0
    targeted_position.pose.orientation.w = 1    

    """


    multistep_task_id = 4
    while multistep_task_id <= 28:
        "path 1"
        print "multistep_task_id= %s" % multistep_task_id
        if multistep_task_id == 0:
            scenario_targeted_position = combine_position(0.9, -5.5, 0)  # x,y, angle
        if multistep_task_id == 1:
            scenario_targeted_position = combine_position(1.1, -5.5, 180)  # x,y, angle
        if multistep_task_id == 2:
            scenario_targeted_position = combine_position(-2, -5.5, 180)  # x,y, angle
        if multistep_task_id == 3:
            scenario_targeted_position = combine_position(-5, -5.5, 180)  # x,y, angle
        if multistep_task_id == 4:
            scenario_targeted_position = combine_position(-7, -5.5, 180)  # x,y, angle
        if multistep_task_id == 5:
            scenario_targeted_position = combine_position(-10, -6, 270)  # x,y, angle
        "path 2"
        if multistep_task_id == 6:
            scenario_targeted_position = combine_position(-10, -5.5, 180)  # x,y, angle
        if multistep_task_id == 7:
            scenario_targeted_position = combine_position(-13.5, -3, 270)  # x,y, angle
        if multistep_task_id == 8:
            scenario_targeted_position = combine_position(-13.6, -2.9, 90)  # x,y, angle
        if multistep_task_id == 9:
            scenario_targeted_position = combine_position(-13.5, 0, 90)  # x,y, angle
        if multistep_task_id == 10:
            scenario_targeted_position = combine_position(-13.5, 3, 90)  # x,y, angle
        if multistep_task_id == 11:
            scenario_targeted_position = combine_position(-13.5, 6, 90)  # x,y, angle
        if multistep_task_id == 12:
            scenario_targeted_position = combine_position(-13.5, 9, 90)  # x,y, angle
        "path 3"
        if multistep_task_id == 13:
            scenario_targeted_position = combine_position(-12, 8, 0)  # x,y, angle
        if multistep_task_id == 14:
            scenario_targeted_position = combine_position(-9, 8, 0)  # x,y, angle
        if multistep_task_id == 15:
            scenario_targeted_position = combine_position(-6, 8, 0)  # x,y, angle
        if multistep_task_id == 16:
            scenario_targeted_position = combine_position(-3, 8, 0)  # x,y, angle
        if multistep_task_id == 17:
            scenario_targeted_position = combine_position(0, 8, 0)  # x,y, angle
        if multistep_task_id == 18:
            scenario_targeted_position = combine_position(3, 8, 0)  # x,y, angle
        if multistep_task_id == 19:
            scenario_targeted_position = combine_position(4, 8, 0)  # x,y, angle
        "path 4"
        if multistep_task_id == 20:
            scenario_targeted_position = combine_position(3.5, 8, 270)  # x,y, angle
        if multistep_task_id == 21:
            scenario_targeted_position = combine_position(4.5, 6.5, 0)  # x,y, angle
        if multistep_task_id == 22:
            scenario_targeted_position = combine_position(4.5, 4.5, 0)  # x,y, angle
        if multistep_task_id == 23:
            scenario_targeted_position = combine_position(4.5, 3, 90)  # x,y, angle
        if multistep_task_id == 24:
            scenario_targeted_position = combine_position(4.5, 3, 270)  # x,y, angle
        if multistep_task_id == 25:
            scenario_targeted_position = combine_position(4, -1.5, 90)  # x,y, angle
        if multistep_task_id == 26:
            scenario_targeted_position = combine_position(3.9, -1.4, 270)  # x,y, angle
        if multistep_task_id == 27:
            scenario_targeted_position = combine_position(4, -6.5, 90)  # x,y, angle
        if multistep_task_id == 28:
            scenario_targeted_position = combine_position(0, 0, 90)  # x,y, angle

        # scenario_targeted_position = combine_position(-2, -2, 180)  # x,y, angle
        current_position = targeted_position_client(token, seq, scenario_targeted_position)
        angle_1, angle_2, angle_3 = to_angle_rad(scenario_targeted_position)
        # print degrees(angle_1), degrees(angle_2), degrees(angle_3)

        rospy.sleep(3)
        multistep_task_id = multistep_task_id + 1


    """
 
    if -1 < degrees(angle_1) < 1:
        angle = degrees(angle_2)
    if degrees(angle_1) > 179:
        angle = degrees(angle_1) - degrees(angle_2)
    if degrees(angle_1) < -179:
        angle = -1 * (degrees(angle_1) + degrees(angle_2))   

    """
    # current_position = targeted_position_client(token, seq, targeted_position)
    # print "token %s/ seq %s/ position.x-req %s/ position.x-rsp %s"%(token, seq, str(targeted_position.pose.position.x), str(current_position.pose.position.x))



