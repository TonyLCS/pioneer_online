#!/usr/bin/env python


from __future__ import print_function

import sys
import os
import rospy

#### keras
import keras
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.models import load_model
import numpy as np

from keras.datasets import mnist
import matplotlib.pyplot as plt

#####ros
import copy
from robot_nav_plan.srv import *
from nav_msgs.msg import OccupancyGrid
import numpy as np
from skimage import io
import matplotlib.pyplot as plt
from geometry_msgs.msg import PoseStamped


class SecondaryReinforcer_NN(object):
    def __init__(self):

        self.slam_current_position = PoseStamped()
        self.get_current_slam_current_position = False

        self.token = 3
        self.seq = 2
        self.modifier_request_value = 3

        self.fig_index = 0
        self.save_dir = os.path.join(os.getcwd(), 'src/robot_nav_plan/result/saved_figures/')

        self.model_save_dir = os.path.join(os.getcwd(), 'saved_NN_models')
        self.model_name = 'elevator_model.h5'#'keras_moving_object_trained_model.h5'  # this is the output model that can be applied in futue.

        self.model = self.loading_NN_model(self.model_save_dir, self.model_name)

    def callback_slammed_position(self, msg):
        self.slam_current_position = msg
        self.get_current_slam_current_position = True

    def convert_map_data_3(self, data, height_ori, width_ori):
        # height_ori = 320
        # width_ori = 480
        mapdata = np.zeros([height_ori, width_ori])
        print("convert into map data")
        for line_index in range(0, height_ori):
            width_index = line_index * width_ori
            mapdata[height_ori - line_index - 1] = data[width_index: (width_index + width_ori)]
        return mapdata

    def add_scope_to_map(self, mapdata_ori, height_ori, width_ori, origin_x, origin_y, position_x, position_y, x_range, y_range):
        # print(mapdata)
        mapdata = copy.deepcopy(mapdata_ori)
        """
        mapdata[height_ori-228-1, 100] = 150 # this is original point of coordinary
        #mapdata[height_ori-228-1-10, 100+10] = 150 # (1,1)
        "draw scope"
        y_range = 20
        x_range = 10
    
        mapdata[(height_ori-228-1-y_range),(100-x_range):(100+x_range)] = 250
        mapdata[(height_ori-228-1+y_range),(100-x_range):(100+x_range+1)] = 250
        mapdata[(height_ori-228-1-y_range):(height_ori-228-1+y_range),(100-x_range)] = 250
        mapdata[(height_ori-228-1-y_range):(height_ori-228-1+y_range),(100+x_range)] = 250
        """
        mapdata[height_ori - origin_y - 1, origin_x] = 250  # this is original point of coordinary

        linewidth = 3
        mapdata[int(height_ori - position_y - 1 - y_range):int(height_ori - position_y - 1 + y_range),
        int(position_x - x_range -linewidth):int(position_x - x_range +linewidth)] = 250
        mapdata[int(height_ori - position_y - 1 - y_range):int(height_ori - position_y - 1 + y_range),
        int(position_x + x_range -linewidth):int(position_x + x_range +linewidth)] = 250
        mapdata[int(height_ori - position_y - 1 - y_range-linewidth):int(height_ori - position_y - 1 - y_range+linewidth), int(position_x - x_range):int(position_x + x_range)] = 250
        mapdata[int(height_ori - position_y - 1 + y_range-linewidth):int(height_ori - position_y - 1 + y_range+linewidth), int(position_x - x_range):int(position_x + x_range)] = 250

        return mapdata
        # io.imshow(mapdata)
        # io.show()

    def slice_map(self, mapdata, height_ori, position_x, position_y, x_range, y_range):
        submap = mapdata[int(height_ori - position_y - 1 - y_range):int(height_ori - position_y - 1 + y_range), int(position_x - x_range):int(position_x + x_range)]
        return submap

    def costmap_client_3(self, header_token_seq, seq, modifier_request_value):
        rospy.wait_for_service('costmap_cell_service')
        try:
            print("costmap_cell_service")
            ask_to_publich_modifier = rospy.ServiceProxy('costmap_cell_service', CostMapMSG)
            resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
            return resp1.modifier_return_value, resp1.height, resp1.width, resp1.origin_x, resp1.origin_y
        except rospy.ServiceException:
            print("Service call failed: %s")

    def loading_NN_model(self, save_dir, model_name):
        #save_dir = os.path.join(os.getcwd(), 'saved_NN_models')
        #model_name = 'keras_moving_object_trained_model.h5'  # this is the output model that can be applied in futue.
        model_path = os.path.join(save_dir, model_name)
        model = load_model(model_path)
        print('Load model from %s ' % model_path)
        return model

    def NN_prediction(self):
        #save_dir = os.path.join(os.getcwd(), 'saved_NN_models')
        #model_name = 'keras_moving_object_trained_model.h5'  # this is the output model that can be applied in futue.

        figure_ori_dir = "/home/tony/tony_catkin/chris_catkin_ws/src/robot_nav_plan/result/saved_figures/"  # ""/home/tony/PycharmProjects/maze/figures/"  # "C:/Users/zzm/Desktop/report 0704/temp2/"
        figure_file_part_2 = "currentCostmap.png"
        test_index = 0
        figure_path = figure_ori_dir + figure_file_part_2
        figure_path = self.save_dir + figure_file_part_2
        img = io.imread(figure_path)  # skimage package
        my_x_test = np.ndarray(shape=(1, img.shape[0], img.shape[1], img.shape[2]), dtype=np.uint8)
        #my_x_test = np.ndarray(shape=(1, 126, 126, 4), dtype=np.uint8)

        my_x_test[test_index] = img
        currentCostmapFigure = my_x_test

        #model_path = os.path.join(save_dir, model_name)
        #model = load_model(model_path)
        #print('Load model from %s ' % model_path)

        out = self.model.predict(currentCostmapFigure, batch_size=1)
        # print("x_plot:", x_plot)
        #print("out:", out.shape)
        print("out:", out)
        #print("out:", out[0][0])

        fig, ax = plt.subplots()
        for i in range(0, out.shape[0]):
            for j in range(0, out.shape[1]):
                plt.scatter(i, j, s=(50 * out[i][j]) ** 2, c="b")

        ax.set_xlabel('Instances (0-3)')
        ax.set_ylabel('Classes (1-2)')
        #plt.show()
        plt.pause(3)
        # plt.show()
        plt.close()
        return out


    def get_costmap(self):
        print("to get costmap figure in realtime")

        (costmap_data, height, width, origin_x, origin_y) = self.costmap_client_3(self.token, self.seq, self.modifier_request_value)
        # print "data %s" %str(costmap_data)
        print("get costmap with height %s/ width %s/ " % (str(height), str(width)))

        resolution = 10
        original_x = int(-1 * resolution * origin_x)
        original_y = int(-1 * resolution * origin_y)

        sub = rospy.Subscriber('/p3dx/pose_listener/slammed_pose', PoseStamped, self.callback_slammed_position)
        rate = rospy.Rate(1)
        while not self.get_current_slam_current_position:
            rate.sleep()

        world_x = int(self.slam_current_position.pose.position.x * resolution)
        world_y = int(self.slam_current_position.pose.position.y * resolution)
        print("current position : x=", self.slam_current_position.pose.position.x, "; y=",
              self.slam_current_position.pose.position.y)

        x_matrix = original_x + world_x
        y_matrix = original_y + world_y

        # convert_map_data(costmap_data)
        mapdata_converted = self.convert_map_data_3(costmap_data, height, width)
        x_range = 7
        y_range = 7
        mapdata = self.add_scope_to_map(mapdata_converted, height, width, int(-10 * origin_x), int(-10 * origin_y),
                                   x_matrix, y_matrix, x_range, y_range)
        # position_x = 10
        # position_y = 22.8
        # submapdata = slice_map(mapdata, height, position_x, position_y)
        submapdata = self.slice_map(mapdata_converted, height, x_matrix, y_matrix, x_range, y_range)
        #print("size of ")

        print("plt plot")
        # plt.imshow(mapdata)
        # plt.draw()
        # plt.pause(1)

        fig = plt.figure(figsize=(3.4, 1.7))
        ax1 = fig.add_subplot(1, 2, 1)
        ax2 = fig.add_subplot(1, 2, 2)
        ax1.imshow(mapdata)
        ax2.imshow(submapdata)

        plt.draw()

        # Save model and weights
        if not os.path.isdir(self.save_dir):
            os.makedirs(self.save_dir)

        png_complete_name = os.path.join(self.save_dir, "full_" + str(self.fig_index) + ".png")
        fig.savefig(png_complete_name)

        extent = ax2.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        #png_part_name = os.path.join(save_dir, str(fig_index) + ".png")
        #fig.savefig(png_part_name, bbox_inches=extent)

        fig_name = os.path.join(self.save_dir, "currentCostmap.png")
        fig.savefig(fig_name, bbox_inches=extent)

        plt.pause(2)
        # plt.show()
        plt.close()
        self.fig_index = self.fig_index + 1


if __name__ == "__main__":

    rospy.init_node('costmap_NN_prediction_service')
    print("costmap_NN_prediction_service node init...")

    secondaryReinforcer_nn_object = SecondaryReinforcer_NN()
    print("secondaryReinforcer_nn_object init...")

    call_from_client = False

    def activeService(req):
        print("request req:", req.seq)
        #secondaryReinforcer_nn_object.get_costmap()
        global call_from_client
        global prediction
        call_from_client = True
        rate = rospy.Rate(1)
        while call_from_client:
            print("service thread is waiting until get prediction from the main thread")
            rate.sleep()
        #prediction1 = [1.0, 0.85]
        prediction1 = prediction[0]
        print("NN prediction[0]:", prediction1)
        return CostMapNNMSGResponse(prediction1)

    s = rospy.Service('costmap_NN_prediction_service', CostMapNNMSG, activeService)
    print("ready for costmap_NN_prediction_service")

    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        if call_from_client:
            secondaryReinforcer_nn_object.get_costmap()
            prediction = secondaryReinforcer_nn_object.NN_prediction()
            print("NN prediction:", prediction)
            call_from_client = False
            print("end of a service, go to sleep~")

        rate.sleep()




    """
    

    rate = rospy.Rate(1)
    for iteration_loop in range(1):

        secondaryReinforcer_nn_object.get_costmap()
        prediction = secondaryReinforcer_nn_object.NN_prediction()
        print("NN prediction:", prediction)

        rate.sleep()
    """
