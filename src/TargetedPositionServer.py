#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *
from nav_msgs.srv import *
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path


global new_calculate_result
global slam_current_position
global token_active_flag
global get_current_slam_current_position


def callback_slammed_position(msg):
    global slam_current_position
    global get_current_slam_current_position

    slam_current_position = msg
    get_current_slam_current_position = True
    pass


def targeted_position_publish_procedural(targeted_position):
    # level 4
    global new_calculate_result
    global token_active_flag
    global slam_current_position
    global get_current_slam_current_position

    if token_active_flag:
        #pub = rospy.Publisher('/p3dx/path_planner/goal', PoseStamped)  # set the goal for the path planner
        pub = rospy.Publisher('/p3dx/path_planner_homeo/goal', PoseStamped)  # set the goal for the path planner_homeostasis
        pub.publish(targeted_position)
        new_calculate_result = True
        get_current_slam_current_position = False

        sub = rospy.Subscriber('/p3dx/pose_listener/slammed_pose', PoseStamped, callback_slammed_position)

        rate = rospy.Rate(1)
        while not get_current_slam_current_position:
            rate.sleep()


def planner_path_length_service_callback(req):
    # level 3
    global slam_current_position
    global new_calculate_result
    global token_active_flag

    new_calculate_result = False
    token_active_flag = True  # set flag to start the exhibition of result

    print "targeted_position_service:: service receives request: "  # request from the client node comes from here
    print req.header_token_seq, req.seq, req.targeted_position
    targeted_position_publish_procedural(req.targeted_position)  # go to procedural to process the request

    rate = rospy.Rate(1)            # wait for until the procedural is done
    while not new_calculate_result:
        rate.sleep()
    print "new_calculate_result"
    print new_calculate_result
    token_active_flag = False  # set flag to stop the exhibition of result

    return TargetedPositionMSGResponse(slam_current_position)  # response the request


def spin_planner_path_length_service():
    # level 2
    rospy.init_node('targeted_position_service')
    s = rospy.Service('targeted_position_service', TargetedPositionMSG, planner_path_length_service_callback)
    print "targeted_position_service:: Service spin. Ready to response to the client."
    rospy.spin()


if __name__ == "__main__":

    spin_planner_path_length_service()
    print "targeted_position_service  finish"

