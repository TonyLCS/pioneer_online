#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *
from geometry_msgs.msg import PoseStamped
import random

def targeted_position_client(header_token_seq, seq, targeted_position):
    rospy.wait_for_service('targeted_position_service')
    try:
        print "client call targeted_position_service"
        request_the_server = rospy.ServiceProxy('targeted_position_service', TargetedPositionMSG)
        resp1 = request_the_server(header_token_seq, seq, targeted_position)  # calls the server with a request
        return resp1.current_position   # get a response from the server
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def planner_path_length_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('planner_path_length_service')
    try:
        print "client call planner_path_length_service"
        ask_to_publich_modifier = rospy.ServiceProxy('planner_path_length_service', PublishModifierMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def publich_modifier_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('publish_modifier')
    try:
        ask_to_publich_modifier = rospy.ServiceProxy('publish_modifier', PublishModifierMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


if __name__ == "__main__":

    for seq in range(1, 41, 1):
        print ""
        """##################### 1 ##################################"""
        print "1. PubModClient active to call PubModService"
        token = 1
        # modifier_request_value = random.uniform(25, 35) / 100
        modifier_request_value = float(seq) / 2
        print "1. token %s/ seq %s/ mod-req %s/" \
              % (token, seq, modifier_request_value)
        modifier_response_value = publich_modifier_client(token, seq, modifier_request_value)
        print "1. token %s/ seq %s/ mod-req %s/ mod-rsp %s" \
              % (token, seq, modifier_request_value, str(modifier_response_value))
        print ""

        """##################### 2  ##################################"""
        print "2. PubModClient active to call targeted_position_service"
        token = 2

        targeted_position = PoseStamped()
        targeted_position.header.frame_id = 'odom'
        targeted_position.pose.position.x = 0 # 0  # 4
        targeted_position.pose.position.y = 0  # -4  # -7
        targeted_position.pose.position.z = 0
        targeted_position.pose.orientation.x = 0
        targeted_position.pose.orientation.y = 0
        targeted_position.pose.orientation.z = 0
        targeted_position.pose.orientation.w = 1

        current_position = targeted_position_client(token, seq, targeted_position)
        print "2. token %s/ seq %s/ " % (token, seq)
        print "2. target_position ( x = %s, y = %s)" % (
        str(targeted_position.pose.position.x), str(targeted_position.pose.position.y))
        print "2. current_position( x = %s, y = %s)" % (
        str(current_position.pose.position.x), str(current_position.pose.position.y))
        print ""

        """##################### 3  ##################################"""
        print "3. PubModClient active to call planner_path_length_service"
        token = 3
        modifier_request_value = 1000
        length_return = planner_path_length_client(token, seq, modifier_request_value)
        print "3. token %s/ seq %s/ the length of the path %s" % (token, seq, str(length_return))
        print "###########################################################################"
        print ""

