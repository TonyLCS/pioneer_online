#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path


from nav_msgs.srv import *
from nav_msgs.msg import OccupancyGrid

global costmap_data
global new_calculate_result
global info_height
global info_width
global origin_x
global origin_y

def get_grid_information(OccupancyGrid):
    global costmap_data
    global new_calculate_result
    global info_height
    global info_width

    global origin_x
    global origin_y

    #print "show  path.plan.header.seq"
    #print OccupancyGrid.header.seq

    costmap_data = OccupancyGrid.data
    info_height = OccupancyGrid.info.height
    info_width = OccupancyGrid.info.width
    origin_x = OccupancyGrid.info.origin.position.x
    origin_y = OccupancyGrid.info.origin.position.y

    new_calculate_result = True

def reqest_costmap():
    # level 4
    #print "subscribe to path topic..."
    # rospy.Subscriber("/p3dx/path_planner/navfn/plan", Path, calculate_length_of_path)
    rospy.Subscriber("/p3dx/laser_map", OccupancyGrid, get_grid_information)


def costmap_service_callback(req):
    # level 3
    global costmap_data
    global new_calculate_result
    global token_active_flag
    global info_height
    global info_width
    global origin_x
    global origin_y

    new_calculate_result = False
    path_length = 0
    token_active_flag = True  # set flag to start the exhibition of result

    #print "planner_path_length_service:: service receives request: "  # request from the client node comes from here
    #print req.header_token_seq, req.seq, req.modifier_request_value
    reqest_costmap()  # go to procedural to process the request

    rate = rospy.Rate(1)            # wait for until the procedural is done
    while not new_calculate_result:
        rate.sleep()
    #print "new_calculate_result"
    #print new_calculate_result
    token_active_flag = False  # set flag to stop the exhibition of result

    print("show  costmap_data")
    #print costmap_data

    return CostMapMSGResponse(costmap_data, info_height, info_width, origin_x, origin_y)  # response the request


def spin_costmap_service():
    # level 2
    rospy.init_node('costmap_cell_service')
    s = rospy.Service('costmap_cell_service', CostMapMSG, costmap_service_callback)
    print("costmap_cell_service:: Ready to call costmap_cell_service.")
    rospy.spin()


if __name__ == "__main__":

    spin_costmap_service()
    print("call costmap service finish")
