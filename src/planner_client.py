#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *
from nav_msgs.srv import *
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path


def get_plan_client():
    rospy.wait_for_service('/p3dx/path_planner/navfn/make_plan')
    # rospy.wait_for_service('/p3dx/move_base/make_plan')
    # rospy.wait_for_service('/p3dx/path_planner/navfn/make_plan')
    start = PoseStamped()
    start.header.frame_id = 'odom'
    start.pose.position.x = 0
    start.pose.position.y = 0
    start.pose.position.z = 0
    start.pose.orientation.x = 0
    start.pose.orientation.y = 0
    start.pose.orientation.z = 0
    start.pose.orientation.w = 1

    goal = PoseStamped()
    goal.header.frame_id = 'odom'
    goal.pose.position.x = 1
    goal.pose.position.y = 0
    goal.pose.position.z = 0
    goal.pose.orientation.x = 0
    goal.pose.orientation.y = 0
    goal.pose.orientation.z = 0
    goal.pose.orientation.w = 1

    path = Path()

    try:
        get_plan = rospy.ServiceProxy('/p3dx/path_planner/navfn/make_plan', GetPlan)
        resp1 = get_plan(start, goal, 0)
        #print resp1
        #print "show  path.plan.header.frame_id"
        path = resp1
        #print path.plan.header.frame_id

        #print "show  len(path.plan.poses)"
        #print len(path.plan.poses)

    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def get_current_position(data):
    print "I heard frame_id " + data.header.frame_id
    #print data.header.frame_id
    print "I heard I am at position " + str(data.pose.position.x) + ", " + str(data.pose.position.y) + ", " + str(data.pose.position.z)
    path_back(data)


def path_back(start):
    rospy.wait_for_service('/p3dx/path_planner/navfn/make_plan')
    # rospy.wait_for_service('/p3dx/move_base/make_plan')
    # rospy.wait_for_service('/p3dx/path_planner/navfn/make_plan')

    start.header.frame_id = 'odom'
    goal = PoseStamped()
    goal.header.frame_id = start.header.frame_id
    goal.pose.position.x = start.pose.position.x + 4
    goal.pose.position.y = start.pose.position.y - 4
    goal.pose.position.z = start.pose.position.z
    goal.pose.orientation.x = start.pose.orientation.x
    goal.pose.orientation.y = start.pose.orientation.y
    goal.pose.orientation.z = start.pose.orientation.z
    goal.pose.orientation.w = start.pose.orientation.w

    path = Path()

    try:
        get_plan = rospy.ServiceProxy('/p3dx/move_base/NavfnROS/make_plan', GetPlan)
        resp1 = get_plan(start, goal, 0)
        #print resp1
        print "show  path.plan.header.frame_id"
        path = resp1
        print path.plan.header.frame_id

        print "show 1 meter ahead len(path.plan.poses)"
        print len(path.plan.poses)

    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def calculate_length_of_path(path):
    print "show  path.plan.header.seq"
    print path.header.seq
    print "show path_planner's path len(path.plan.poses)"
    print len(path.poses)


def init_a_node():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("/p3dx/pose_listener/slammed_pose", PoseStamped, get_current_position)
    # subscript path_planner's path
    rospy.Subscriber("/p3dx/path_planner/navfn/plan", Path, calculate_length_of_path)
    rospy.spin()


if __name__ == "__main__":

    print "client: call for make_plan service"
    rospy.init_node("robotNavPlanService", anonymous=True)
    rate = rospy.Rate(4)
    while not rospy.is_shutdown():
        #get_plan_client()
        # rospy.Subscriber("/p3dx/pose_listener/slammed_pose", PoseStamped, get_current_position)
        rospy.Subscriber("/p3dx/path_planner/navfn/plan", Path, calculate_length_of_path)
        rate.sleep()
    # get_plan_client()
    print "call make_plan service finish"

    # print "get current position"
    # rospy.Subscriber("/p3dx/pose_listener/slammed_pose", PoseStamped, get_current_position)
    # init_a_node()

    # print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
