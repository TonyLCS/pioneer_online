#!/usr/bin/env python

import rospy
import time

from dynamic_reconfigure.server import Server
from dynamic_tutorials.cfg import TutorialsConfig
from geometry_msgs.msg import Twist

from std_msgs.msg import String
import roslib

global condition_1
global action_1
global condition_2
global action_2
global time_inhibition

global condition_lcs
global condition_lcs_s1
global condition_lcs_s2
global condition_lcs_s3



def callback(config, level):
    rospy.loginfo("""Reconfigure Request: {int_param}, {double_param},\ 
          {str_param}, {bool_param}, {size}""".format(**config))
    return config


def callback_path_planner(msg):
    set_condition(1)
    set_action(1, msg)


def callback_reflex(msg):
    global time_inhibition
    set_condition(2)
    set_condition(3)
    set_action(2, msg)
    inhibition_timer()


def publish_mux(msg):
    pub.publish(msg)
    # clean_condition


def publish_lcs_condition():
    global condition_lcs
    pub_lcs_condition.publish(condition_lcs)  # publish multiplex state


def create_lcs_condition():
    global condition_lcs_s1
    global condition_lcs_s2
    global condition_lcs_s3
    global condition_lcs

    """check inhibition, if delay > 1s, then no inhibition"""
    delay = rospy.Time.from_sec(time.time()) - time_inhibition
    if delay > rospy.Duration(2):
        clean_condition_att(2)
        clean_condition_att(3)

    condition_lcs = condition_lcs_s1 + condition_lcs_s2 + condition_lcs_s3


def show_lcs_condition():
    global condition_lcs
    rospy.loginfo(" lcs_condition(" + condition_lcs + ")")


def set_action(action_seq, msg):
    global action_1
    global action_2

    if action_seq == 1:
        action_1 = msg  # cmd_vel from Multipler_PathPlanner
        #rospy.loginfo("receive a message from '/p3dx/Multipler_PathPlanner/cmd_vel'")
        #rospy.loginfo("Linear components: (%f, %f, %f)", msg.linear.x, msg.linear.y, msg.linear.z)
    if action_seq == 2:
        action_2 = msg  # cmd_vel from Multipler_Reflex
        #rospy.loginfo("receive a message from '/p3dx/Multipler_Reflex/cmd_vel'")
        #rospy.loginfo("Linear components: (%f, %f, %f)", msg.linear.x, msg.linear.y, msg.linear.z)


def set_condition(condition_seq):
    global condition_1
    global condition_2

    global condition_lcs_s1
    global condition_lcs_s2
    global condition_lcs_s3

    if condition_seq == 1:
        condition_1 = True
        condition_lcs_s1 = '1'

    if condition_seq == 2:
        condition_2 = True
        condition_lcs_s2 = '1'

    if condition_seq == 3:
        condition_lcs_s3 = '1'


def clean_condition_att(condition_seq):

    global condition_lcs_s1
    global condition_lcs_s2
    global condition_lcs_s3

    if condition_seq == 1:
        condition_lcs_s1 = '0'

    if condition_seq == 2:
        condition_lcs_s2 = '0'

    if condition_seq == 3:
        condition_lcs_s3 = '0'


def clean_condition():
    global condition_1
    global condition_2

    global condition_lcs_s1
    global condition_lcs_s2
    global condition_lcs_s3

    condition_1 = False
    condition_2 = False

    condition_lcs_s1 = '0'
    condition_lcs_s2 = '0'
    condition_lcs_s3 = '0'


def set_decision():
    global condition_1
    global action_1
    global condition_2
    global action_2
    global time_inhibition

    global condition_lcs

    """ lcs: 1.send condition"""
    create_lcs_condition()
    show_lcs_condition()
    publish_lcs_condition()

    """lcs : 2 show all action """
    # rospy.loginfo("     PathPlanner (%f, %f, %f), Reflex (%f, %f, %f)",action_1.linear.x, action_1.linear.y, action_1.linear.z, action_2.linear.x, action_2.linear.y, action_2.linear.z)

    """lcs rules effect here"""
    if condition_lcs == "000" or condition_lcs == "100":
        publish_mux(action_1)  # Multipler_PathPlanner
    if condition_lcs == "001" or condition_lcs == "010" or condition_lcs == "011" \
            or condition_lcs == "101" or condition_lcs == "110" or condition_lcs == "111":
        publish_mux(action_2)  # Multipler_Reflex

    if condition_lcs == '011' or condition_lcs == '111':  # condition_2:
        inhibition_timer()

    delay = rospy.Time.from_sec(time.time()) - time_inhibition

    if delay <= rospy.Duration(1):
        set_condition(3)
        publish_mux(action_2)
        # rospy.loginfo("receive a message from '/p3dx/Multiplexer_Reflex/cmd_vel'")
        rospy.loginfo(" Multiplexer_Reflex!!!")
        rospy.loginfo(" Multiplexer_Reflex -> Linear components: (%f, %f, %f)", action_2.linear.x, action_2.linear.y, action_2.linear.z)

    elif condition_lcs == '100' or condition_lcs == '000':  # condition_1:
        publish_mux(action_1)
        # rospy.loginfo("receive a message from '/p3dx/Multiplexer_PathPlanner/cmd_vel'")
        # rospy.loginfo(" Multiplexer_PathPlanner -> Linear components: (%f, %f, %f)", action_1.linear.x, action_1.linear.y, action_1.linear.z)
    if not delay <= rospy.Duration(1):
        clean_condition_att(3)

    # clean_condition()


def inhibition_timer():
    """stop vel published by pathPlanner, just do reflex"""
    global time_inhibition
    time_inhibition = rospy.Time.from_sec(time.time())


def init():
    global condition_1
    global action_1
    global condition_2
    global action_2
    global time_inhibition

    global condition_lcs
    global condition_lcs_s1
    global condition_lcs_s2
    global condition_lcs_s3

    condition_1 = False
    action_1 = Twist()
    condition_2 = False
    action_2 = Twist()

    condition_lcs = None
    condition_lcs_s1 = '#'
    condition_lcs_s2 = '#'
    condition_lcs_s3 = '#'

    init_action(action_1)
    init_action(action_2)

    time_inhibition = rospy.Time.from_sec(time.time())
    rospy.sleep(3)


def init_action(data):
    data.linear.x = 0
    data.linear.y = 0
    data.linear.z = 0
    data.angular.x = 0
    data.angular.y = 0
    data.angular.z = 0
    # rospy.loginfo("Linear components: (%f, %f, %f)", data.linear.x, data.linear.y, data.linear.z)


if __name__ == "__main__":
    global condition_1
    global action_1
    global condition_2
    global action_2

    init()

    rospy.init_node("Multiplexer_CmdVel", anonymous=False)

    #   subscribe to topics that publish cmd_vel information
    rospy.Subscriber('/p3dx/Multiplexer_PathPlanner/cmd_vel', Twist, callback_path_planner)
    rospy.Subscriber('/p3dx/Multiplexer_Reflex/cmd_vel', Twist, callback_reflex)
    #   2. publish vel command for Pioneer
    pub = rospy.Publisher('/p3dx/cmd_vel', Twist)

    # LCS: publish condition
    pub_lcs_condition = rospy.Publisher('/p3dx/Multiplexer/lcs_condition', String)


    # dynamic setting
    srv = Server(TutorialsConfig, callback)
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        set_decision()
        rate.sleep()
