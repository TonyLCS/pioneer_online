#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *
from nav_msgs.srv import *
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path

global new_calculate_result
global path_length
global token_active_flag


def calculate_length_of_path(path):
    global path_length
    global new_calculate_result
    global token_active_flag
    # print "calculate_length_of_path token_active_flag (%s)"%token_active_flag
    if token_active_flag:
        print "show  path.plan.header.seq"
        print path.header.seq
        print "show path_planner's path len(path.plan.poses)"
        print len(path.poses)
        new_calculate_result = True
        path_length = len(path.poses)


def reqest_planner_for_length_of_path():
    # level 4
    print "subscribe to path topic..."
    # rospy.Subscriber("/p3dx/path_planner/navfn/plan", Path, calculate_length_of_path)
    rospy.Subscriber("/p3dx/path_planner_homeo/navfn/plan", Path, calculate_length_of_path)


def planner_path_length_service_callback(req):
    # level 3
    global path_length
    global new_calculate_result
    global token_active_flag

    new_calculate_result = False
    path_length = 0
    token_active_flag = True  # set flag to start the exhibition of result

    print "planner_path_length_service:: service receives request: "  # request from the client node comes from here
    print req.header_token_seq, req.seq, req.modifier_request_value
    reqest_planner_for_length_of_path()  # go to procedural to process the request

    rate = rospy.Rate(1)            # wait for until the procedural is done
    while not new_calculate_result:
        rate.sleep()
    print "new_calculate_result"
    print new_calculate_result
    token_active_flag = False  # set flag to stop the exhibition of result

    return PublishModifierMSGResponse(path_length)  # response the request

def spin_planner_path_length_service():
    # level 2
    rospy.init_node('planner_path_length_service')
    s = rospy.Service('planner_path_length_service', PublishModifierMSG, planner_path_length_service_callback)
    print "publish_modifier_service:: Ready to call planner_path_length_service."
    rospy.spin()


if __name__ == "__main__":

    spin_planner_path_length_service()
    print "call make_plan service finish"

