#!/usr/bin/env python

from __future__ import division
import sys
import time

import rospy
import copy
from robot_nav_plan.srv import *
from geometry_msgs.msg import PoseStamped
from math import sin, cos, pi, atan, floor, pow, radians, atan2, asin, degrees
import random

from robot_nav_plan.msg import PAOCWindowMSG



global has_current_position
global slam_current_position
global scenario_targeted_position
global goal_id
global window_id

global already_complete_multistep_scenario
global PAO_feedback_seq
global publish_position
global get_PAOC_feedback_seq
global csv_seq_step_seq


def to_angle_rad(position):
    x = position.pose.orientation.x
    y = position.pose.orientation.y
    z = position.pose.orientation.z
    w = position.pose.orientation.w
    sqw = w * w
    sqx = x * x
    sqy = y * y
    sqz = z * z
    unit = sqx + sqy + sqz + sqw
    test = x * y + z * w
    print "unit= %s, test=%s"%(unit,test)
    if test > 0.499*unit:
        heading = 2 * atan2(x, w)
        attitude = pi / 2
        bank = 0
        print "1. heading=%s, attitude=%s, bank=%s" % (heading, attitude, bank)
        return heading, attitude, bank
    if test < -0.499 * unit:
        heading = -2 * atan2(x, w)
        attitude = -1 * pi / 2
        bank = 0
        print "2. heading=%s, attitude=%s, bank=%s" % (heading, attitude, bank)
        return heading, attitude, bank

    heading = atan2(2 * y * w - 2 * x * z, 1 - 2 * sqy - 2 * sqz)
    attitude = asin(2 * test)
    bank = atan2(2 * x * w - 2 * y * z, 1 - 2 * sqx - 2 * sqz)
    print "3. heading=%s, attitude=%s, bank=%s"%(heading, attitude, bank)

    if -1 < degrees(heading) < 1:
        angle = degrees(attitude)
    if degrees(heading) > 179:
        angle = degrees(heading) - degrees(attitude)
    if degrees(heading) < -179:
        angle = -1 * (degrees(heading) + degrees(attitude))

    print "final angle :%s" %angle

    return heading, radians(angle), bank


def rotate(heading, attitude, bank):
    # Assuming the angles are in radians.

    c1 = cos(heading / 2)
    s1 = sin(heading / 2)
    c2 = cos(attitude / 2)
    s2 = sin(attitude / 2)
    c3 = cos(bank / 2)
    s3 = sin(bank / 2)
    c1c2 = c1 * c2
    s1s2 = s1 * s2

    w = c1c2 * c3 - s1s2 * s3
    x = c1c2 * s3 + s1s2 * c3
    y = s1 * c2 * c3 + c1 * s2 * s3
    z = c1 * s2 * c3 - s1 * c2 * s3
    return x, y, z, w


def combine_position(x, y, angle):
    a_targeted_postition = PoseStamped()
    a_targeted_postition.header.frame_id = 'map'
    a_targeted_postition.pose.position.x = x
    a_targeted_postition.pose.position.y = y
    a_targeted_postition.pose.position.z = 0

    #print "angle -deg = %s"%angle
    angle = radians(angle)
    #print "angle -rad = %s"%angle
    (a_targeted_postition.pose.orientation.x, a_targeted_postition.pose.orientation.y,
     a_targeted_postition.pose.orientation.z, a_targeted_postition.pose.orientation.w) = rotate(0, angle, 0)
    #print "a_targeted_postition.pose.orientation.z= %s, a_targeted_postition.pose.orientation.w= %s"%(a_targeted_postition.pose.orientation.z, a_targeted_postition.pose.orientation.w)
    return a_targeted_postition


def planner_path_length_client(header_token_seq, seq, modifier_request_value):
    rospy.wait_for_service('planner_path_length_service')
    try:
        print "client call planner_path_length_service"
        ask_to_publich_modifier = rospy.ServiceProxy('planner_path_length_service', PublishModifierMSG)
        resp1 = ask_to_publich_modifier(header_token_seq, seq, modifier_request_value)
        return resp1.modifier_return_value
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def targeted_position_client(header_token_seq, seq, targeted_position):
    rospy.wait_for_service('targeted_position_service')
    try:
        print "client call targeted_position_service"
        request_the_server = rospy.ServiceProxy('targeted_position_service', TargetedPositionMSG)
        resp1 = request_the_server(header_token_seq, seq, targeted_position)  # calls the server with a request
        return resp1.current_position   # get a response from the server
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def publich_modifier_client(modifier_request_value):
    rospy.wait_for_service('publish_modifier')
    try:
        ask_to_publich_modifier = rospy.ServiceProxy('publish_modifier', PublishModifierMSG)
        resp1 = ask_to_publich_modifier(0, 0, modifier_request_value)
        return resp1.modifier_return_value
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def check_target_within_window(width):
    global slam_current_position
    global scenario_targeted_position
    global has_current_position

    has_current_position = False
    rospy.Subscriber('/p3dx/pose_listener/slammed_pose', PoseStamped,
                     callback_slammed_position)  # get current position
    rate = rospy.Rate(10)  # 10hz
    while not has_current_position:
        print "wait for subscription of slammed_pose... check every 0.1 second"
        rate.sleep()

    distance_x = scenario_targeted_position.pose.position.x - slam_current_position.pose.position.x
    distance_y = scenario_targeted_position.pose.position.y - slam_current_position.pose.position.y

    distance_2 = pow(distance_x, 2) + pow(distance_y, 2)

    if distance_2 < pow(width, 2) + pow(width/2, 2):
        return True
    else:
        return False


def callback_slammed_position(msg):
    global has_current_position
    global slam_current_position
    # print "get slam_current_position. "
    slam_current_position = msg
    has_current_position = True


def generate_position_to_stay_at_window_orignal():
    global window_id
    global slam_current_position
    position_to_stay_at_window_orignal_x = slam_current_position.pose.position.x
    position_to_stay_at_window_orignal_y = slam_current_position.pose.position.y
    position_to_stay_at_window_orignal = combine_position(position_to_stay_at_window_orignal_x,
                                                          position_to_stay_at_window_orignal_y, window_id*90)
    return position_to_stay_at_window_orignal


def calcualte_window_id(position):
    global window_id
    "the orientation of the input position decise the window_id of PAOC"
    angle_1, angle_2, angle_3 = to_angle_rad(position)
    angle = degrees(angle_2)
    window_id = 0

    if -45 < angle <= 45:
        window_id = 0
    if 45 < angle <= 135:
        window_id = 1
    if 135 < angle <= 225:
        window_id = 2
    if 225 < angle <= 270 or -90 <= angle <= -45:
        window_id = 3
    return window_id


def single_step_targeted_position_client(token, seq, targeted_position):
    rospy.wait_for_service('multiple_cmdVel_service')
    try:
        print "     client call multiple_cmdVel_service"
        request_the_server = rospy.ServiceProxy('multiple_cmdVel_service', TargetedPositionMSG)
        resp1 = request_the_server(token, seq, targeted_position)  # calls the server with a request
        print "     Service success response."
        return resp1.current_position   # get a response from the server
    except rospy.ServiceException, e:
        print "     Service call failed: %s"%e


def call_PAOC_record_service(header_token_seq, csv_seq, window_id, window_width, facing_position):
    "publish massage to a initial client node, which call for service"
    global PAO_feedback_seq
    global multistep_task_id
    global csv_seq_step_seq

    pub = rospy.Publisher('/p3dx/paoc/windowUpdate', PAOCWindowMSG)  # set the goal for the path planner
    pub.publish(header_token_seq, csv_seq, window_id, window_width, facing_position)
    rospy.Subscriber('/p3dx/paoc/serviceComplement', PAOCWindowMSG, call_PAOC_return_service)

    pub_count = 0
    r = rospy.Rate(2)
    while True:
        print "     paoc windowUpdate publish, require PAOC_update_node to fulfill csv_seq= %s, csv_seq_step_seq=%s," \
              " multistep_task_id=%s, current feedback = %s"\
              %(csv_seq, csv_seq_step_seq, multistep_task_id, PAO_feedback_seq)
        pub = rospy.Publisher('/p3dx/paoc/windowUpdate', PAOCWindowMSG)  # set the goal for the path planner
        pub.publish(header_token_seq, csv_seq, window_id, window_width, facing_position)
        "also subscribe in order to keep the same page if it finish"
        rospy.Subscriber('/p3dx/paoc/serviceComplement', PAOCWindowMSG, call_PAOC_return_service)
        pub_count = pub_count + 1
        if pub_count >= 10:
            break
        r.sleep()
    print " paoc windowUpdate publish"


def call_PAOC_return_service(msg):
    global PAO_feedback_seq
    global get_PAOC_feedback_seq
    PAO_feedback_seq = msg.csv_seq
    get_PAOC_feedback_seq = True
    #print "PAOC feedback, PAO_feedback_seq = %s" %PAO_feedback_seq


def select_targeted_position():
    global scenario_targeted_position
    global multistep_task_id

    "path 1"
    print "multistep_task_id= %s" % multistep_task_id
    if multistep_task_id == 0:
        scenario_targeted_position = combine_position(1, -4, 270)  # x,y, angle
    if multistep_task_id == 1:
        scenario_targeted_position = combine_position(-2, -4, 270)  # x,y, angle
    if multistep_task_id == 2:
        scenario_targeted_position = combine_position(-5, -4, 270)  # x,y, angle
    if multistep_task_id == 3:
        scenario_targeted_position = combine_position(-8, -4, 270)  # x,y, angle
    if multistep_task_id == 4:
        scenario_targeted_position = combine_position(-7, -5.5, 180)  # x,y, angle
    if multistep_task_id == 5:
        scenario_targeted_position = combine_position(-10, -6, 270)  # x,y, angle
    "path 2"
    if multistep_task_id == 6:
        scenario_targeted_position = combine_position(-10, -5.5, 180)  # x,y, angle
    if multistep_task_id == 7:
        scenario_targeted_position = combine_position(-13.5, -3, 270)  # x,y, angle
    if multistep_task_id == 8:
        scenario_targeted_position = combine_position(-13.6, -2.9, 90)  # x,y, angle
    if multistep_task_id == 9:
        scenario_targeted_position = combine_position(-13.5, 0, 90)  # x,y, angle
    if multistep_task_id == 10:
        scenario_targeted_position = combine_position(-13.5, 3, 90)  # x,y, angle
    if multistep_task_id == 11:
        scenario_targeted_position = combine_position(-13.5, 6, 90)  # x,y, angle
    if multistep_task_id == 12:
        scenario_targeted_position = combine_position(-13.5, 9, 90)  # x,y, angle
    if multistep_task_id == 13:
        scenario_targeted_position = combine_position(-10, 8.5, 180)  # x,y, angle

    if multistep_task_id == 14:
        scenario_targeted_position = combine_position(-10, 5.5, 180)  # x,y, angle
    if multistep_task_id == 15:
        scenario_targeted_position = combine_position(-11.5, 4, 270)  # x,y, angle
    if multistep_task_id == 16:
        scenario_targeted_position = combine_position(-8.5, 4, 270)  # x,y, angle
    if multistep_task_id == 17:
        scenario_targeted_position = combine_position(-5.5, 4, 270)  # x,y, angle
    if multistep_task_id == 18:
        scenario_targeted_position = combine_position(-2.5, 4, 270)  # x,y, angle
    if multistep_task_id == 19:
        scenario_targeted_position = combine_position(0.5, 4, 270)  # x,y, angle
    if multistep_task_id == 20:
        scenario_targeted_position = combine_position(2.5, 4, 270)  # x,y, angle
    if multistep_task_id == 21:
        scenario_targeted_position = combine_position(1, 5.5, 0)  # x,y, angle
    if multistep_task_id == 22:
        scenario_targeted_position = combine_position(1, 8.5, 0)  # x,y, angle
    if multistep_task_id == 23:
        scenario_targeted_position = combine_position(3.5, 8, 270)  # x,y, angle
    "path 4"
    if multistep_task_id == 24:
        scenario_targeted_position = combine_position(4.5, 6, 0)  # x,y, angle
    if multistep_task_id == 25:
        scenario_targeted_position = combine_position(4.5, 4.5, 0)  # x,y, angle
    if multistep_task_id == 26:
        scenario_targeted_position = combine_position(4.5, 3, 90)  # x,y, angle
    if multistep_task_id == 27:
        scenario_targeted_position = combine_position(4.5, 3, 270)  # x,y, angle
    if multistep_task_id == 28:
        scenario_targeted_position = combine_position(4, -1.5, 90)  # x,y, angle
    if multistep_task_id == 29:
        scenario_targeted_position = combine_position(3.9, -1.4, 270)  # x,y, angle
    if multistep_task_id == 30:
        scenario_targeted_position = combine_position(5, -5.5, 180)  # x,y, angle
    if multistep_task_id == 31:
        scenario_targeted_position = combine_position(5, -6, 90)  # x,y, angle
    "room"
    if multistep_task_id == 32:
        scenario_targeted_position = combine_position(-12.5, -10, 0)  # x,y, angle
    if multistep_task_id == 33:
        scenario_targeted_position = combine_position(-12.5, -10.5, 0)  # x,y, angle
    """
        "path 1"
    print "multistep_task_id= %s" % multistep_task_id
    if multistep_task_id == 0:
        scenario_targeted_position = combine_position(1, -4, 270)  # x,y, angle
    if multistep_task_id == 1:
        scenario_targeted_position = combine_position(-2, -4, 270)  # x,y, angle
    if multistep_task_id == 2:
        scenario_targeted_position = combine_position(-5, -4, 270)  # x,y, angle
    if multistep_task_id == 3:
        scenario_targeted_position = combine_position(-8, -4, 270)  # x,y, angle
    if multistep_task_id == 4:
        scenario_targeted_position = combine_position(-7, -5.5, 180)  # x,y, angle
    if multistep_task_id == 5:
        scenario_targeted_position = combine_position(-10, -6, 270)  # x,y, angle
    "path 2"
    if multistep_task_id == 6:
        scenario_targeted_position = combine_position(-10, -5.5, 180)  # x,y, angle
    if multistep_task_id == 7:
        scenario_targeted_position = combine_position(-13.5, -3, 270)  # x,y, angle
    if multistep_task_id == 8:
        scenario_targeted_position = combine_position(-13.6, -2.9, 90)  # x,y, angle
    if multistep_task_id == 9:
        scenario_targeted_position = combine_position(-13.5, 0, 90)  # x,y, angle
    if multistep_task_id == 10:
        scenario_targeted_position = combine_position(-13.5, 3, 90)  # x,y, angle
    if multistep_task_id == 11:
        scenario_targeted_position = combine_position(-13.5, 6, 90)  # x,y, angle
    if multistep_task_id == 12:
        scenario_targeted_position = combine_position(-13.5, 9, 90)  # x,y, angle
    "path 3"
    if multistep_task_id == 13:
        scenario_targeted_position = combine_position(-12, 8, 0)  # x,y, angle
    if multistep_task_id == 14:
        scenario_targeted_position = combine_position(-9, 8, 0)  # x,y, angle
    if multistep_task_id == 15:
        scenario_targeted_position = combine_position(-6, 8, 0)  # x,y, angle
    if multistep_task_id == 16:
        scenario_targeted_position = combine_position(-3, 8, 0)  # x,y, angle
    if multistep_task_id == 17:
        scenario_targeted_position = combine_position(0, 8, 0)  # x,y, angle
    if multistep_task_id == 18:
        scenario_targeted_position = combine_position(3, 8, 0)  # x,y, angle
    if multistep_task_id == 19:
        scenario_targeted_position = combine_position(4, 8, 0)  # x,y, angle
    "path 4"
    if multistep_task_id == 20:
        scenario_targeted_position = combine_position(3.5, 8, 270)  # x,y, angle
    if multistep_task_id == 21:
        scenario_targeted_position = combine_position(4.5, 6, 0)  # x,y, angle
    if multistep_task_id == 22:
        scenario_targeted_position = combine_position(4.5, 4.5, 0)  # x,y, angle
    if multistep_task_id == 23:
        scenario_targeted_position = combine_position(4.5, 3, 90)  # x,y, angle
    if multistep_task_id == 24:
        scenario_targeted_position = combine_position(4.5, 3, 270)  # x,y, angle
    if multistep_task_id == 25:
        scenario_targeted_position = combine_position(4, -1.5, 90)  # x,y, angle
    if multistep_task_id == 26:
        scenario_targeted_position = combine_position(3.9, -1.4, 270)  # x,y, angle
    if multistep_task_id == 27:
        scenario_targeted_position = combine_position(5, -5.5, 180)  # x,y, angle
    if multistep_task_id == 28:
        scenario_targeted_position = combine_position(5, -6, 90)  # x,y, angle

    "path 3.2"
    if multistep_task_id == 30:
        scenario_targeted_position = combine_position(-10, 8.5, 180)  # x,y, angle
    if multistep_task_id == 31:
        scenario_targeted_position = combine_position(-10, 5.5, 180)  # x,y, angle
    if multistep_task_id == 32:
        scenario_targeted_position = combine_position(-11.5, 4, 270)  # x,y, angle
    if multistep_task_id == 33:
        scenario_targeted_position = combine_position(-8.5, 4, 270)  # x,y, angle
    if multistep_task_id == 34:
        scenario_targeted_position = combine_position(-5.5, 4, 270)  # x,y, angle
    if multistep_task_id == 35:
        scenario_targeted_position = combine_position(-2.5, 4, 270)  # x,y, angle
    if multistep_task_id == 36:
        scenario_targeted_position = combine_position(0.5, 4, 270)  # x,y, angle
    if multistep_task_id == 37:
        scenario_targeted_position = combine_position(2.5, 4, 270)  # x,y, angle
    if multistep_task_id == 38:
        scenario_targeted_position = combine_position(1, 5.5, 0)  # x,y, angle
    if multistep_task_id == 39:
        scenario_targeted_position = combine_position(1, 8.5, 0)  # x,y, angle
    if multistep_task_id == 40:
        scenario_targeted_position = combine_position(3, 8, 0)  # x,y, angle

    "repeat taskID 21, to test dataset which has 4 times (600) instances"
    if multistep_task_id == 51:
        scenario_targeted_position = combine_position(3.5, 8, 270)  #
    if multistep_task_id == 52:
        scenario_targeted_position = combine_position(4.5, 6, 0)  #
        
    
    """
    return scenario_targeted_position


if __name__ == "__main__":

    rospy.init_node('multistep_client')

    global scenario_targeted_position
    global already_complete_multistep_scenario
    global csv_seq_step_seq
    global PAO_feedback_seq
    global publish_position

    global has_current_position
    global slam_current_position
    global goal_id
    global window_id
    global get_PAOC_feedback_seq
    global multistep_task_id


    print "Multistep client initiate"

    token = 8
    job_id = 3
    csv_seq_step_seq = -1
    window_width = 3
    multistep_task_id = 9
    PAO_feedback_seq = -2

    last_scenario_targeted_position = PoseStamped()
    fix_facing_position = PoseStamped()
    publish_position = PoseStamped()

    fix_facing_position.header.frame_id = 'map'
    publish_position.header.frame_id = 'map'
    last_scenario_targeted_position.header.frame_id = 'map'

    #to_test_select_targeted_position()
    #multistep_task_id = 6

    #while not rospy.is_shutdown():
    while multistep_task_id <= 33:

        "1. subscribe scenario_targeted_position"
        # subscribe
        already_complete_multistep_scenario = False

        pass
        select_targeted_position()

        "2. check whether already finish a multistep scenario"
        seq = 0

        csv_seq_step_seq = multistep_task_id

        while not already_complete_multistep_scenario:
            print "\n"
            print "Job_id = %s" % job_id
            pass

            """######## job 1 #########"""
            " set up facing position"

            if job_id == 1:
                "job 1.1 calculate, window_id"
                print "job 1.1  calculate  window_id"
                calcualte_window_id(last_scenario_targeted_position)

                print "job 1.2 facing: get a right position for PAOC execution "
                publish_position = copy.deepcopy(last_scenario_targeted_position)
                fix_facing_position = copy.deepcopy(last_scenario_targeted_position)
                """
                # publish_position = generate_position_to_stay_at_window_orignal()
                publish_position.pose.position.x = last_scenario_targeted_position.pose.position.x
                publish_position.pose.position.y = last_scenario_targeted_position.pose.position.y
                publish_position.pose.position.z = last_scenario_targeted_position.pose.position.z

                publish_position.pose.orientation.x = last_scenario_targeted_position.pose.orientation.x
                publish_position.pose.orientation.y = last_scenario_targeted_position.pose.orientation.y
                publish_position.pose.orientation.z = last_scenario_targeted_position.pose.orientation.z
                publish_position.pose.orientation.w = last_scenario_targeted_position.pose.orientation.w

                last_scenario_targeted_position
                "when it get its facing position, change job_id"
                # fix_facing_position.header.frame_id = 'odom'
                fix_facing_position.pose.position.x = last_scenario_targeted_position.pose.position.x
                fix_facing_position.pose.position.y = last_scenario_targeted_position.pose.position.y
                fix_facing_position.pose.position.z = last_scenario_targeted_position.pose.position.z

                fix_facing_position.pose.orientation.x = last_scenario_targeted_position.pose.orientation.x
                fix_facing_position.pose.orientation.y = last_scenario_targeted_position.pose.orientation.y
                fix_facing_position.pose.orientation.z = last_scenario_targeted_position.pose.orientation.z
                fix_facing_position.pose.orientation.w = last_scenario_targeted_position.pose.orientation.w
                """


            """######## job 2#########"""
            "5. stay at window origin position before PAOC finish"
            if job_id == 2:
                print "job 2 stay at window fix_facing_position position before PAOC finish"
                #publish_position = generate_position_to_stay_at_window_orignal()

                "call PAOC client"
                calcualte_window_id(last_scenario_targeted_position) # get Window_id
                call_PAOC_record_service(job_id, csv_seq_step_seq, window_id, window_width, fix_facing_position)

                publish_position = copy.deepcopy(fix_facing_position)
                "set location"
                "check if the publish_position is achieveable"
                publish_position_is_accessable = False
                publish_position_is_accessable_count = 1
                while not publish_position_is_accessable:
                    "add small different"
                    small_random_distance = random.uniform(0, 1) / 5
                    if random.uniform >= 0.5:
                        publish_position.pose.position.x = fix_facing_position.pose.position.x + small_random_distance
                    else:
                        publish_position.pose.position.x = fix_facing_position.pose.position.x - small_random_distance
                    if random.uniform >= 0.5:
                        publish_position.pose.position.y = fix_facing_position.pose.position.y + small_random_distance
                    else:
                        publish_position.pose.position.y = fix_facing_position.pose.position.y - small_random_distance

                    targeted_position_client(token, seq, publish_position)
                    length_return = planner_path_length_client(token, seq, 0.15)
                    if 2 <= length_return <= 90:
                        publish_position_is_accessable = True
                    else:
                        publish_position_is_accessable = False
                        publish_position_is_accessable_count = publish_position_is_accessable_count + 1

                    if publish_position_is_accessable_count >= 20:
                        publish_position_is_accessable = True  # quit
                        #  job_id = 3  # quit

            """######## job 3 #########"""
            "6. go to window target position when PAOC finish " \
            "(make sure target position is not beyond scenario_targeted_position )"
            if job_id == 3:
                print "job 3.1 go to window target position when PAOC finish"
                publich_modifier_client(0.15) #publish modifier
                calcualte_window_id(scenario_targeted_position) # window_id
                publish_position = copy.deepcopy(scenario_targeted_position)
                """
                "7. check if the pulbish_position is beyone the target"
                if check_target_within_window(window_width):
                    print "job3.2 target is within window! "
                    publish_position = copy.deepcopy(scenario_targeted_position)                
                """


            "8. publich position  and call for service"

            print "execute job_id (%s) publich position  and call for service" %job_id

            if job_id == 2:
                print "facing position x=%s, y=%s." % (
                    str(fix_facing_position.pose.position.x), str(fix_facing_position.pose.position.y))
            print "publish position x=%s, y=%s." % (
                str(publish_position.pose.position.x), str(publish_position.pose.position.y))

            current_position = single_step_targeted_position_client(token, seq, publish_position)

            print "current_position: token %s/ seq %s/, x= %s/ , y= %s/ ." \
                  % (token, seq, str(current_position.pose.position.x), str(current_position.pose.position.y))
            seq = seq + 1

            """################################"""
            """######## job_id update #########"""
            """################################"""

            job_update_status = False

            """# job 1  #"""
            if not job_update_status:
                if job_id == 1:
                    pass
                    rospy.sleep(5)
                    job_id = 2
                    job_update_status = True

            """# job 2  #"""
            "update PAOC states"
            if not job_update_status:
                if job_id == 2:
                    rospy.Subscriber('/p3dx/paoc/serviceComplement', PAOCWindowMSG, call_PAOC_return_service)
                    rospy.sleep(5)

                    print "csv_seq_step_seq = %s, PAO_feedback_seq = %s" %(csv_seq_step_seq, PAO_feedback_seq)

                    if PAO_feedback_seq == csv_seq_step_seq:
                        job_id = 3
                        print "\n"
                        print "finish job 2 !!!"
                        print "finish a task csv_seq_step_seq = %s, PAO_feedback_seq = %s" % (csv_seq_step_seq, PAO_feedback_seq)

                    else:
                        print "csv_seq_step_seq = %s, PAO_feedback_seq = %s" % (csv_seq_step_seq, PAO_feedback_seq)

                    job_update_status = True

            """# job 3  #"""
            if not job_update_status:
                if job_id == 3:

                    "when it achieve this position, set job_id into initiative value and end this iteration"
                    has_current_position = False
                    rospy.Subscriber('/p3dx/pose_listener/slammed_pose', PoseStamped,
                                     callback_slammed_position)  # get current position
                    rate = rospy.Rate(10)  # 10hz
                    while not has_current_position:
                        print "wait for subscription of slammed_pose... check every 0.1 second"
                        rate.sleep()

                    distance_x = slam_current_position.pose.position.x - scenario_targeted_position.pose.position.x
                    distance_y = slam_current_position.pose.position.y - scenario_targeted_position.pose.position.y
                    distance_2 = pow(distance_x, 2) + pow(distance_y, 2)
                    if distance_2 < 0.1:
                        print "\n"
                        print "finish job 3!"
                        already_complete_multistep_scenario = True

                        job_id = 1
                        last_scenario_targeted_position = copy.deepcopy(scenario_targeted_position)
                        """
                        last_scenario_targeted_position.pose.position.x = scenario_targeted_position.pose.position.x
                        last_scenario_targeted_position.pose.position.y = scenario_targeted_position.pose.position.y
                        last_scenario_targeted_position.pose.position.z = scenario_targeted_position.pose.position.z
                        last_scenario_targeted_position.pose.orientation.x = scenario_targeted_position.pose.orientation.x
                        last_scenario_targeted_position.pose.orientation.y = scenario_targeted_position.pose.orientation.y
                        last_scenario_targeted_position.pose.orientation.z = scenario_targeted_position.pose.orientation.z
                        last_scenario_targeted_position.pose.orientation.w = scenario_targeted_position.pose.orientation.w
                        """
                    rospy.sleep(5)
                    job_update_status = True

            print "client wait for 10s, sleep, multistep_task_id= %s."%multistep_task_id
            print "\n"

            rospy.sleep(10)

        print "finish a multistep task"
        multistep_task_id = multistep_task_id + 1


