#!/usr/bin/env python

import rospy
import sys
import roslaunch


def startANode():
    package = 'robot_nav_plan'
    executable = 'token_controller.py'
    node = roslaunch.core.Node(package, executable, name="robot_nav_plan")

    launch = roslaunch.scriptapi.ROSLaunch()
    launch.start()

    process = launch.launch(node)
    print process.is_alive()


if __name__ == "__main__":
    rospy.init_node("token_controller", anonymous=False)
    rospy.loginfo(""" token_controller node init""")
    try:
        rospy.loginfo("""to start a token_controller node""")
        startANode()
    except rospy.exceptions.ROSException:
        rospy.loginfo(""" token_controller start fail""")

    r = rospy.Rate(0.1)
    while not rospy.is_shutdown():
        r.sleep()