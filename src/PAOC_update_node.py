#!/usr/bin/env python

import sys
import rospy
from robot_nav_plan.srv import *
from nav_msgs.srv import *
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path


from robot_nav_plan.msg import PAOCWindowMSG

global req_record


def call_PAOC_record_service(req):

    global req_record
    global resp1
    print "begin to call PAOC_service"
    rospy.wait_for_service('PAOC_service')

    if req.csv_seq == req_record:
        print "this request alread been sent before, received request csv_seq=%s"%req.csv_seq

        "publish a PAOC complement flag"
        pub_count = 0
        r = rospy.Rate(3)
        while True:
            print "     feedback publish"
            pub = rospy.Publisher('/p3dx/paoc/serviceComplement', PAOCWindowMSG)  # set the goal for the path planner
            pub.publish(req.header_token_seq, req.csv_seq, req.window_id, req.window_width, req.facing_position)
            pub_count = pub_count + 1
            if pub_count >= 30:
                break
            r.sleep()
        print "finish feedback publish, publish req.csv_seq = %s "%req.csv_seq

    else:

        rospy.wait_for_service('PAOC_service')
        try:
            print "     client active PAOC_service"
            request_paoc_server = rospy.ServiceProxy('PAOC_service', PAOCMSG)
            print "     pass req.csv_seq=%s to PAOC_service."%req.csv_seq
            resp1 = request_paoc_server(req.header_token_seq, req.csv_seq, req.window_id, req.window_width,
                                        req.facing_position)  # calls the server with a request
            "set the req_record for flag"
            req_record = req.csv_seq
            print "     PAOC_service finish one single step data collection. set req_record=%s" %req_record
            "publish a PAOC complement flag"

            pub_count = 0
            r = rospy.Rate(3)
            while True:
                "set flag of finish"

                pub = rospy.Publisher('/p3dx/paoc/serviceComplement', PAOCWindowMSG)  # set the goal for the path planner
                pub.publish(req.header_token_seq, req_record, req.window_id, req.window_width, resp1.current_position)
                pub_count = pub_count + 1
                if pub_count >= 60:
                    break
                r.sleep()
            print "finish feedback publish, the finish csv_seq=%s"%req_record
            return resp1.current_position  # get a response from the server
        except rospy.ServiceException, e:
            print "     PAOC_service call failed: %s" % e

    """
    try:
        if req.csv_seq == req_record:
            pass

        else:
            print "     client call PAOC_service"
            request_paoc_server = rospy.ServiceProxy('PAOC_service', PAOCMSG)
            resp1 = request_paoc_server(req)  # calls the server with a request
            req_record = req.csv_seq
            print "     PAOC_service success response."
            return resp1.current_position  # get a response from the server

    except rospy.ServiceException, e:
        print "     PAOC_service call failed: %s" % e    
    
    """

    print "finish to call PAOC_service"


def require_paoc_service():
    rospy.init_node("paoc_window_update_node", anonymous=True)
    rospy.Subscriber('/p3dx/paoc/windowUpdate', PAOCWindowMSG, call_PAOC_record_service, queue_size=10)
    rospy.spin()
    """
    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        rospy.Subscriber('/p3dx/paoc/windowUpdate', PAOCWindowMSG, call_PAOC_record_service)
        rate.sleep
    # rospy.spin()    
    """



if __name__ == "__main__":
    print "client: requiring_paoc_update_node"
    global req_record
    req_record = -1
    require_paoc_service()

